![](SimArchitectExample.png)

SIMarchitect is an open source framework for modeling and simulation and forms a shell on MATLAB/Simulink(R). SIMarchitect makes the simulation work of system developers and students much easier because of the used data bus. This forces a standard interface through which engineers could work uniformly. System models and algorithms thus become reusable and can easily be placed in a library.

**LICENSE**

SIMarchitect is based on original work produced by TNO  
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Note that SIMarchitect libraries, such as the HDH-HILS library, are licensed under library-specific license 
conditions. See the license file in the top level directory of the respective library. To install a library
please consult the readme file located in the library base directory.

**DISCLAIMER**

The code of SIMarchitect is based on original work produced by TNO. The Hogeschool van Arnhem en
Nijmegen (hereafter: “HAN”) published SIMarchitect in consultation and cooperation with TNO.
HAN has deliberately chosen to publish SIMarchitect in open source. The reason for using open
source is that HAN wants to contribute to innovation and development. HAN believes it is important
to share knowledge and give others the opportunity to use SIMarchitect as a starting point for
further developments.
Because it is open source and controlled by the community, HAN does not control the source code
nor its quality. Use SIMarchitect on your own risk. SIMarchitect is made available under the MIT
license. Please note the license contains an exclusion of liability.

**Wiki**

A wiki providing more information on SIMarchitect and the HDH-HILS library can be found here:
http://openmbd.com/wiki/SIMarchitect

**Installation**

How to run SIMarchitect for the first time:

 
1. Start MATLAB 2015b or higher and navigate to the ../Framework folder

 
2. Start SIMarchitect by typing "SIMarchitect" on the MATLAB
 command line in the directory (Framework must be the current folder of MATLAB)

3. Accept the License and Disclaimer to continue installation.

4. Type "SIMarchitect" again to start the programme   


  Built with MATLAB Version: 8.6.0.267246 (R2015b)  
  
  **Contact**
  
  For more information contact jan.benders@han.nl
