%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research � TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
%}

function SIMarchitect(action)
%------------------Path Section: Add SIMarchitect directories to the MATLAB path-------------
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%
%Add the appropriate paths based on where SIMarchitect.m is found with the which command (This
%             defines the main SIMarchitect directory)
% NOTE: paths will only be added with the addpath command if they don't already exist.
%If new directories were added to the path (using addpath), then the path is saved in the 
%		MATLAB pathdef.m file. 

% directories needed to add to MATLAB path for SIMarchitect to run properly (they will be added in this order, the one that is on top 
% here will be on top in the matlab path as well)
%
% Used to determine whether the SIMarchitect.m script is called for the first
% time.
persistent SIMarchitect_first_time;

warning off

addpath(strcat(pwd,'\general\library\functions'))
SIMarchitect_first_time = 1;

% Adding paths to License Files/ Accept/Decline License and Disclaimer Prompt

if ~exist('SIMarchitect_root.m','file') % Check is SIMarchitect_root is installed. If not, execute License and Disclaimer prompt
        SIMarchitect_first_time = 1;
        
        % Add paths of function files
        mydir  = pwd;
        idcs   = strfind(mydir,'\');
        newdir = mydir(1:idcs(end)-1);
        addpath(newdir);
        disp('License and Disclaimer Must Be Agreed')
        addpath(strcat(pwd,'\general\library\functions'))
        addpath(strcat(pwd,'\general\library\functions\gui')) 
        
        % Open License and Disclaimer Installer
        assignin('base','Accept_tc',[])
        gui_t_and_c
        waitfor(gui_t_and_c);
        Accept_tc = evalin('base','Accept_tc'); %% Take Accept_tc value from workspace
        
        % Determine if License and Disclaimer Accepted
        if Accept_tc == 0 %% Declined (Installation aborted, reset to default values)
            msg = 'SIMarchitect License and Disclaimer Declined. Installation has been aborted!';
            warndlg(msg)
            SIMarchitect_first_time = []; %% Reset value to empty
            assignin('base','Accept_tc',[]); 
            evalin('base','clear Accept_tc')
            error(msg); % Cancels further execution of installation
            
        elseif isempty(Accept_tc); %% Dialog Closed (Installation aborted, reset to default values)
            msg = 'Installation has been aborted!';
            warndlg(msg)
            evalin('base','clear Accept_tc');
            SIMarchitect_first_time = []; %% Reset value to empty
            error(msg); 
            
        end
        
        % Accepted
        evalin('base','clear Accept_tc')
        disp('License and Disclaimer Agreed');

end

% When there are paths added to the matlab path, then this flag needs to be
% set to 1.
added2matlab = 0;

% Get the SIMarchitect installation path
SIMarchitect_installation_path = get_SIMarchitect_installation_path;

% Get the helper functions of SIMarchitect working!
SIMarchitect_gen_lib = [SIMarchitect_installation_path, '\general\library\functions'];
if ~isdir( SIMarchitect_gen_lib )
    errordlg(['SIMarchitect not correctly installed. Could not find the SIMarchitect general functions.'],...
              'SIMarchitect -> Error');
    return;
end

if SIMarchitect_add2path({ SIMarchitect_installation_path, SIMarchitect_gen_lib })
    added2matlab = 1;
end

% Check the SIMarchitect_root.m function
try
    flag = strcmpi(SIMarchitect_installation_path, SIMarchitect_root); 
catch
    flag = 0; %function not available
end

if ~flag,
    fid=fopen([SIMarchitect_installation_path,'\general\library\functions\SIMarchitect_root.m'],'wt+');
    if fid == -1,   %file not succesfully opened.
        eval(['!attrib -R "',SIMarchitect_installation_path,'\general\library\functions\SIMarchitect_root.m"']);
        fid=fopen([SIMarchitect_installation_path,'\general\library\functions\SIMarchitect_root.m'],'wt+');
        if fid == -1,
            warndlg(['Could not create/modify the file SIMarchitect_root.m. Some functionality is lost;',...
                    'please make sure that you have free access to this file, then try again.'],'SIMarchitect -> Warning');
        else
            create_SIMarchitect_root(fid,SIMarchitect_installation_path);    
        end
        fprintf('SIMarchitect, first startup\n');
        for iii=1:15
            pause(.1)
            fprintf('.');
        end
        fprintf('\nInitialisation of SIMarchitect succesfull\n')
        fprintf('Please type ''SIMarchitect'' to start\n')
        SIMarchitect_first_time = 0;
        %create the tempdir if it doesn't exist
        temppath = [SIMarchitect_installation_path,'\temp'];
        if ~isdir(temppath)
            mkdir(SIMarchitect_installation_path,'temp')
        end
        return
    else
        create_SIMarchitect_root(fid,SIMarchitect_installation_path);
        fprintf('SIMarchitect, first startup\n');
        %create the tempdir if it doesn't exist
        temppath = [SIMarchitect_installation_path,'\temp'];
        if ~isdir(temppath)
            mkdir(SIMarchitect_installation_path,'temp')
        end
        for iii=1:15
            pause(.1)
            fprintf('.');
        end
        fprintf('\nInitialisation of SIMarchitect succesfull\n')
        fprintf('Please type ''SIMarchitect'' to start\n')
        SIMarchitect_first_time = 0;
        return
    end
    fclose(fid);
end

% Get all the paths that needs to be included of the current SIMarchitect
% installation!
components   = get_installed_components;
if SIMarchitect_first_time == 1
    disp('SIMarchitect components loaded:');
end
for i=1:length(components)
    info = get_component_info(components{i});
    
    includeCompDir        = [SIMarchitect_root, '\', components{i}];
    if isdir(includeCompDir)
        if SIMarchitect_add2path({includeCompDir})
            added2matlab = 1;
        end
    end
    
    if isfield(info, 'include')
        if ~iscell(info.include)
            temp = info.include;
            info.include = {temp};
        end
        
        if SIMarchitect_add2path(info.include, components{i})
            added2matlab = 1;
        end
    end
    
    if SIMarchitect_first_time == 1
        if isfield(info, 'name')
            disp(['- ', info.name]);
            
        else
            disp(['- ', components{i}]);
        end
    end
end

% When there are paths added to the MATLAB path, the user gets the question
% wheter or not to save this.
if added2matlab
    y=questdlg('The Matlab path has been updated with SIMarchitect directories for this Matlab session.  Would you like to save the new path for future Matlab sessions?', ...
        'SIMarchitect -> Question', ...
        'Yes','No','Yes');
    
    switch y,
        case 'Yes',       
            if strcmp(version('-release'), '13')
                result=path2rc; %path2rc saves the current path to pathdef.m (Both are MATLAB files-- not SIMarchitect files)
            else
                % Save the current MATLAB path in the pathdef.m file.
                % Within 2007a path2rc is depricated!
                result=savepath;
            end
            
            if result==0
                disp('SIMarchitect Directories were successfully added to the Matlab Path.  See Path browser for path information.');
                
            else
                disp('Not successful. SIMarchitect directories were not successfully saved in the Matlab Path.');
            end
            
        case 'No',
    end % switch   
end

% Set the first time variable to zero.
SIMarchitect_first_time = 0;

% Load Library Database

library_load

% Start startup figure
gui_start;

%%__________________________________________________________________________
function create_SIMarchitect_root(fid,SIMarchitect_dir)
fprintf(fid,'function SIMarchitect_root_dir=SIMarchitect_root\n');
fprintf(fid,'%%returns the root of the SIMarchitect demo\n');
fprintf(fid,'SIMarchitect_root_dir=char(''%s'');\n',SIMarchitect_dir);
fprintf(fid,'%%EOF');

%%__________________________________________________________________________
function installation_path = get_SIMarchitect_installation_path
    installation_path = mfilename('fullpath');
    installation_path = installation_path(1:end-length(mfilename)-1);

%%__________________________________________________________________________
function path_added = SIMarchitect_add2path ( cellList, component )

    if nargin == 1
        component = '';
    else
        component = ['\', component, '\'];
    end

    p          = lower(path);
    path_added = 0;
    
    for i=1:1:length(cellList), %:-1:1,              
        if ~isempty(strfind(cellList{i}, ':\')) || ~isempty(strfind(cellList{i}, '\\'))
            directory = lower(cellList{i});
        else
            directory = lower([SIMarchitect_root, component, cellList{i}]);
        end

        regdir = directory;
        regdir = regexprep(regdir, '\\+$', '');
        regdir = regexprep(regdir, '\/+$', '');
        regdir = strrep(regdir, '//', '/');
        regdir = strrep(regdir, '/', '\/');
        regdir = strrep(regdir, '\', '\\');

        str = {['^', regdir, ';'], [';', regdir, ';'], [';', regdir,'$']};
        res = regexp(p, str);
        
        notFound = isempty(res{1}) && isempty(res{2}) && isempty(res{3});
        
        if notFound
            if isdir(directory),   %check whether the directory exists, if yes then add otherwise issue warning
                addpath(directory);
                path_added = 1;
            else
                disp(['WARNING -> The standard directory: ',directory,...
                        ' can''t be found, SIMarchitect may not work properly']);
            end            
        end    
    end

% Function check previous SIMarchitect installs. When different SIMarchitect versions are
% living in this path, conflicts between these two could happen.
function result = SIMarchitect_check_matlab_path
    p  = lower(path);
    cs = strfind(p, ';');
    
    start = 1;
    for i=1:length(cs)
        c = cs(i);
    
        item = p(start:c-1);
        start = c+1;
        
        r = strfind(item, '\general\library');
        if ~isempty( r )
            SIMarchitect_path = item(1:r-1);
            if ~strcmp(SIMarchitect_path, lower(SIMarchitect_root))
                result = 0;
                return;
            end
        end
    end

    result = 1; 
%EOF

