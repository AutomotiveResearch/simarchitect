%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function delete_fcn(block_handle)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
% Description: This function is the default 'DeleteFcn' of the component models in the SIMarchitect library. Purposes:
%	- It removes all FROM blocks, a GOTO block and a bus selector block and the connections for that Module/Area
%   - It resizes the (sub)connector if applicable
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%________________________________________________________________________________________________________
%
% 0. Get some system and block information
%
% Delete order: 
% 1. a) Line between SIMarchitect selector and Module/Area
%    b) SIMarchitect selector
% 2. Delete FROM block that is/was connected to bus_selector
% 3. a) Delete line between Module/Area and GOTO block that is connected to con_o
%    b) Delete GOTO block that is/was connected to con_o
% 4. Check and carry out actions with respect to FROM block(s) that are linked to con_o GOTO block
%________________________________________________________________________________________________________

%________________________________________________________________________________________________________
%
% 0. Get some system and block info
%________________________________________________________________________________________________________
   block_name=get_param(block_handle,'Name'); % the name of the caller block
   system_name=gcs;	%name of the system
   con_i_position=get_param(block_handle,'InputPorts');
   if ~isempty(con_i_position),
       con_i_position=con_i_position(1,:); % NOTE assumes first port is always the con_i port (don't know how to look for tag)
   end
   con_o_position=get_param(block_handle,'OutputPorts');
   if ~isempty(con_o_position),
       con_o_position=con_o_position(1,:); % NOTE assumes first port is always the con_o port (don't know how to look for tag)
   end

%________________________________________________________________________________________________________
% 
% 1. a) Delete line between SIMarchitect bus-selector and Module/Area (if exist)
%    b) Delete SIMarchitect bus-selector
%________________________________________________________________________________________________________
try
    selector=find_system(gcs,'FindAll','on','SearchDepth',1,'Name',['selector_',block_name]);
    ports=get_param(selector,'PortConnectivity');
    if ~isempty(ports(2).DstPort),
        bus_selector_outputport_position=get_param(selector,'OutputPorts');
        delete_line(system_name,bus_selector_outputport_position);
    end
    delete_block(selector)
end
        

%________________________________________________________________________________________________________
%
% 2. Delete FROM block that is/was connected to bus_selector
%________________________________________________________________________________________________________
temp=find_system(gcs,'FindAll','on','SearchDepth',1,'Name',['from_main_',block_name]);
if ~isempty(temp),
    from1_port_position=get_param(temp,'OutputPorts');
    try 
        delete_line(system_name,from1_port_position); 
    end
    delete_block(temp);
end

%________________________________________________________________________________________________________
%
% 3. a) Delete line between Module/Area and GOTO block that is connected to con_o
%    b) Delete GOTO block that is/was connected to con_o
%________________________________________________________________________________________________________
temp=find_system(gcs,'FindAll','on','SearchDepth',1,'BlockType','Goto','Name',['goto_',block_name]);
if ~isempty(temp),
    goto_port_position=get_param(temp,'InputPorts');
    try 
        delete_line(system_name,goto_port_position); 
    end
    delete_block(temp);
end

%________________________________________________________________________________________________________
%
% 4. Handle all things that are related to FROM block(s) that are linked to con_o GOTO block
%________________________________________________________________________________________________________
temp=find_system(gcs,'FindAll','on','SearchDepth',1,'BlockType','From','GotoTag',block_name); %returns block handles
if ~isempty(temp),
    %a) Try to find connector and information
    try
                
        connector=[];
        try 
            connector=find_system(system_name,'SearchDepth',1,'Name','sub_connector'); % returns cell array
            if isempty(connector),
                connector=find_system(system_name,'SearchDepth',1,'Name','main_connector'); % returns cell array
            end
            connector=connector{1}; %change into character array
        end

        % Check whether FROM for Module/Area is connected to (sub)connector!!!
        if ~isempty(connector), 
            connector_ports_old=get_param(connector,'PortConnectivity');  %returns a structure
        end

    % b) Delete all FROM blocks that have reference to Module/Area
        for i=1:length(temp),
            try
                delete_block(temp(i));
            end
        end
        
    % c) Check if FROM Module/Area was connected to (sub)connector
        if exist('connector_ports_old'),
            
            for i=1:length(connector_ports_old)-1, %-1 because last port is the outport of the connector
                from_connected=find(connector_ports_old(i).SrcBlock==temp);

    % d) If FROM Module/Area is connected, start resize, reposition, reconnect procedure
                if ~isempty(from_connected), 
                
                    %delete all lines connected to inports
                    for ii=1:length(connector_ports_old)-1,
                        try
                            delete_line(system_name,connector_ports_old(ii).Position);
                        end
                    end
        
                    % get ports number and reduce with one, with a minimum of 1
                    connector_ports_num=get_param(connector,'Inputs');  %returns a character array with the number of input ports
                    connector_ports_num_new=str2num(connector_ports_num)-1;
        
                    % skip resize connector in case reduced size is zero
                    if connector_ports_num_new > 0, 
                        eval(['set_param(connector,''Inputs'',''',num2str(connector_ports_num_new),''');']);
                        % resize connector
                        connector_pos=get_param(connector,'Position');
                        connector_pos_new=[connector_pos(1),connector_pos(2),max(connector_pos(3)-30,connector_pos(1)+50),connector_pos(4)];
                        set_param(connector,'Position',connector_pos_new);

                        % reposition and reconnect all already connected from blocks
                        connector_ports_new=get_param(connector,'PortConnectivity');  %returns a structure
                        ii=1;
                        for iii=1:length(connector_ports_new)-1, % -1 for the output port
                            src_block=connector_ports_old(ii).SrcBlock;
                            try 
                                src_block=get_param(src_block,'Handle');
                            catch 
                                src_block=-1;
                            end
                            if src_block==-1,
                                ii=ii+1;
                                src_block=connector_ports_old(ii).SrcBlock;
                            end
                            try 
                                if strcmp(get_param(src_block,'BlockType'),'From')|...
                                    strcmp(get_param(src_block,'Tag'),'none'), %reposition only the from blocks or the constant 0, not the user added blocks
                                    set_param(src_block,'Position',...
                                    [connector_ports_new(iii).Position(1)-15,connector_ports_new(iii).Position(2)+20,connector_ports_new(iii).Position(1)+15,connector_ports_new(iii).Position(2)+40]);
                                end
                            end
                            try
                                src_port_nr=connector_ports_old(ii).SrcPort+1; %+1 because first port is assigned 0
                                src_port_position=get_param(src_block,'OutputPorts');
                                src_port_position=src_port_position(src_port_nr,:);
                                line=add_line(system_name,[src_port_position;connector_ports_new(iii).Position]);
                            end
                            ii=ii+1;
                        end
  
                        %reposition everything connected to output of connector
                        connector_outport_shift = connector_ports_old(end).Position(1) - connector_ports_new(end).Position(1);            
                        for i=1:length(connector_ports_new(end).DstBlock),
                            pos=get_param(connector_ports_new(end).DstBlock(i),'Position');
                            pos(1)=pos(1)-connector_outport_shift;
                            pos(3)=pos(3)-connector_outport_shift;
                            set_param(connector_ports_new(end).DstBlock(i),'Position',pos);
                        end
                        delete_line(system_name,connector_ports_new(end).Position);
                        connector_name=get_param(connector,'Name');
                        for i=1:length(connector_ports_new(end).DstBlock),
                            DstBlock=get_param(connector_ports_new(end).DstBlock(i),'Name');
                            line=add_line(system_name,[connector_name,'/1'],[DstBlock,'/1'],'autorouting','on');
                        end
                    end

                end % End of d)
                
            end % End of 'for' c)
            
        end % End of 'exist('...')' c)
            
    end % End of a)
    
end % End of 4)
