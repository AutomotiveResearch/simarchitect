%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function path_found = is_path_added (directory)

p          = lower(path);
path_found = 0; 

directory = lower(directory);
directory = strrep(directory, '\\', '\');
directory = strrep(directory, '//', '/');

regdir = directory;
regdir = regexprep(regdir, '\\+$', '');
regdir = regexprep(regdir, '\/+$', '');
regdir = strrep(regdir, '\\', '\');
regdir = strrep(regdir, '//', '/');
regdir = strrep(regdir, '/', '\/');
regdir = strrep(regdir, '\', '\\');

str = {['^', regdir, ';'], [';', regdir, ';'], [';', regdir,'$']};
res = regexp(p, str);

notFound = isempty(res{1}) && isempty(res{2}) && isempty(res{3});

if ~notFound
    path_found = 1;
end
