%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function stop_fcn(varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
% Description: This function is the 'StopFcn' of the model. Functions:
%   1. renames an existing structure 'data_set' to 'data_set1' 
%   2. moves the structure 'parameters' to the structure 'data_set.parameters'
%   3. handles the output data and saves the data into a structure named 'data_set.output'
%   4. gets some info about simulation settings and stores these in data_set.info
%   5. cleans up the caller Workspace
%
% When calles with the argument 'pause' (stop_fcn pause) it doesn't remove the 'yout' from the workspace.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%________________________________________________________________________________________________________
%% ISSUES
% - deal with characters in signal names that are not allowed in variable names? %done for numbers (JE 10-09-01)
%________________________________________________________________________________________________________
t_sim=toc; %to asses simulation time

%% 
%display some output
disp('___________________SIMarchitect____________________________');
disp(['The simulation time was: ',num2str(round(t_sim*10)/10),'[s]']); 
disp(['']);


%check if output available
if evalin('caller',['exist(''yout'')'])~=1 | evalin('caller',['isempty(yout.signals.values)'])==1,
    disp('Warning: No output available in workspace to process');
    disp('______________________________________________________');
    return
end

%create yout within this WS;
% evalin('caller',['assignin(''caller'',''yout'',yout);']);
% % yout = evalin('base','yout');
yout = evalin('caller','yout');

%rename existing 'data_set' if it exists
if evalin('caller',['exist(''data_set'',''var'')']),
    evalin('caller',['data_set1=data_set; clear data_set']);    %not to mix up data
    disp(sprintf(['Renamed the existing structure ''data_set'' to ''data_set1'',\n',...
            'newly generated output exists in ''data_set.output''\n']))
end

%process the generated output
try
    %sub-function, gets cell array of chars with names of selected signals
    [selected_signals]=get_selected_signals( length(yout.signals.values(1,:)) );
    
    
    %% Create the output structure
    %add all signals and the values to the structure output
    for i=1:length(selected_signals),
        var=char(selected_signals{i});
        try %check if string is allowed in a structure, otherwise modify it            
            eval(['data_set.output.',var,'=yout.signals.values(:,',num2str(i),');']);
        catch
            var = modify_name(var,'signal');
            eval(['data_set.output.',var,'=yout.signals.values(:,',num2str(i),');']);
        end        
    end
    %add the time vector to the output structure
    data_set.output.t=yout.time;
    
catch
    warndlg('Could not correctly execute post process function, error in assigning output to data_set.');
end


%add structure with parametervalues to the data_set-structure
try
%     parameters = evalin('base','parameters');    %get the structure in this WS
    parameters = evalin('caller','parameters');    %get the structure in this WS
%     evalin('caller',['assignin(''caller'',''parameters'',parameters);']);    %get the structure in this WS
    data_set.parameters=parameters;
catch
    disp('Parameter-structure could not be saved in the data-structure')
end

%add some info to the data_set-structure
try
    t_stop=[num2str(round(data_set.output.t(end)*100)/100),' [s]'];
    data_set.info=get_info(bdroot(gcs),t_sim,t_stop);
catch
    disp('ERROR in stop_fcn while adding the ''info'' to the data_set structure')
end

disp('______________________________________________________');

assignin('caller','data_set',data_set); %create structure data_set in the caller workspace
% assignin('base','data_set',data_set); %create structure data_set in the caller workspace

%% Cleaning up
% If during a simulation the pause button is pressed, the output can be made available by typing
% "stop_fcn pause" at the command line. In this case we don't want to delete the variable "yout"
% because it is needed when the simulation is continued. Otherwise, delete the variable "yout"
if nargin>0,
    if varargin{1}=='pause',
        return
    end
else
    evalin('caller',['clear yout']);
end

%% See whether there is a graph_eval figure and whether it needs to be auto-updated
h= findall(0,'Tag','graph_eval_figure');
for i=1:length(h),
    ud = get(h(i),'Userdata');
    if ~isempty(ud)
        if ud.auto_refresh,
            set(ud.handles.fig,'Handlevisibility','on');
            graph_eval('refresh','',ud);
            set(ud.handles.fig,'Handlevisibility','callback');
        end
    end
end
 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   SUBFUNCTIONS
function [selected_signals,local]=get_selected_signals(output_vectors_num)     %get selected signal names from bus-selector
%global local yout

handle=find_system(bdroot(gcs),'Tag','output_selector');
[selected_signals selected_signals_num] = get_selection(handle); % get a cell array with where the selected signals are listed (not expanded list!)

% check whether the number of selected signals matches the number of vectors in YOUT. If not than expand signals.
if output_vectors_num~=selected_signals_num,
    %More output-vectors than selected signals, so expand cell array 'selected_signals'
    bus_signals=get_param({[handle{1},'/BusSelector']},'InputSignals'); % returns the names of the signals in the bus in a cell-array -structure    
    [j bus_signals_list]=strip_signal(bus_signals);                   % bus_signals_list is a cell array with all signals in the bus (sub-function) (j is not used here)
    selected_signals=expand(selected_signals,bus_signals_list);       % returns full expanded list of selected signals listed in a cell array (sub-function)        
    selected_signals_num=length(selected_signals);                      % the real total number of selected signals (expanded)
end

%do a re-check
if output_vectors_num~=selected_signals_num,
    disp('Error: the number of signals in the output_selector doesn''t match the number of columns in the yout.signals.values array ');
    output=[];
    return %forced end of function
end

%end subfunction get_output


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [selected_signals,selected_signals_num] = get_selection(handle);

selected_signals=get_param({[handle{1},'/BusSelector']},'OutputSignals');    %the labels, one cell array with one charracter array
selected_signals=selected_signals{1}; %character array    

%convert character array into cell array
selected_signals_num=length(findstr(selected_signals,','))+1;   %the NOT expanded number of selected signals

%the first selected signal
[temp{1} rem]=strtok(selected_signals,',');

%the 2nd until the last-1 selected signal
for i=2:(selected_signals_num-1),
    [temp{i} rem]=strtok(rem,',');
end

%the last selected signal
if selected_signals_num>1,
    temp{selected_signals_num}=rem(2:end);	%cell-array with signal names
end
selected_signals = temp;

%end subfunction [selected_signals,selected_signals_num] = get_selected_signals(handle);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [j,vars]=strip_signal(cell_array,string,vars,j)
% This function strips the cell array that is obtained when the signal input of the output selector is obtained
if nargin==1,
    %during first call
    j=1;
    vars=[];
    string=[];
end

[height,width]=size(cell_array);
for i=1:height,
    if width==2,    
        % if width is 2, 
        %   1st cell contains the name of the level
        %   2nd cell contains the levels below (this is also a cell array)
        string=[string,cell_array{1},'.'];
        vars{j,1}=[string];
        j=j+1;
        [j,vars]=strip_signal(cell_array{2},string,vars,j); %continue exploring 2nd cell
        
    elseif iscell(cell_array{i}),
        %explore this cell array
        [j,vars]=strip_signal(cell_array{i},string,vars,j);
        
    else,
        %we're now at signal level
        vars{j,1}=[string,cell_array{i}];
        j=j+1;
    end
end

%end subfunction [j,vars]=strip_signal(cell_array,string,vars,j)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function expanded_sel_list=expand(not_expanded_sel_list,expanded_bus_list)
expanded_sel_list={};


%create expanded selection list
num_signals_in_bus = length(expanded_bus_list);
for i=1:length(not_expanded_sel_list),
    flag=0;

    indx = find(strcmp(expanded_bus_list,not_expanded_sel_list{i}));
    if ~isempty(indx),  %found the exact signal in the bus list
        expanded_sel_list{end+1}=expanded_bus_list{indx}; %add it to the selection list
        flag=1; %indicates that selected signal is found and added.
    end
    
    if ~flag,
        indx=find(strcmp(expanded_bus_list,[not_expanded_sel_list{i},'.']));
        if ~isempty(indx),                                          %matching string indicates that there is at least one level below the selected signal
            while ~flag & indx<num_signals_in_bus,                  %explore down the list to get all signals below selected signal level
                str = expanded_bus_list{indx+1};                    %select following string STR in cell array of expanded_bus_list
                if ~isempty(strfind(str,not_expanded_sel_list{i})), %if STR starts with the same string as the selected signal
                    if ~strcmp(str(end),'.'),                       %if STR doesn't end with a '.', add it to the selection list, otheriwse this indicates that there is a level below STR, so don't add it to the expanded_sel_list but look 1 further.
                        expanded_sel_list{end+1}=str;               %add it to the selection list
                    end
                    indx=indx+1;
                else
                    flag=1;
                end
            end %while ~flag,
        end  %if ~isempty(indx)
    end %if ~flag,
end

%end subfunction expanded_list=expand(not_expanded_sel_list,expanded_bus_list)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function info=get_info(varargin)
if nargin==3,
    system=varargin{1};
    simtime=varargin{2};
    StopTime=varargin{3};    
elseif nargin==2,
    system=varargin{1};
    simtime='??';
    StopTime=varargin{2};        
else
    info=[];
    return
end

info.ModelName=get_param(system,'Name');
info.FileName=get_param(system,'FileName');
info.ModelVersion=get_param(system,'ModelVersion');
info.LastModifiedDate=get_param(system,'LastModifiedDate');
info.StartTime=[get_param(system,'StartTime'),' [s]'];
info.StopTime=StopTime; %[num2str(round(evalin('caller',['data_set.output.t(end)'])*100)/100),' [s]'];
info.Solver=get_param(system,'Solver');
if strmatch(info.Solver,char('FixedStepDiscrete','ode1','ode2','ode3','ode4','ode5')),
    %FixedStep solver
    info.FixedStep=get_param(system,'FixedStep');    
    info.SolverMode=get_param(system,'SolverMode');
else
    %Variable step solver
    info.RelTol=get_param(system,'RelTol');
    info.AbsTol=get_param(system,'AbsTol');
    info.MaxStep=[get_param(system,'MaxStep'),' [s]'];
    info.MinStep=[get_param(system,'MinStep'),' [s]'];
    info.InitialStep=[get_param(system,'InitialStep'),' [s]'];
    info.Refine=get_param(system,'Refine');
    info.OutputOption=get_param(system,'OutputOption');
end

try
    info.OutputSampleTime=[get_param(char(find_system(bdroot(system),'BlockType','ToWorkspace','Tag','output_tw')),...
            'SampleTime'),' [s]']; %find the ToWorkspace block with tag 'output_tw' and get sample time
catch
    info.OutputSampleTime='??';
end
info.SimulationTime=[num2str(simtime),' [s]'];
info.Date=datestr(now,1);
info.Time=datestr(now,13);
info.SIMarchitect_version=SIMarchitect_info('version');