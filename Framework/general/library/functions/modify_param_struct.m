%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function [bool,parameters]=modify_param_struct(parameters,param2modify,value)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%
% Description: This function is used to modify the structure called 'parameters' which must be available in the
%   workspace.
%   MODIFY_PARAM_STRUCT(PARAMETERS,PARAM2MODIFY,VALUE) modifies the value(s) of the parameter(s) (strings!) in the cell-array 
%   PARAM2MODIFY. The values are taken from the cell array VALUE.
%
%   When the PARAM2MODIFY contains the parameter .....Parameterfile it loads the parameterfile and defines all
%   variables that are declared in the parameterfile, in the structure parameter on the same level as the variable
%   Parameterfile.
%
%   This function is called from the batch run template (available in the 'useful' directory
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% param2modify= a cell array with strings (or a single string)
% value = a cell arra of the same dimensions as 'param2modify' with the corresponding values

bool=1; %indicates that there are no errors, if errors occure, bool is set to -1
try
    %% Check whether dimensions of param2modify and value are identical
    if size(param2modify)==size(value),
        num_param=length(param2modify); %the number of parameters to be modified
    else
        disp('The dimensions of the ''param2modify'' cell array must be identical to the ''value'' cell array')
        bool = -1;
        return
    end
    
    %% Start loop to modify data
    for i=1:num_param,
        param=param2modify{i};
        if ~strcmp(param(end-12:end),'Parameterfile'),
            eval([param,'=value{i};']); %modify the value of the parameter
        else
            %modify all parameters that are declared in a parameterfile (NOT the parameterfile itself!)
            eval([param,'=''',value{i},''';']); %modify the parameter Parameterfile
            subname=param(1:end-14);        %get the name of the parent-layer of the structure .....Parameterfile
            
            [parameters,bool]=load_parameters(parameters,value{i},subname);
            if bool==-1,
                return
            end
        end
    end
catch
    bool=-1;
end

%modify the values in the parameter-structure where the modifications are in a m-file
function [parameters,bool]=load_parameters(parameters,parameterfile,subname)

try,
    bool=1;
    eval(parameterfile);    %run the parameterfile
    eval([subname,'.ParameterfileExt=''',char(parameterfile),'.m'';']);  %register the name of the parameterfile
    clear parameterfile
catch,
    disp(['ERROR: File ''',parameterfile,''' cannot be loaded.']);
    disp(['error message: ',lasterr]);
    bool=-1;
    return
end;

loaded_vars.name=who;
for i=1:length(loaded_vars.name),
    if ~strcmp(char(loaded_vars.name{i}),'subname')&~strcmp(char(loaded_vars.name{i}),'parameters'),
        eval([subname,'.',char(loaded_vars.name{i}),'=',loaded_vars.name{i},';']);
    end
end
