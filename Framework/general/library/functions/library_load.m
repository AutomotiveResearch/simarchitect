function [] = library_load()
if exist('SIMarchitect_root.m','file')
    if strcmp(pwd,SIMarchitect_root)
        mydir  = pwd;
        idcs   = strfind(mydir,'\');
        newdir = mydir(1:idcs(end)-1);
        newdir = strcat(newdir,'\Library');

        direct = dir(newdir);
        direct = direct(3:end,:);
        disp('_______________________Loading Library Database_______________________');

        for i = 1:length(direct)
             inst_dir = strcat(newdir,'\',getfield(direct(i),'name'));
             if exist(strcat(inst_dir,'\installed.txt'),'file')
                addpath(inst_dir);
                message = strcat(getfield(direct(i),'name'),' Library Loaded');
                disp(message)
             end
        end
    end
end
end

