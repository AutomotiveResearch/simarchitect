%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}

function obj = IniFile ( file, multiple )
%
%   Creates an IniFile instance.
%
%   An IniFile consists of sections and key-value pairs. The first section is
%   called 'global' and the rest must be named by using brackets, like
%   '[section_name]'. An example of an inifile can look like:
%
%   [global] ; optional, it will be interpreted as global.
%
%   key1 = value1
%   key2 = value2
%
%   [section1] ; comments is given by ;
%   ; and can be at the beginning of a line too.
%   key1 = value1
%   key2 = value2
% 
%   Example usage:
%     ini = IniFile('settings.ini');
%
%     % Read from the global section
%     pos_x = get(ini, 'pos_x'); % Without the section it will use global.
%     pos_y = get(ini, 'global', 'pos_y');
%
%     % Set the new positions
%     set(ini, 'pos_x', pos_x_new);
%     set(ini, 'global', 'pos_y', pos_y_new);
%     
%     % Save the new values to the ini file. When no file is given at
%     % construction an error is initiated. A file can be given to saveFile
%     % method.
%     saveFile(ini);
%
%   Note: Every method contains its own documentation, please check these
%   before usage.
%


if nargin == 0
    file     = '';
    multiple = 0;
end

if nargin == 1
    multiple = 0;
end

obj.contents = struct;
obj.file     = file;
obj.multiple = multiple;

obj = class( obj, 'inifile' );

if nargin ~= 0 && exist(file, 'file') == 2
    obj.contents = processFile( obj, obj.file );
end
