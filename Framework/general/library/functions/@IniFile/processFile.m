%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}

function str = processFile( obj, file ) %#ok<INUSL>
% PRIVATE METHOD - helper function.
% read the given ini file and returns it structure.
%
% Syntax: structure = processFile( obj, file )
%

str = struct;

fid = fopen(file); % Reading set as default!

if fid == -1
    warning(['IniFile/', mfilename, ': could not open given file ''', file, '''.']); %#ok<WNTAG>
    return;
end

line    = fgetl(fid);
section = 'global'; 

while  ischar(line)
        
    % Removing comments starting with % or ;
    line = regexprep(line, '[;%].*$', '');

    % Trimming of the line
    line = trim(line);
    
    if ~strcmp(line, '')
    
        % Checking wheter it is a key value pair.
        pos = findstr(line, '=');
        if pos
            key   = trim( line(1:pos-1) );
            value = trim( line(pos+1:end) );
                        
            if obj.multiple
                if isfield(str, section) && isfield(str.(section), key) && iscell(str.(section).(key))
                    str.(section).(key){end+1} = value;
                    
                else
                    if  isfield(str, section) && isfield(str.(section), key)
                        temp                       = str.(section).(key);
                        str.(section).(key)        = {};
                        str.(section).(key){end+1} = temp;
                        str.(section).(key){end+1} = value;
                        
                    else 
                        str.(section).(key) = value;
                    end
                end
                
            else
                str.(section).(key) = value;
            end
        end
    
        % Checking wheter it is a section
        if regexp(line, '\[\w+\]')
            section = line(2:end-1);
        end
        
    end
    
    line = fgetl(fid);
end

fclose(fid);

function str = trim ( str )

str = regexprep(str, '^\s*', '');
str = regexprep(str, '\s*$', '');
