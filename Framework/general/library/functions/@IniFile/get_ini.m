%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}

function value = get_ini ( obj, section, key )
% Returns the value of the given section and key pair.
%
% Syntax: value = get ( obj, section, key )
%
% Returns the key of the global section.
% Syntax: value = get ( obj, key )
%
% Returns the total structure of the ini file
%         given as str.section.key.
% Syntax: value = get ( obj )

value = '';

if nargin == 1
    value = obj.contents;
    return;
end

if nargin == 2
    key     = section;
    section = 'global';
end

if isfield( obj.contents, section ) && isfield( obj.contents.(section), key )
    value = obj.contents.(section).(key);
        
else
    %disp(['IniFile/get: ', section, '-', key, ' does not exist!']);
end
