%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function move_fcn(block_handle)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
% Description: This function is the default 'MoveFcn' of the component models in the SIMarchitect library. Purposes:
%	- It places: 1 from block, a goto block and a bus selector block in the right position relative to the component block
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% get some info
block_position=get_param(block_handle,'Position'); %position of caller block 
%e.g [left top right bottom] (the four corners in pixels on the screen) (top left is 0,0)
block_name=get_param(block_handle,'Name'); % the name of the caller block
block_mask=get_param(block_handle,'Mask');
system_name=gcs;	%name of the system

%%find first output port and first input port
%IT IS ASSUMED THAT THESE ARE THE CON_O RESP. CON_I ports!!!
ports=get_param(block_handle,'PortConnectivity');
con_o_port=[];
con_i_port=[];
for i=1:length(ports),
    if ports(i).Type=='1',
        if ~isempty(ports(i).SrcBlock),
            con_i_port=ports(i);
        elseif ~isempty(ports(i).DstBlock),
            con_o_port=ports(i);
            break
        end           
    end
end


%________________________________________________________________________________________________________
%% Move 1 goto block and 1 from block and selector and connect them
%________________________________________________________________________________________________________
%%%%move bus_selector block to be connected to con_i port
try
    newposition_bus_selector=[block_position(1),block_position(2)-5-15,block_position(1)+10,block_position(2)-5];
    
    if strcmp(get_param(con_i_port.SrcBlock,'Tag'),'SIMarchitect_selector'),
        set_param(con_i_port.SrcBlock,'MoveFcn','');    %switch off move fcn
        set_param(con_i_port.SrcBlock,'Position',newposition_bus_selector);
        set_param(con_i_port.SrcBlock,'MoveFcn','move_fcn_sub(gcbh)'); %switch on move fcn again
        %% make sure line is 'neat'
        line=find_system(system_name,'FindAll','on',...
            'Type','line',...
            'SrcBlockHandle',con_i_port.SrcBlock);
        if ~isempty(line),
            bus_selector_outputport_position=get_param(con_i_port.SrcBlock,'Outputports');
            set_param(line,'Points',[bus_selector_outputport_position;bus_selector_outputport_position(1)-10,...
                    bus_selector_outputport_position(2);con_i_port.Position]);
        end
    end
end
%________________________________________________________________________________________________________
%%%%move from block to be connected to bus_selector
try
    if strcmp(block_mask,'on'),
        newposition_from1=[block_position(1)+25,block_position(2)-5-15,block_position(1)+50,block_position(2)-5];
        SrcBlock=[];
        if~isempty(con_i_port),
            x=get_param(con_i_port.SrcBlock,'PortConnectivity');
            try
                if ~isempty(x(1).SrcBlock)& strcmp(x(1).Type,'1'),
                    SrcBlock=x(1).SrcBlock;  %input port of busselector
                end
            end
        end
        if strcmp(get_param(SrcBlock,'Tag'),'SIMarchitect_from1'),
            set_param(SrcBlock,'MoveFcn','');                   %switch off
            set_param(SrcBlock,'Position',newposition_from1);
            set_param(SrcBlock,'MoveFcn','move_fcn_sub(gcbh)'); %switch on
            %% make sure line is 'neat'
            if ~isempty(SrcBlock),
                line=find_system(system_name,'FindAll','on',...
                    'Type','line',...
                    'SrcBlockHandle',SrcBlock);
                if ~isempty(line),
                    from1_port_position=get_param(SrcBlock,'Outputports');
                    bus_selector_inputport_position=get_param(con_i_port.SrcBlock,'Inputports');
                    set_param(line,'Points',[from1_port_position;bus_selector_inputport_position]);
                end
            end
        end
        
    else    %a non masked system (has no bus_selector)
        newposition_from1=[block_position(1),block_position(2)-5-15,block_position(1)+25,block_position(2)-5];
        if ~isempty(con_i_port),
            SrcBlock=con_i_port.SrcBlock;
        else
            SrcBlock=[];
        end
        
        if strcmp(get_param(SrcBlock,'Tag'),'SIMarchitect_from1'),
            set_param(SrcBlock,'MoveFcn','');                   %switch off
            set_param(SrcBlock,'Position',newposition_from1);
            set_param(SrcBlock,'MoveFcn','move_fcn_sub(gcbh)'); %switch on
            
            %% make sure line is 'neat'
            if ~isempty(SrcBlock),
                line=find_system(system_name,'FindAll','on',...
                    'Type','line',...
                    'SrcBlockHandle',SrcBlock);
                if ~isempty(line),
                    from1_port_position=get_param(SrcBlock,'Outputports');
                    set_param(line,'Points',[from1_port_position; from1_port_position(1)-10,...
                            from1_port_position(2);con_i_port.Position]);
                end
            end
            
        end
    end
end
%________________________________________________________________________________________________________
try
    %%%%move goto block to be connected to con_o port
    newposition_goto=[block_position(3)-25,block_position(2)-5-15,block_position(3),block_position(2)-5];
    if~isempty(con_o_port),
        if strcmp(get_param(con_o_port.DstBlock,'Tag'),'SIMarchitect_goto'),
            set_param(con_o_port.DstBlock,'MoveFcn','');                   %switch off
            set_param(con_o_port.DstBlock,'Position',newposition_goto);
            set_param(con_o_port.DstBlock,'MoveFcn','move_fcn_sub(gcbh)'); %switch on        
            
            
            %% make sure line is 'neat'
            line=find_system(system_name,'FindAll','on',...
                'Type','line',...
                'DstBlockHandle',con_o_port.DstBlock);
            if ~isempty(line),
                goto_port_position=get_param(con_o_port.DstBlock,'InputPorts');
                set_param(line,'Points',[con_o_port.Position;con_o_port.Position(1)+10,con_o_port.Position(2);goto_port_position]);
            end
            
        end
    end
end
%________________________________________________________________________________________________________