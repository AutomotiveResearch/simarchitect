%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function persist_workdir( action )
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%
% Usage: persist_workdir;
%        persist_workdir('forget');
%
% Description:
% The first time this function is called it will store the working
% directory at that moment. Everytime it will be called after, it will
% change to the working directory persistently. When the 'forget' option is
% used, the working directory is forgotten. The next time it is called it
% will start over as if it was "the first time".
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The first working directory in which SIMarchitect is called/runned has to be
% memorized in order to get back to this directory when the user ends
% simulation. Starting a simulation the working directory is changed to
% the directory of the model. When ending the SIMarchitect model it will call
% SIMarchitect.m again and then the working directory is changed back to the
% working directory the user left off before opening the model.
% When the GUI is closed manually the persistent variable is made empty!

if nargin == 0
    action = 'persist';
end

persistent SIMarchitect_WORKING_DIRECTORY;

if strcmp(action, 'forget')
    SIMarchitect_WORKING_DIRECTORY = [];
    
else
    if isempty(SIMarchitect_WORKING_DIRECTORY)
        SIMarchitect_WORKING_DIRECTORY = pwd;

    else
        cd(SIMarchitect_WORKING_DIRECTORY);
    end
end
