%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function name_out=modify_name(name_in,type)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
% Description: This function checks whether a string contains charactes that are not allowed for structures.
%              When this is the case, the string is modified. The not allowed characters are replaced by a '_'.
%              NAME_IN should be a char array with the name that has to be converted.
%              TYPE should be string with either 'parameter' or 'signal', indicating whether it is a parameter
%              or signal that is being converted.
%   This function is called from the mask_fcn and the stop_fcn.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%________________________________________________________________________________________________________


name_out=name_in;
points=findstr('.',name_out);
num_points=length(points);

% If a field of the structure:
% 1) starts with a number we're going to put an 's' in front of this field
% 2) contains spaces we're going to replace this by '_'
% 3) contains any of the following characters !@#$%^&*()~`'":;?></\|}{[] these will be replaced by '_'


%%% 1)
num=0;
%check first field for number at the start
if ~isempty(str2num(name_out(1)))&~strcmp(name_out(1),'i'),    
    name_out=['s',name_out];
    num=num+1; 
end

if num_points>1,
    %check other fields for number at the start
    for ii=1:length(points)-1,
        if ~isempty(str2num(name_out((points(ii)+1+num))))&~strcmp(name_out((points(ii)+1+num)),'i'),    
            name_out=[name_out(1:points(ii)+num),'s',name_out(points(ii)+1+num:end)];
            num=num+1;
        end
    end
end

%check last field for number at the start
if num_points>=1,
    if ~isempty(str2num(name_out((points(end)+1+num))))&~strcmp(name_out((points(end)+1+num)),'i'),     
        name_out=[name_out(1:points(end)+num),'s',name_out(points(end)+1+num:end)];
    end 
end

%%%% 2)    
%% Check whole string for spaces and replace with '_'
spaces = find(isspace(name_out)); %indices of space(s)
if ~isempty(spaces),
    name_out(spaces) = '_';
end

%%%% 3)
error_char = [char(33:45), char(47), char(58:64), char(91:94), char(96), char(123:126)]; %['''"!@#$%^&*()~`:;?></\|}{[]-+=']; %list of not allowed characters
for ii=1:length(error_char),
    name_out=strrep(name_out,error_char(ii),'_'); %replace not allowed characters by '_'
end

if ~strcmp(name_in,name_out),
    msgtext=strvcat('At least one signal has been renamed because it contained not allowed special characters.' ,...
                    'Please see the Matlab Command Window for a more detailed specification.');
    msgbox(msgtext,'WARNING: Signal rename','warn','replace');
    disp(['Renamed ',type,': ''',name_in,''''])
    disp(['     to ',type,': ''',name_out,''''])
    disp([' '])
    disp(['     Because a fieldname in a structure cannot start with a number,']); 
    disp(['     contain spaces, or contain any of the following characters: ']);
    disp(['     ',error_char]);
    disp([' '])
end
