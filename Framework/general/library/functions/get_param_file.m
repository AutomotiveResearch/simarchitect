%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function [Pfile,directory]=get_param_file(BlockHandle,parameterfile)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%
% Description: [PFILE DIRECTORY] = GET_PARAM_FILE(BLOCKHANDLE,PARAMETERFILE)
%              This function returns the name of the parameterfile and the directory where the file is located. Input
%              is either the BlockHandle of an SIMarchitect module or the name of the parameterfile that is on the mask.
%              The following rules are followed for finding the parameterfile:
%              1) Checks if model has a working directory. Then tries to find parameterfile in working directory.
%              2) If no working directory, it tries to find it on anywhere on the matlab path (takes first hit).
%
%              If error it returns '-1' for both 'Pfile' and 'directory'
%              Normally Pfile and directory are character arrays.
%              This function is called from: busselect_fcn.m and mask_fcn.m
%
% Revision history see end of file.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%try to get parameterfile name
if isempty(parameterfile), %we should have a BlockHandle
    allparameterfile = get_param(BlockHandle,'MaskValues'); %ASSUMING THAT THE FIRST MASKENTRY IS THE NAME OF THE PARAMETERFILE!!
    parameterfile = char(allparameterfile(1));
end

%try to find name of work_dir
try
    work_dir=get_param(find_system(bdroot(gcs),'Tag','ParameterSetting'),'MaskValues');
    work_dir=char(work_dir{1});
    if strcmp(work_dir(end),'\'),
        work_dir=work_dir(1:end-1);
    end
catch
    work_dir=[];
end

%find the parameterfile
if ~isempty(work_dir),    
    if exist([work_dir,'\',parameterfile,'.m'],'file')==2,  % '.m' is necessary here because we might not be in p-file directory
        Pfile = parameterfile;
        directory = work_dir;
    else
        disp('___');
        disp(['ERROR: File ''',parameterfile,''' not found in parameterfile-directory.']);
        disp(['   Looking for: ',work_dir,'\',parameterfile]);
        disp('___');
        
        %% provide output and return                
        Pfile = -1; %indicates error 
        directory = -1;
    end
    
else
    Pfile=which([parameterfile,'.m']);    %try to find parameterfile from where-ever it might be found on the Matlab path
    if isempty(Pfile),
        Pfile = -1;
        disp('___');
        disp(['ERROR: File ''',parameterfile,''' not found in parameterfile-directory or in Matlab path.']);
        disp('___');

    else
        disp('___');        
        disp(['A working directory for this model could not be found.']);
        disp(['Used the following parameterfile: ',Pfile]);    
        disp('___');        
        [Pfile directory] = strtok(fliplr(Pfile),'\');
        Pfile = fliplr(Pfile(3:end)); %skip the .m
        directory = fliplr(directory);
    end    
end

