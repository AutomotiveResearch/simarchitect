%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function name_change_fcn(block_handle)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%
% Description: Is called when the name of an SIMarchitect component is changed. It then changes the name of the 'from_main'
% , the 'selector', the 'goto' and the 'from', accordingly. If one of these blocks can't be found, it is skipped.
% It ASSUMES that the con_i port is the first input port and that the con_o port is the first output port!
%
% the input argument BLOCK_HANDLE is supposed to be a handle to a simulink block
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%________________________________________________________________________________________________________
%
% Make sure that the name for the block is allowed
% 1) check first character
% 2) check for special characters (also enter)
% 3) check for spaces and change them to '_'
%________________________________________________________________________________________________________

block_name=get_param(block_handle,'Name'); % the name of the caller block
signal_name=char(get_param(block_handle,'OutputSignalNames'));
signal_name=signal_name(1,:);

%%% 1) NAME LENGTH
% Check for number of characters. The maximum for references in FROM and GOTO is 31 characters. 
if length(block_name)>31,
    set_param(block_handle,'Name',signal_name);
    msgtext=strvcat('Maximum length for block name is 31 characters.');
    msgbox(msgtext,'WARNING: Length of block name','warn');
    return
end


%%% 2) SPECIAL CHARACTERS (incl. numbers) at 1st position of block name
% Check if name starts with special character
% If so, do not allow a name change (reinstall old name)
if (block_name(1)<65) | (block_name(1)>90 & block_name(1)<97) | (block_name(1)>122) %first character must be A..Z or a..z (especially for FROM and GOTO block references)
    set_param(block_handle,'Name',signal_name);
    msgtext=strvcat( 'Blockname should begin with a..z or with A..Z' ,...
                     'Block name has not been changed.' ,...
                     'Please see Matlab Command Window for additional information');
    msgbox(msgtext,'ERROR: Block rename','warn');
    disp('ERROR: Blockname should begin with a..z or with A..Z');
    disp('Blockname should start with any one of the following characters: ~!@#$%^&*()_-+=[]{}/\|;:",.<>?'' or <enter>');
   return
end

%%%% 3) SPECIAL CHARACTERS in name
error_char = [char(0:31), char(33:47), char(58:64), char(91:94), char(96), char(123:126), char(127:255)]; %list of characters that are not allowed (especially for FROM and GOTO blocks)  
for ii=1:length(error_char),
    character_check=find(block_name==error_char(ii)); % indices of special characters
    if ~isempty(character_check),
        set_param(block_handle,'Name',signal_name);
        msgtext=strvcat('Blockname should not contain any of the following special characters:' ,...
                        '"!@#$%^&*()~`:;?></\|}{[]-+='' or <enter>');
        msgbox(msgtext,'ERROR: Block rename','warn');
        disp('ERROR: Blockname should not contain one of the following characters: ''"!@#$%^&*()~`:;?></\|}{[]-+= or <enter>');
        return
    end
end


%%% 4) SPACES
% Check whole string for spaces and replace with '_'
spaces = find(isspace(block_name)); %indices of space(s)
if ~isempty(spaces),
   block_name(spaces) = '_';
   set_param(block_handle,'Name',block_name);
   msgtext=strvcat( 'Blockname should not contain spaces.' ,...
                    'Spaces have been replaced with ''_''.');
   msgbox(msgtext,'WARNING: Spacing replaced','warn');
end



%________________________________________________________________________________________________________
%
% Find all tags, names, etc. that are related to the block name and change them
%________________________________________________________________________________________________________


%% get some info
block_position=get_param(block_handle,'Position'); %position of caller block 
%e.g [left top right bottom] (the four corners in pixels on the screen) (top left is 0,0)
block_mask=get_param(block_handle,'Mask');
system_name=gcs;	%name of the system

%% get the con_i port and the con_o port.
ports=get_param(block_handle,'PortConnectivity');
if ~isempty(ports),
con_i_port=ports(1); % NOTE assumes first port is always the con_i port (don't know how to look for tag)
flag=1; i=2;
while flag
    if str2num(ports(i).Type)==1, % NOTE assumes first port is always the con_o port (don't know how to look for tag)
        flag=0;
        index=i;
    else
        i=i+1;
    end
end
    
    if flag, % we're not sure whether first port is an in or out port (If there are no input ports than it is the first output port)
        if ~isempty(ports(1).SrcBlock),
            con_i_port=ports(1);
            con_o_port=[];
        elseif  ~isempty(ports(1).DstBlock),
            con_i_port=[];
            con_o_port=ports(1);    % the first port is an output port
        else
            return %nothing connected to first IO port and there is no other one to check
        end    
    else
        con_i_port=ports(1);
        con_o_port=ports(index);
    end
else
    return %there are no ports
end



%% change stuff connected to con_o port & the FROM block connected to the (sub-)connector
if ~isempty(con_o_port),

    if ~isempty(con_o_port.DstBlock),
        % GOTO block that is associated with the Module
        goto_block=find_system(system_name,'SearchDepth',1,'GotoTag',signal_name,'BlockType','Goto'); %NOTE only look at current level in model!!! returns a cell array
        if ~isempty(goto_block),
            set_param(goto_block{1},'Name',['goto_',block_name],'GotoTag',block_name);
        end
        
        % line connected to con_o port
        line=find_system(gcs,'SearchDepth',1,'FindAll','on','Type','line','Name',signal_name);
        if ~isempty(line), 
            set_param(line,'Name',block_name);  %rename line
        end   

%%% this part only adds a signal_name to the line, it does not solve the incorrect propagation of signal on the con_o !!!
%        % line connected to GOTO block (in case con_o_port and GOTO are not connected directly)
%        goto_block_ren=find_system(system_name,'SearchDepth',1,'GotoTag',block_name,'BlockType','Goto'); %NOTE only look at current level in model!!! returns a cell array
%        goto_port=get_param(goto_block_ren{1},'PortConnectivity');
%        if isempty(strmatch(block_handle,goto_port.SrcBlock)),
%            line2=find_system(gcs,'SearchDepth',1,'FindAll','on','Type','line','SrcBlockHandle',goto_port.SrcBlock);
%            set_param(line2,'Name',block_name);
%        end
            

        %find the FROM blocks that are associated with the GOTO block
        from_block=find_system(system_name,'SearchDepth',1,'GotoTag',signal_name,'BlockType','From'); %NOTE only look at current level in model!!! returns a cell array
        for iv=1:length(from_block),
            if length(from_block)>1,
                set_param(from_block{iv},'Name',['from_',block_name,'_',num2str(iv)],'GotoTag',block_name);
            else
                set_param(from_block{iv},'Name',['from_',block_name],'GotoTag',block_name);
            end
        end
        
    end
end

%% change stuff connected to con_i port
if ~isempty(con_i_port),
if ~isempty(con_i_port.SrcBlock),
    if ~(con_i_port.SrcBlock==-1),
        %selector
        if strmatch(get_param(con_i_port.SrcBlock,'Tag'),'SIMarchitect_selector'), 
            set_param(con_i_port.SrcBlock,'Name',['selector_',block_name]);
            selector_ports=get_param(con_i_port.SrcBlock,'PortConnectivity');
            if ~isempty(selector_ports(1).SrcBlock),
                if strmatch(get_param(selector_ports(1).SrcBlock,'BlockType'),'From'),
                    set_param(selector_ports(1).SrcBlock,'Name',['from_main_',block_name]);
                end
            end

        %may also be a 'from'   
        elseif strmatch(get_param(con_i_port.SrcBlock,'BlockType'),'From'),     
            set_param(con_i_port.SrcBlock,'Name',['from_main_',block_name]);                     
        end;    
        
    end
end
end
