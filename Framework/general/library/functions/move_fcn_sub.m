%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function move_fcn_sub(block_handle)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
% Description: Prevenst the moving ov the selector, the goto and the from relative to the component they are connected to.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tag=get_param(block_handle,'Tag');
switch(tag),
case 'SIMarchitect_goto'
    try
        ports=get_param(block_handle,'PortConnectivity');
        component=ports(1).SrcBlock;
        set_param(block_handle,'MoveFcn','');                   %switch off
        move_fcn(component);    %call in the move_fcn for the component, that way the goto will be placed back to position
        set_param(block_handle,'MoveFcn','move_fcn_sub(gcbh)'); %switch on
    end
    
case 'SIMarchitect_selector'
    try
        ports=get_param(block_handle,'PortConnectivity');
        component=ports(2).DstBlock;
        set_param(block_handle,'MoveFcn','');                   %switch off        
        move_fcn(component);    %call in the move_fcn for the component, that way the selector will be placed back to position
        set_param(block_handle,'MoveFcn','move_fcn_sub(gcbh)'); %switch on        
    end

case 'SIMarchitect_from1'
    try
        ports=get_param(block_handle,'PortConnectivity');
        if strcmp(get_param(ports(1).DstBlock,'Tag'),'SIMarchitect_selector'),
            selector=ports(1).DstBlock;
            set_param(block_handle,'MoveFcn','');                   %switch off            
            move_fcn_sub(selector);    %call in the move_fcn_sub for the selector, that way it will call the main move_fcn
            set_param(block_handle,'MoveFcn','move_fcn_sub(gcbh)'); %switch on            
        else
            component=ports(1).DstBlock;
            set_param(block_handle,'MoveFcn','');                   %switch off            
            move_fcn(component);    %call in the move_fcn for the component, that way the selector will be placed back to position
            set_param(block_handle,'MoveFcn','move_fcn_sub(gcbh)'); %switch on            
        end
    end
end
lasterr('');%reset last error message, Matlab complains about recursiveness. Which is basically true, but it is not bothering us.
