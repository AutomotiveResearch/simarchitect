%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function [added,bool,subname]=mask_fcn(parameterfile,component)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
% Description: This function loads the parameterfile OR a mat file with the same name as the simulink block where 
% this function is called from. It saves the parameters that are loaded into the mask also in a structure named 
% 'parameters' the parameters structure remains in the workspace.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(get_param(bdroot,'SimulationStatus'),'stopped'), 
    % This is the case when user 'looks under mask' for the first time. The mask function gets executed.
    % This may result in an error if it is the library that is looked under. To prevent this, just don't
    %  execute this function any further.
    added = [];
    bool = 0;
    subname = [''];
    return
end

%% Make structure 'parameters' available in this function (load it from base WS)
% Caller WS is the Mask Workspace of the simulink masked subsystem which
% calls this function.
if evalin('base',['exist(''parameters'',''var'');']),
    parameters = evalin('base','parameters');
else
    parameters = [];
end

%% Check whether to load parameters from m-file of from parameter structure
%set ACTIVE = [1] if parameters have to be loaded from parameter-structure
active=0;   %by default load from parameterfile
if isfield(parameters,'active'),
    if parameters.active==1,
        active=1;        
    end
end

% Create the subname for the data to be stored into the parameter structure
subname=modify_name_loc(component);


% Add the loaded parameters to the structure parameters in a new field with the 'subname'
if ~active,    
    % load the data from the m-files
    [bool,parameters,added]=load_parameters(parameterfile,subname,parameters);
else
    % load the data from the parameter-structure
    [bool,parameters,added]=load_parameters_from_struct(subname,parameters);
end


%Write the structure 'parameters' to the base WS
assignin('base','parameters',parameters);


%In case an error occured....
if bool==-1,
     h=errordlg(sprintf(['Error loading the parameterfile of ',subname,'.\n',...
             'See command window for more details.\n\n',...
             'It may be that Simulink will issue an error, however the problem lies in the parameterfiles.\n\n',...
             '(Ignore this message when no parameterfile has been set for this module)']),'SIMarchitect: error');
     set(h,'WindowStyle','modal');
 end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% SUBFUNCTIONS
function [bool,parameters,added]=load_parameters(parameterfile,subname,parameters)

try,
    LOCAL_VARS.flag=0;
    LOCAL_VARS.bool=0; %indicates to mask init commands that parameters are loaded from parameterfiles
    LOCAL_VARS.subname=subname;
    clear subname;
    
    [Pfile directory] = get_param_file([],parameterfile); %get file and location of file with name parameterfile
    
    
    if Pfile~=-1,
        %%run the parameterfile
        LOCAL_VARS.Pfile=Pfile;
        LOCAL_VARS.directory=directory;
        LOCAL_VARS.flag2=1;
        clear Pfile directory parameterfile ModuleHandle 
        
        %make sure we're in working directory
        if ~strcmp(LOCAL_VARS.directory,pwd),
            LOCAL_VARS.cur=pwd;
            cd(LOCAL_VARS.directory);
            LOCAL_VARS.flag=1;
        end
        
        try
            eval(LOCAL_VARS.Pfile);
        catch
            disp('___');
            disp(['ERROR LOADING FILE: ',LOCAL_VARS.Pfile, ' -> Error IN file!']);
            disp('___');
            %% provide output and return
            bool=-1; %indicates error to mask init commands
            parameters=parameters;
            added.none=-1;
            return            
        end
        
        if LOCAL_VARS.flag, %return to original directory
            cd(LOCAL_VARS.cur); 
        end;
        
    else %file not found
        %return
        bool=-1; %indicates error to mask init commands
        parameters=parameters;
        added.none=-1;
        return            

    end

    %register filename and 
    eval(['parameters.' LOCAL_VARS.subname '.Parameterfile=''',char(LOCAL_VARS.Pfile),'.m'';']);  %register the name of the parameterfile

    %the structure 'con_info' is used for connection information should not be in the parameters structure
    if exist('con_info','var')==1,
        if isstruct(con_info), %isstruct returns error if 'info' doesn't exist.
            clear con_info
        end
    end
    
    % add parameters to structure 'added' and to structure 'parameters'
    loaded_vars.name=who;
    
    % the 'dummy_level_i' is to prevent likely interference with parameters named i
    added.none=-1; %% make sure output is defined
    for dummy_level_i=1:length(loaded_vars.name),
        var = loaded_vars.name{dummy_level_i};
        if sum(strcmp(char(var),{'LOCAL_VARS','parameters'}))==0, %parameters is required output, LOCAL_VARS is a local structure
            try
                eval(['parameters.' LOCAL_VARS.subname '.',char(var),'=',var,';']);
            catch
                var = modify_name(var);
                eval(['parameters.' LOCAL_VARS.subname '.',char(var),'=',var,';']);            
            end
            eval(['added.',char(var),'=parameters.' LOCAL_VARS.subname '.',char(var),';']);
        end
    end

 
    
catch,
    if LOCAL_VARS.flag, %return to original directory
        cd(LOCAL_VARS.cur); 
    end;

    disp('___');
    disp(['ERROR: Trying to load file ''',LOCAL_VARS.Pfile,''' Error: ',lasterr]);
    disp('___');
    parameters=parameters;
    added.none=-1;    
    LOCAL_VARS.bool=-1; %indicates error to mask init commands
end;
bool=LOCAL_VARS.bool;
%end subfunction load_parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [bool,parameters,added]=load_parameters_from_struct(subname,parameters)

try,
    LOCAL_VARS.bool=1; %indicates to mask init commands that parameters are loaded from the parameter structure
    LOCAL_VARS.subname=subname;
    clear subname;
    
    loaded_vars.name=eval(['fieldnames(parameters.',LOCAL_VARS.subname,');']);
    for i=1:length(loaded_vars.name),
        if ~strcmp(char(loaded_vars.name{i}),'LOCAL_VARS.')&...
                ~strcmp(char(loaded_vars.name{i}),'parameters')&...
                ~strcmp(char(loaded_vars.name{i}),'ParameterfileExt')&...
                ~strcmp(char(loaded_vars.name{i}),'Parameterfile'),
            eval(['added.',char(loaded_vars.name{i}),'=parameters.',LOCAL_VARS.subname,'.',loaded_vars.name{i},';']);
        elseif strcmp(char(loaded_vars.name{i}),'Parameterfile'),
            eval(['parameters.' LOCAL_VARS.subname '.Parameterfile= ''(loaded from parameter structure)'';']);
        elseif strcmp(char(loaded_vars.name{i}),'ParameterfileExt'),    %The existence of this field indicates that a parameterfile is loaded via the 'structure-route' (see modify_param_struct.m)
            eval(['parameters.' LOCAL_VARS.subname '.Parameterfile=parameters.',LOCAL_VARS.subname,'.',loaded_vars.name{i},';']);
            eval(['parameters.',LOCAL_VARS.subname,'=rmfield(parameters.',LOCAL_VARS.subname,',''ParameterfileExt'');']);
        end
    end
    if length(loaded_vars.name) == 1 & strcmp(char(loaded_vars.name{i}),'Parameterfile')
        added.none=-1;
    end
catch,
    disp('___');
    disp(['ERROR: Could not load parameters from ''parameters.',LOCAL_VARS.subname,'''']);
    disp('___');
    parameters=parameters;    
    added.none=-1;
    LOCAL_VARS.bool=-1;    %indicates error to mask init commands
end;
bool=LOCAL_VARS.bool;
%end subfunction load_parameters_from_struct
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function subname=modify_name_loc(component)

%get the name of the subsystem including the path (until first '/' is reached)(excluding the system name)
subname=component;
[rem,subname]=strtok(subname,'/');%take away system name
subname=subname(2:end); %take of the '/'

subsys=[];
par=subname;
subname=[];
while ~isempty(par),   %take from every sub level the name and make this a level in the parameters structure
    
    [par,ch]=strtok(par,'/');
    if ~isempty(ch),
        if ch(2)=='/',  %correct for '//' in name, this is not a token.
            [par2 ch] = strtok(ch(3:end),'/'); %the ch includes the token!
            par = [par,'_',par2];
        end
        subname=[subname,par,'.'];
        par=ch(2:end); 
    else
        subname=[subname,par];
        par=[];
    end        
    
end    

subname = modify_name(subname,'parameter');

