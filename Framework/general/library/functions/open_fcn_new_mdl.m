%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function flag=open_fcn_new_mdl(system)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%
%
% Description: Opens a new SIMarchitect model. The model must be available in 
%              <SIMarchitect_root>\library\models\simcat_mod_def.mdl
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

flag=0;     %indicates unsuccesfull opening of a file, if al goes well this is changed to 1

disp(['__SIMarchitect: Creating new model__________________________________________________________________']);
try
    % Get name and location for new model
    [filename, mdl_pathname] = uiputfile({'*.slx','Model (*.slx)'; '*.mdl','Model (*.mdl)'},'Save new model as:'); %select file
    
    if filename==0,
        y=questdlg('You have to save the model under a different name.','SIMarchitect: Question','Continue','Stop','Continue');
        if strcmp(y,'Continue'),
            i=0;
            while filename==0,
                if i==1,
                    y=questdlg('You have to save the model under a different name.','SIMarchitect: Question','Continue','Stop','Continue');
                    if strcmp(y,'Stop'),
                        disp('-> Action canceled');
                        return                
                    end;
                end
                [filename, mdl_pathname] = uiputfile('*.mdl','Save model as:'); %select file
                i=1;
            end
        else
            disp('-> Action canceled');
            return
        end   
    end
    %check for forbidden path
    string=['Save new model as'];
    file=[filename];
    mdl_pathname=check_path(mdl_pathname,file,string);  %check for forbidden dir's
catch
    disp('SIMarchitect ERROR: Could not get name and location for new model.')
    return    
end

%make sure '.mdl' or '.slx' is stripped of
if length(filename)>3,  %filename long enough for check
    if strcmp(filename(end-3:end),'.mdl') || strcmp(filename(end-3:end),'.slx'),
        system=filename(1:end-4);
    else
        system=filename;
    end
else
    system=filename;
end

%open the model
try
    cd([SIMarchitect_root,'\general\library\models\']);
    load_system('empty_mod_def');                                                 %load the system but do not show it
    save_system(['empty_mod_def'],[mdl_pathname,filename]);                       %save under new name
    bdclose('empty_mod_def');                                                     %close default system
    open_system(system);                                                        %show new system
    disp(['Created model: ',mdl_pathname,filename]);
catch
    disp(['Could not create the new model, the error was: ',lasterr]);
    return
end


block=find_system(system,'Tag','ParameterSetting');                           %find block for save options
if ~isempty(block),
    block_handle=get_param(char(block),'Handle');
end

try     %working directory
    if ~isempty(block_handle),
        
        work_pathname=[mdl_pathname,'parameterfiles'];
        
        %set working directory
        if work_pathname ~= 0,
            if ~isdir(work_pathname),
                eval(['cd(''',mdl_pathname,''');']);    %only with matlab command 'cd' able to change to dir's with a space
                eval(['!md parameterfiles']);       %create the directory 'parameterfiles' with a system command
                eval(['cd parameterfiles']);       %change to this directory
            end
            %work_pathname=check_path(work_pathname,file,string);  %check for forbidden dir's
            disp(['Working directory set to: ',work_pathname,' (can be changed in save options)']);            
            eval(['cd(''',mdl_pathname,''');']);    %only with matlab command 'cd' able to change to dir's with a space            
            eval(['cd parameterfiles']);       %change to this directory
            disp(['Changed current directory to: ',work_pathname]);                        
        else
            work_pathname=mdl_pathname;
            disp(['Working directory set to: ',work_pathname,' (can be changed in parameters setting GUI)']);
            cd(work_pathname);
        end    
        set_param(block_handle,'MaskValues',{work_pathname});    %update mask    
    end
    flag=1; %indicate succesfull opening of new model
catch
    disp('SIMarchitect WARNING: Could not set default working directory (can be changed in parameters setting GUI), error was:');
    disp(lasterr);
    return
end  



save_system %last save to save the parameterfiles directory


disp(['_____________________________________________________________________________________________________']);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function dir=check_path(pathname,file,string)

%make sure pathname is not one of following directories
dirs=lower(strvcat([SIMarchitect_root,'\library'],...
    [SIMarchitect_root,'\documentation']));

for i=1:size(dirs,1),
    b=strmatch(deblank(dirs(i,:)),lower(pathname));
    if b,
        break
    end
end
if ~b,
    dir=pathname; %ok
else    %not ok
    while b,
        h=msgbox(['The directory ',pathname,' is not allowed, is a protected SIMarchitect directory. Please select another directory'],...
            'SIMarchitect: Message');
        drawnow
        waitfor(h);
        [filename, pathname] = uiputfile(file,string); %select file
        if pathname~=0,
            for i=1:size(dirs,1),
                b=strmatch(deblank(dirs(i,:)),lower(pathname));
                if b,
                    break
                end
            end
        else
            break
        end
    end
    dir=pathname;
end

