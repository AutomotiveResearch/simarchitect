%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function init_fcn
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%
%
% Description: clear the structure 'parameters' from the workspace, if parameters.active is non-existent or 0
%              called from the model as the 'InitFcn'
%              Checks parameterfiles directory and if not exist asks for correct (relative/absolute) directory.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% global issues
%check for existence of the structure 'parameters'
x=evalin('base',['exist(''parameters'');']);

if x==1,    % a variable (might not be a structure) exists in the WS
    try
        x=evalin('base',['parameters.active;']); %try to get the value of the field (when it doesn't exist, it errors out)
        if x==0,
            evalin('base',['clear parameters']); %if parameters.active=0, variables have to be loaded from files, so deleter parameters
        end
    catch
        evalin('base',['clear parameters']);    % parameters.active didn't exist in the WS, so delete parameters
    end
end

% Check of location of parameterfiles
% Read path of Simulink Model
modeldir=get_param(gcs,'FileName'); % complete name and path of Simulink model
% modeldir=strrep(modeldir,['\',gcs,'.mdl'],''); % remove name of model
modeldir=strrep(modeldir,['\',gcs,'.slx'],''); % remove name of model
% Read name of directory of parameterfiles
% WARNING: Name parameter settings block ("ParameterSetting") may not be editted!
pardir=get_param([gcs,'/ParameterSetting'],'MaskValueString');

if isempty(pardir)
    pardir='.\NoValidDirectoryNameFound';
end
% check of relative or absolute location of parameterfiles
if pardir(1:1) == '.'  % relative directory
    pardir2 = [modeldir pardir(2:end)];
else                    % absolute directory
    pardir2 = pardir;
end
% Check existence pardir2
% Asks for name of directory if pardir2 does not exist
if ~exist(pardir2,'dir')
    Buttonname=questdlg('Update parameterfile directory','Error incorrect parameterfile directory','Yes','Yes');
    [pardir2] = uigetfolder('Select directory for parameterfiles');
    if isempty(findstr(pardir2,modeldir))     % absolute location of parameterfiles
        pardir = pardir2;
    else                                        % relative location of parameterfiles
        pardir = ['.' strrep(pardir2,modeldir,'')];
        if length(pardir) == 1                 % pardir = pardir2
            pardir = '.\';
        end
    end
    set_param([gcs '/ParameterSetting'],'MaskValueString',pardir); % store new directory parameterfiles
end
