%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function varargout = gui_param_set(varargin)
% DEV_GUI_1 Application M-file for Dev_GUI_1.fig
%    FIG = DEV_GUI_1 launch Dev_GUI_1 GUI.
%    DEV_GUI_1('callback_name', ...) invoke the named callback.

% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018

%% 
if nargin == 0  % LAUNCH GUI
    
    fig = openfig(mfilename); 
    % Generate a structure of handles to pass to callbacks, and store it. 
    handles = guihandles(fig);
    guidata(fig, handles);

    %create userdata structure to store modifications before applying them and the block handle
    ud.file = {};
    ud.module ={};
    ud.path={};
    ud.load_WS=[];
    ud.block_handle = get_param(gcb,'Handle');
    ud.system = gcs;
    set(fig,'userdata',ud);
    
    %1x1 cell array 1st field containing working dir
    a=get_param(ud.block_handle,'MaskValues');
    flag=exist(a{1});
    if flag~=7,
        a{1}=[a{1},' -DIR. NOT FOUND!!'];
    end
    if ~isempty(a{1}),
        add_to_textbox(handles.work_dir_textbox,a{1});  %subfunction
    end


    %Read block name from Simulink Block
    %blockname = get_param(gcb,'name');
    set(handles.text4,'String',gcs)

    %run the 'load_WS_ChkBox_Callback
    load_WS_ChkBox_Callback(handles.load_WS_ChkBox,[], handles, 'init');
    
    %center the figure
    center_fig(fig);
    set(fig,'Visible','on');   
    
    
    %---GUIDE generated code-----------------------------------------    
    if nargout > 0
        varargout{1} = fig;
    end
    
elseif ischar(varargin{1}) % INVOKE NAMED SUBFUNCTION OR CALLBACK
    
    try
        [varargout{1:nargout}] = feval(varargin{:}); % FEVAL switchyard
    catch
        disp(lasterr);
    end
    
end


% Pushbuttons--------------------------------------------------------------------
% --------------------------------------------------------------------
function varargout = button_select_dir_Callback(h, eventdata, handles, varargin)
% callback from select working dir button
%get directory name
[pathname] = uigetfolder('Select directory for parameterfiles'); %select file
%<MvdM> Code changed, do not add a relative path, only ad a fixed path
% the reason for relative path is unknown by me

% modeldir=get_param(gcs,'FileName');
% modeldir=strrep(modeldir,['\',gcs,'.mdl'],'');
% WARNING: Name parameter settings block ("ParameterSetting") may not be editted!
% if ~isempty(findstr(pathname,modeldir))
%     pathname=['.',strrep(pathname,modeldir,'')];
%     if length(pathname) == 1
%         pathname = '.\';
%     end
%     set_param([gcs '/ParameterSetting'],'MaskValueString',pathname);
% end

if ~isempty(pathname) %If pressed cancel than empty
    %%%update GUI and ud
    ud = get(handles.figure1,'UserData');
    ud.path = pathname(1:end);
    set(handles.figure1,'UserData',ud);
    set(handles.apply_button,'Enable','on');    %enable the apply button
    
    add_to_textbox(handles.work_dir_textbox,pathname(1:end));  %subfunction    
else
    % add the path in the GUI <MvdM>
    set_param([gcs '/ParameterSetting'],'MaskValueString',pathname);
end
gui_param_set('model_browser_Callback',handles.model_browser,[],guidata(gcbo),'Update'); %update the browser


% --------------------------------------------------------------------
function varargout = select_button_Callback(h, eventdata, handles, varargin)
% Callback for Selectfile button

%make sure we're in working dir
curr_path = pwd;
work_dir = get(handles.work_dir_textbox,'UserData');
if ~strcmp(curr_path,work_dir),
    flag=1;
    cd(work_dir);
else
    flag=0;
end

%get file name
[filename, pathname] = uigetfile({'*.m','datafiles (.m)'},'Select parameterfile for module'); %select file
if ~isnumeric(filename),
    if strcmp(filename(end-1:end),'.m'), %take of .m
        filename=filename(1:end-2);
    end
    add2editfield(filename,pathname,handles);
end

%make sure we end up in same directory as we started
if flag,
    cd(curr_path);
end

% --------------------------------------------------------------------
function varargout = edit_button_Callback(h, eventdata, handles, varargin)
%Callback from edit button

file=get(handles.parameterfile_edit_field,'Userdata');
path=get(handles.work_dir_textbox,'Userdata');
if ~isempty(file)&~isempty(path),
    edit([path,'\',file]); %open the matlab editor with the specified file
else
    errordlg(['Cannot open the file, is any selected?']);
end    

% --------------------------------------------------------------------
function varargout = ok_button_Callback(h, eventdata, handles, varargin)
%% Callback from OK button
% Apply changes
apply_changes(handles);
%Close figure
delete(handles.figure1);

% --------------------------------------------------------------------
function varargout = cancel_button_Callback(h, eventdata, handles, varargin)
%Callback from Cancel button
%Close figure, no further actions undertaken
delete(handles.figure1);

% --------------------------------------------------------------------
function varargout = help_button_Callback(h, eventdata, handles, varargin)
%Callback from Help button
SIMarchitect_open_help('Parameter_Settings');

% --------------------------------------------------------------------
function varargout = apply_button_Callback(h, eventdata, handles, varargin)
%Callback from Apply button
% Apply changes
apply_changes(handles);

% Edit fields--------------------------------------------------------------------
function varargout = parameterfile_edit_field_Callback(h,eventdata,handles,varargin)
%Callback from parameterfile edit field

filename = deblank(get(h,'String'));
if strcmp(filename(end-1:end),'.m'),
    filename=filename(1:end-2);
end
pathname=get(handles.work_dir_textbox,'Userdata');
if ~strcmp(pathname(end),'\'),
    pathname=[pathname,'\'];
end
add2editfield(filename,pathname,handles);

% ------------------------------------------------------------------------------------------------------------------------------------------
% Listfields callbacks--------------------------------------------------------------------

function varargout = model_browser_Callback(h, eventdata, handles, varargin)
% Callback of the uicontrol model_browser
if strcmp(varargin{1},'Create'),    
    structure=[];
    %% get the structure of the model
    %OLD: list=find_system(gcs,'MaskVariables',['parameterfile=&1;'])    % *x1 cell array with full paths to masked components
    list=find_system(gcs,'MaskVariables',['parameterfile=&1;']); 
    list=[list; find_system(gcs,'MaskVariables',['parameterfile=&1;VisualiseVRML=@2;ReplayVRML=@3;'])];
                                                %find_system(gcs,'MaskVariables',---), --- literal text from mdl text-file
    
    list=[list; find_system(gcs,'MaskVariables',['parameterfile=&1;VisualiseTopView=@2;'])];                                                                                     
    list=[list; find_system(gcs,'Tag','control_block')];
    
    data = create_cell_struct(list);
    data=data{1}{2};
    if ~isempty(data),
        treeview('Create',h,data); %initialise the browser
    else
        set(h,'String','');
    end
    
elseif strcmp(varargin{1},'Update')
    treeview('Update'); %update the listbox
    % evaluate selection
    selected=get(h,'value');
    treeData=get(h,'Userdata');
    [dispData idx] = FindDisplayedData(treeData);
    selectedData=dispData(selected);
    selectedDataName=selectedData.Fullname;
       
    if selectedData.IsExpanded==-1, %not expandable
        ud = get(handles.figure1,'UserData');   
        %%% Find the parameterfilename and plot it in the appropriate box
        %add system name in front and replace . by / to obtain full path to block
        selectedDataName=[ud.system,'/',strrep(selectedDataName,'.','/')];
        %filename=char(get_param(selectedDataName,'MaskValues'))%old
                
        allfilenames = get_param(selectedDataName,'MaskValues'); %ASSUMING THAT THE FIRST MASKENTRY IS THE NAME OF THE PARAMETERFILE!!
        filename = char(allfilenames(1));
             

        %Check if file not in data that still needs to be applied. In that case take that filename.
        for i=1:length(ud.module),
            if strcmp(selectedDataName,ud.module(i)),
                filename = char(ud.file(i));
            end
        end
        
        %Find the location of the file that will be loaded. First of all the mask_fcn will look at the working directory
        %and secondly it will look at the general Matlab path.
        ud = get(handles.figure1,'UserData');
        flag=check_file([],filename,ud);

        if flag==1, %indicates succes in check_file function
            text=filename;
            ud=filename;
            set(handles.edit_pushbutton,'Enable','on');
            set(handles.select_button,'Enable','on');            
        else
            text=['File not found in parameterfile-directory, model will not run!!'];
            ud='';
            set(handles.edit_pushbutton,'Enable','off');
            set(handles.select_button,'Enable','on');
        end
        
        if length(text)<70,
            set(handles.parameterfile_edit_field,'String',text,...
                'Userdata',ud,'Enable','on');
        else
            set(handles.parameterfile_edit_field,'String',['.....',text(end-65:end)],...
                'Userdata',ud,'Enable','on');
        end
    else
        set(handles.parameterfile_edit_field,'String','none',...
            'Userdata','none','Enable','off');
        set(handles.edit_pushbutton,'Enable','off');
        set(handles.select_button,'Enable','off');
    end
end

%-------------------------------------------------------
%CheckBoxes callbacks
function varargout = load_WS_ChkBox_Callback(h, eventdata, handles, varargin)
%load parameters from structure callback
ud = get(handles.figure1,'UserData');
% check whether a structure called parameters is available in WS
flag=evalin('base',['exist(''parameters'',''var'');']);
if flag,    %check if it is a structure
    flag = evalin('base',['isstruct(parameters);']);

    handles2set = [handles.model_browser
        handles.button_select_dir
        handles.work_dir_textbox 
        handles.parameterfile_edit_field 
        handles.edit_pushbutton
        handles.select_button ]; %perhaps not most ideal place to define this...
end

if ~isempty(varargin)&flag,
    %is the case when GUI gets started and when parameters structure already exists
    flag2 = evalin('base',['isfield(parameters,''active'');']);
    if flag2,
        value = evalin('base',['parameters.active;']);
        set(h,'Value',value);
    else
        value=0;
    end
else    
    value = get(h,'value');    
end


if value&flag,
    %%load parameters from structure
    % if structure is available, set 'parameters.active=1' (doesn't really matter whether or not this was already the case)
    ud.load_WS = '1';
    set(handles.apply_button,'Enable','on');    %enable the apply button
    
    % disable the appropriate sections of the GUI
    if flag,
        set(handles2set,'enable','off');            
    end
    
elseif value&~flag    
    answ=questdlg('The structure ''parameters'' is not available in the WS. The model first has to be initialised; initialise now?',...
        'SIMarchitect: Warning','Yes','No','Yes');
    if strcmp(answ,'Yes'),
        set_param(ud.system,'SimulationCommand','Update');
        assignin('base','parameters',parameters);
        load_WS_ChkBox_Callback(h, eventdata, handles);
    else
        set(h,'Value',0);
    end    
    
    
elseif ~value&flag
    %%load parameters from files
    % if structure is available, set 'parameters.active=0' (doesn't really matter whether or not this was already the case)
    ud.load_WS = '0';
    set(handles.apply_button,'Enable','on');    %enable the apply button
    
    % enable the appropriate sections of the GUI
    set(handles2set,'enable','on');            
    
end
set(handles.figure1,'UserData',ud);

%-------------------------------------------------------
%-------------------------------------------------------
%---------------used sub functions--------------------------
%-------------------------------------------------------
%-------------------------------------------------------
function add2editfield(filename,pathname,handles)

if ~isempty(filename) %If pressed cancel do not echo the zero return
    %%% update GUI
    ud = get(handles.figure1,'UserData');
    
    flag = check_file(pathname,filename,ud); %subfunction that handles the parameterfile returns '-1' or '1'
    
    %Find selected module
    selected=get(handles.model_browser,'value');
    treeData=get(handles.model_browser,'Userdata');
    [dispData idx] = FindDisplayedData(treeData);
    selectedData=dispData(selected);
    selected_module=selectedData.Fullname;
    %add system name in front and replace . by / to obtain full path to block
    selected_module=[get_param(bdroot(gcs),'Name'),'/',strrep(selected_module,'.','/')];
    
    %add module to userdata    
    ud.module{end+1} = selected_module;
    set(handles.apply_button,'Enable','on');    %enable the apply button
    
    if flag==1, %indicates succes in check_file function
        %add file to userdata    
        ud.file{end+1} = filename;
        
        text=filename;
        edit_ud=filename;
        set(handles.edit_pushbutton,'Enable','on');                
    else
        %add 'none' to userdata    
        ud.file{end+1} = 'none';

        text=['File not found in parameterfile-directory, model will not run!!'];
        ud='none';
        set(handles.edit_pushbutton,'Enable','off');        
    end
    
    if length(text)<70,
        set(handles.parameterfile_edit_field,'String',text,...
            'Userdata',edit_ud);
    else
        set(handles.parameterfile_edit_field,'String',['.....',text(end-65:end)],...
            'Userdata',edit_ud);
    end    
    set(handles.figure1,'Userdata',ud);
    
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
function [dd, idx] = FindDisplayedData(ud)
%find displayed data in model browser listbox
dd  = ud;
idx = [];
rem = [];

for i = 1:length(ud)
    if ud(i).IsDisplayed
        idx = [idx i];
    else
        rem = [rem i];
    end
end
dd(rem) = [];

% --------------------------------------------------------------------

function flag=check_file(pathname,parameterfile,ud)
%% function returns -1 if file is not in parameterfile-directory of this model after this function has finished, otherwise return 1;
%% Function is called from 'select_button_Callback' and 'model_browser_Callback' when called with argument 'update'.
flag=-1;
block_handle = ud.block_handle;

%%%%%%%%%% find out what the working directory is
try
    if ~isempty(ud.path),
        work_dir = ud.path;
    else
        work_dir=get_param(block_handle,'MaskValues'); %the mask variable where the work dir is stored
        work_dir=work_dir{1};
        if ~(exist(work_dir,'dir')==7),
            h=warndlg(['Please select an existing parameterfile-directory first.'],'SIMarchitect Message');
            waitfor(h);
            return
        end            
    end
catch
    errordlg(lasterr)
    return
end
if ~strcmp(work_dir(end),'\'),
    work_dir=[work_dir,'\'];
end

%%%%%%%%% Check if selected file is in selected work dir
copy_flag=0;
if ~isempty(pathname), %function is called from 'select_button_Callback'
    if ~strcmp(lower(work_dir),lower(pathname)),
        string=sprintf(['The selected file is not in the parameterfile-directory of this model.\n',...
                'Copy the file to the parameterfile-directory?\n(Otherwise the model will not run.)']);
        answ=questdlg(string,'SIMarchitect: Question','Yes','No','Yes');
        if strcmp(answ,'Yes'),
            copy_flag=1; %copy it
        end
    else
        flag=1;
    end
else %function is called from 'model_browser_Callback'
    flag=exist([work_dir,parameterfile,'.m'],'file')==2;    % '.m' is necessary, otherwise it can't find the file if it is not the pwd
end

%%%%%%%%% Try to copy file if necessary and wanted
if copy_flag,
    %check if it is already in working directory, if no, copy it there, if yes, ask permission to overwrite
    destination=[work_dir,parameterfile];
    exist_flag=exist([destination,'.m'],'file')==2;
    if exist_flag,
        string=sprintf(['A file with the same name already exists in the parameterfile-directory of this model.\n',...
                'Overwrite this file?']);
        answ=questdlg(string,'SIMarchitect: Question','Yes','No','Yes');
        if strcmp(answ,'No'),
            copy_flag=0; % don not copy it
            msgbox(['The file that will be used is the file that is currently in the parameterfile-directory.'],...
                'SIMarchitect: Message');
            flag=1;
        end
    end
    
    if copy_flag,  %allowed to copy it
        source=[pathname,parameterfile];
        [status msg]=copyfile(source,destination,'writable');
        if status==0,
            string=sprintf('Error copying file: %s :\n%s',[parameterfile],msg);
            errordlg(string,'SIMarchitect: Error');
        else
            disp(['COPIED ',source,' TO ',destination]);
            flag=1;
        end
    end        
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function add_to_textbox(handle,string)

%wrap text if necessary and place it in box
if length(string)<60,
    set(handle,'String',string,...
        'Userdata',string,...
        'Enable','on');
else
    set(handle,'String',['.....',string(end-50:end)],...
        'Userdata',string,...
        'Enable','on');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function data = create_cell_struct(list)
%   Creates a cell array with Simulink block names as input. The cell array can be used
%   as input for 'treeview'.
%   If an element of the cell array is a string, it
%   represents a node with no children. If the element is a cell
%   array, the first element of that cell array represents the name of
%   the node and the second element contains the children of that node.


flag=1;
data = {};
i=1;
j=1;

while flag,
    var = list{i};
    [par ch] = strtok(var,'/'); %the ch includes the token!
    
    if ~isempty(ch) & ch(2)=='/',  %correct for '//' in name, this is not a token.
        [par2 ch] = strtok(ch(3:end),'/'); %the ch includes the token!
        par = [par,'//',par2];
    end

    if isempty(ch),     %token not found, add directly to cell data
        data{j,1} = par;
        i=i+1; j=j+1;

    else
        indx=find(strncmp({par},list(i:end),length(par)))+i-1; %find indices of elements in list (from current point onwards) with same parent. 
        newlist={};
        for ii=1:length(indx), %create new list of children of this node
            newlist{ii,1} = list{indx(ii)}(length(par)+2:end);
        end

        data{j,1}{1} = par; %first element in cell array is node name
        data{j,1}{2} = create_cell_struct(newlist); %2nd element contains children of this node
        i=i+length(indx); j=j+1;

    end
    if i>length(list),
        flag=0;
    end    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function apply_changes(handles)

ud = get(handles.figure1,'Userdata');

%% path
if ~isempty(ud.path),
    set_param(ud.block_handle,'MaskValues',{ud.path});    %update mask
end

%% files
if ~isempty(ud.file),
    for i=1:length(ud.file),
        %set blocks and files
        set_param(ud.module{i},'MaskValues',ud.file(i));
    end
end

%% load from WS
if ~isempty(ud.load_WS),
    evalin('base',['parameters.active=',ud.load_WS,';']);
end

%clear the userdata again (except the block handle)
ud.file = {};
ud.module ={};
ud.path={};
set(handles.figure1,'userdata',ud);

set(handles.apply_button,'Enable','off');

