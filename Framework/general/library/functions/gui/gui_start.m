%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function varargout = gui_start(varargin)
% DEV_GUI_1 Application M-file for Dev_GUI_1.fig
%    FIG = DEV_GUI_1 launch Dev_GUI_1 GUI.
%    DEV_GUI_1('callback_name', ...) invoke the named callback.

% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018

%%%%%Comments
%All .m datafiles that will be assigned to simulink blocks are assumed to be in 
%the directory SIMarchitect_root\temp

persist_workdir; % remember the working directory!

if nargin == 0  % LAUNCH GUI
    
    fig = openfig(mfilename,'reuse');

    % Generate a structure of handles to pass to callbacks, and store it. 
    handles = guihandles(fig);
    
    % Get the menu data back into the GUIDATA!!
    hls = guidata(fig);
    if ~isempty(hls)
        fn = fieldnames(hls);
        for i=1:length(fn)
            if ~isfield(handles, fn{i})
                handles.(fn{i}) = hls.(fn{i});
            end
        end
    end
    
    handles = createMenu(fig, handles);
    guidata(fig, handles);
    
    %set logo in figure
    [logo, ~, alpha] = imread('SIMarchitect1000px.png');    
    %f = imshow(logo, 'Parent', handles.logo_axes);
    %set(f, 'AlphaData', alpha);     % make logo background transparent

    % Set the close function
    set(fig, 'CloseRequestFcn', @close_request_Callback);
    
    % SIMT-3004: Changed the order of the code while MATLAB did not display
    % the figure correctly.
    
    %center figure on screen
    center_fig(fig);
    set(fig,'Visible','on');
    
    %---GUIDE generated code-----------------------------------------    
    if nargout > 0
        varargout{1} = fig;
    end
    
elseif ischar(varargin{1}) % INVOKE NAMED SUBFUNCTION OR CALLBACK
    
    try
        [varargout{1:nargout}] = feval(varargin{:}); % FEVAL switchyard
    catch
        disp(lasterr);
    end
    
end

% Close request------------------------------------------------------------------
function varargout = close_request_Callback(varargin)
persist_workdir('forget'); % Forget the "old" working directory!
closereq;

% Pushbuttons--------------------------------------------------------------------
function varargout = close_button_Callback(h, eventdata, handles, varargin)
%Callback from Cancel button
%Close figure, no further actions undertaken
close_request_Callback(varargin);
warning on

% --------------------------------------------------------------------
function varargout = help_button_Callback(h, eventdata, handles, varargin)
%Callback from Help button
SIMarchitect_open_help('Start_Figure');
% --------------------------------------------------------------------

function varargout = new_model_button_Callback(h, eventdata, handles, varargin)
% Callback from Create New Model button
flag=open_fcn_new_mdl;
if flag,
    h=gcs;
    simulink
    simulink('load','SIMarchitect_lib'); %load the library, so that references can be made
    open_system(h);
    
    delete(handles.figure1);
end

% --------------------------------------------------------------------

% --------------------------------------------------------------------
function varargout = open_model_button_Callback(h, eventdata, handles, varargin)
%Callback from open model button
[filename model_path] = uigetfile('*.mdl;*.slx','Select the model to open'); %get the source_file
if filename~=0,
    if ~cd(model_path)
        errordlg(sprintf(['SIMarchitect: could not change to the model directory ''%s',...
            '''. \n\nIt is possible that the model cannot be runned!'], model_path));
    end
    
    load_system(filename(1:end-4)); vlag=0;

    try
        %% try to find work directory
        block=char(find_system(gcs,'Tag','ParameterSetting'));
        mask_values=get_param(block,'MaskValues');
        work_dir=mask_values{1};
        
        %% change directory to working directory if it exists, otherwise look for subdirectory parameterfiles in current dir and
        %% change to that one
        if ~strcmp(pwd,work_dir),                            %no need if already in directory
            if exist(work_dir,'dir') == 7,                          %directory exists
                cd(work_dir);                                       %change dir and echo this
                disp(['______SIMarchitect message _____________________________________________________'])
                disp(['Changed current directory to the parameterfiles directory of the model:'])
		disp(work_dir);                   
                disp(['___________________________________________________________________________'])                
            elseif exist('parameterfiles','dir') == 7               %check for subdirectory 'parameterfiles'
                cd('parameterfiles');
                disp(['______SIMarchitect message _____________________________________________________'])
                disp(['Changed current directory and the parameterfiles-directory of the model to:'])
                disp([model_path,'parameterfiles'])
                disp(['___________________________________________________________________________'])                
                mask_values{1}=[model_path,'parameterfiles'];
                set_param(block,'MaskValues',mask_values);
                vlag=1;
            end
        end        
    end
    open_system([model_path,filename(1:end-4)]);
    if vlag
                 answ=questdlg(['SIMarchitect automatically changed the parameterfiles directory for this model to the "parameterfiles" '...
                    'directory immediately below the model directory, do you want to save '...
                    'this change (recommended)?'],'SIMarchitect question','Yes','No','Yes');
                if strcmp(answ,'Yes'),
                    save_system
                end
    end
    
    delete(handles.figure1);
end

% --------------------------------------------------------------------
function varargout = documentation_button_Callback(h, eventdata, handles, varargin)
% Callback for documentation button for output file
SIMarchitect_open_help;


% --------------------------------------------------------------------
function varargout = library_button_Callback(h, eventdata, handles, varargin)
% Callback for documentation Tutorial button for output file
web('http://openmbd.com/wiki/SIMarchitect_Tutorial','-browser');


% --------------------------------------------------------------------
function varargout = open_examples_button_Callback(h, eventdata, handles, varargin)
% Callback for open examples button for output file
set(handles.figure1,'Visible','off');
bool=menu_test_model;
if bool,    %a model was opened, so close the start figure
    delete(handles.figure1);
else
    set(handles.figure1,'Visible','on');
end


% --------------------------------------------------------------------
function varargout = email_button_Callback(h, eventdata, handles, varargin)
% Callback for Email us button for output file
% BUG!!
pause(1);
web mailto:NOT-DEFINED@UNDEFINED.UNKNOWN

% --------------------------------------------------------------------
function handles = createMenu( parent, handles )

if isfield(handles, 'menu_model')
    return;
end

% Menu - Model
handles.menu_model = uimenu(parent, 'Label', 'Model'); % Changed to file from workspace

uimenu(handles.menu_model,...
       'Label',    'New Model',...
       'callback', @menuNewModel);
                 
uimenu(handles.menu_model,...
       'Label',    'Open Model',...
       'callback', @menuOpenModel);
   
uimenu(handles.menu_model,...
       'Label',    'Model Examples',...
       'callback', @menuExampleModels,...
       'Separator','on');

uimenu(handles.menu_model,...
       'Label',    'Close',...
       'callback', @close_request_Callback,...
       'Separator','on');

% Menu - Help
handles.menu_help = uimenu(parent, 'Label', 'Help');

uimenu(handles.menu_help,...
       'Label',    'Help',...
       'callback', @menuHelp);
                 
uimenu(handles.menu_help,...
       'Label',    'Tutorial',...
       'callback', @menuHelpTutorial);

uimenu(handles.menu_help,...
       'Label',    'SIMarchitect General',...
       'callback', @menuHelpDocumentation,...
       'Separator','on');

components = get_installed_components;
for i=1:length(components)
    component = components{i};
    info = get_component_info(component);
    
    if isfield(info, 'help')
        uimenu(handles.menu_help,...
               'Label',    info.name,...
               'UserData', info.help,...
               'callback', @menuHelpComponent);
    end
end
   
uimenu(handles.menu_help,...
       'Label',    'E-mail us',...
       'callback', @menuHelpEmail,...
       'Separator','on');
   
function handles = getHandlesOfMenuId ( h )

while strcmp( lower(get(h, 'Type')), 'figure' ) && ~isempty( get(h, 'parent') )
    h = get(h, 'Parent');
end

handles = guidata(h);

function menuNewModel ( varargin )
new_model_button_Callback(varargin{1}, [], getHandlesOfMenuId(varargin{1}));

function menuOpenModel ( varargin )
open_model_button_Callback(varargin{1}, [], getHandlesOfMenuId(varargin{1}));

function menuExampleModels ( varargin )
open_examples_button_Callback(varargin{1}, [], getHandlesOfMenuId(varargin{1}));

function menuHelpDocumentation ( varargin )
documentation_button_Callback(varargin{1}, [], getHandlesOfMenuId(varargin{1}));

function menuHelpTutorial ( varargin )
library_button_Callback(varargin{1}, [], getHandlesOfMenuId(varargin{1}));

function menuHelp ( varargin )
help_button_Callback(varargin{1}, [], getHandlesOfMenuId(varargin{1}));

function menuHelpEmail ( varargin )
email_button_Callback(varargin{1}, [], getHandlesOfMenuId(varargin{1}));

function menuHelpComponent ( varargin )
SIMarchitect_open_help('', get(varargin{1}, 'UserData'));

%EOF
