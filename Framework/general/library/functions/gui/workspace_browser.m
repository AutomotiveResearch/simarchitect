%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function output = workspace_browser(varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WORKSPACE_BROWSER(ACTION,FIGURE HANDLE, DATA);
% Action:
%   'create'
%   'select'
%   'refresh'
%   'close'
%
% FIGURE must be a hanlde to a valid figure
% DATA must be a structure
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isempty(varargin),
    %if no arguments given, try to create the browser
    try
        handles = create_fig;
        data    = eval_workspace;
        create_browser(handles,data);
    catch
        errordlg(lasterr);
    end
else
    action=varargin{1};
    switch action
    case 'create'
        if nargin==3,
            figH=varargin{2};
            data=varargin{3};
            create_browser(figH,data)
        elseif nargin==2,
            figH=create_fig;
            data=varargin{2};
            create_browser(figH,data)
        elseif nargin==1,
            figH=create_fig;
            data= eval_workspace;
            create_browser(figH,data)
        end
    case 'update'
        update
    case 'evaluate'
        evaluate
    case 'refresh'
        handles=get(gcbf,'Userdata');
        data= eval_workspace;
        create_browser(handles,data);
    case 'edit'
        edit
    case 'close'
        close(gcf);
    end
end






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function create_browser(varargin)

handles=varargin{1};
data = varargin{2};

handles.list=treeview('Create',handles.fig,data);

set(handles.list,...
    'Units',               'pixels', ...
    'Callback',            'workspace_browser update',...
    'Position',            [16.0500   139.0000  270.9000  450.0000],...
    'Backgroundcolor',     [0.75 0.75 0.75],...
    'Visible',             'on',...
    'Enable',              'on', ...
    'Max',                 1,...
    'Min',                 0,...
    'value',               1,...
    'Tag',                'TheListBox');    %create listbox

set(handles.fig,'Visible','on');
set(handles.fig,'Userdata',handles);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
function handles=create_fig

handles.fig=figure('Position',[701    84   301   600],...
    'numbertitle','off',...
    'Name',['SIMarchitect: Workspace Browser'],...
    'MenuBar','none',...
    'Color',[0 0.1882 0.5568],...
    'Resize','off',...
    'Visible','off');  %create new figure

%header of value
handles.TitleValue = uicontrol(handles.fig, ...
    'Style',               'text',...
    'HorizontalAlignment', 'left', ...
    'Units',               'Pixels',... %'normalized', ...
    'Position',            [16.0500   110.0000  270.9000   15.0000],... %[0.05 0.09 0.9 0.05],...
    'ForegroundColor',               [1 1 1],...
    'BackgroundColor',     [0 0.1882 0.5568],...
    'String',              'Value',...
    'Visible',             'on',...
    'Tag',                 'EditFieldTitle'...
    );

%value field
handles.TxtValue = uicontrol(handles.fig, ...
    'Style',               'text',...
    'HorizontalAlignment', 'left', ...
    'Units',               'Pixels',... %'normalized', ...
    'Position',            [16.05 89 185 20],... %[0.05 0.09 0.9 0.05],...
    'BackgroundColor',     [0.75 0.75 0.75],...    
    'String',              'none',...
    'Visible',             'on',...
    'Tag',                 'EditField',...
    'Enable',              'on' ...
    );

%edit button
handles.BtnValue = uicontrol(handles.fig, ...
    'Style',               'pushbutton',...
    'HorizontalAlignment', 'left', ...
    'Units',               'Pixels',... 
    'Callback',            'workspace_browser(''edit'')',...
    'Position',            [211.7 89 75.25 20],... %[0.05 0.09 0.9 0.05],...
    'BackgroundColor',     [0.75 0.75 0.75],...    
    'String',              'Edit',...
    'Visible',             'on',...
    'Tag',                 'EditField',...
    'Enable',              'on' ...
    );


%header of edit
handles.TitleString = uicontrol(handles.fig, ...
    'Style',               'text',...
    'HorizontalAlignment', 'left', ...
    'Units',               'Pixels',... %'normalized', ...
    'Position',            [16.0500   61.0000  270.9000   15.0000],... %[0.05 0.09 0.9 0.05],...
    'ForegroundColor',               [1 1 1],...
    'BackgroundColor',     [0 0.1882 0.5568],...
    'String',              'String',...
    'Visible',             'on',...
    'Tag',                 'EditFieldTitle'...
    );


%edit field
handles.TxtString = uicontrol(handles.fig, ...
    'Style',               'edit',...
    'HorizontalAlignment', 'left', ...
    'Units',               'Pixels',... %'normalized', ...
    'Position',            [16.0500   40.0000  270.9000   20.0000],... %[0.05 0.09 0.9 0.05],...
    'String',              'none',...
    'Visible',             'on',...
    'Tag',                 'EditField',...
    'Enable',              'on' ...
    );


%eval button
handles.BtnEval = uicontrol(handles.fig, ...
    'Style',               'Pushbutton',...
    'Callback',            'workspace_browser(''evaluate'')',...
    'HorizontalAlignment', 'left', ...
    'Units',               'Pixels',... %'normalized', ...
    'Position',            [16.0500   11.0000   75.2500   20.0000],... %[0.05 0.025 0.25 0.05],...
    'String',              'Evaluate',...
    'Visible',             'on',...
    'Enable',              'on' ...
    );

%refresh button
handles.BtnRefresh = uicontrol(handles.fig, ...
    'Style',               'Pushbutton',...
    'Callback',            'workspace_browser(''refresh'')',...
    'HorizontalAlignment', 'left', ...
    'Units',               'Pixels',... %'normalized', ...
    'Position',            [113.8750   11.0000   75.2500   20.0000],... %[0.375 0.025 0.25 0.05],...
    'String',              'Refresh',...
    'Visible',             'on',...
    'Enable',              'on' ...
    );

%close button
handles.BtnClose = uicontrol(handles.fig, ...
    'Style',               'Pushbutton',...
    'Callback',            'workspace_browser(''close'')',...
    'HorizontalAlignment', 'left', ...
    'Units',               'Pixels',... %'normalized', ...
    'Position',            [211.7000   11.0000   75.2500   20.0000],... %[0.70 0.025 0.25 0.05],...
    'String',              'Close',...
    'Visible',             'on',...
    'Enable',              'on' ...
    );

set(handles.fig,'Userdata',handles);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
function [output] = get_selection

handles=get(gcbf,'Userdata');

selected=get(handles.list,'Value');
if isempty(selected),
    warndlg('Nothing selected');
    output=[-1];
    return
end
treeData=get(handles.list,'Userdata');
[dispData idx] = FindDisplayedData(treeData);
selectedData=dispData(selected);
for i=1:length(selectedData),
    selectedDataName{i}=selectedData(i).Fullname;
end
output=selectedDataName;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
function vars=eval_workspace
%function is called by workspace_browser

vars_in_ws=evalin('base',['whos;']);
vars=[];
%create an empty structure array with the contents of the workspace
for i=1:length(vars_in_ws),
    structure=evalin('base',['isstruct(',vars_in_ws(i).name,')']);
    cell_array=evalin('base',['iscell(',vars_in_ws(i).name,')']);
    if structure,
        [j,vars_in_struct]=get_vars_in_struct(vars_in_ws(i).name);   %get variables in structure in a cell array
        for i=1:length(vars_in_struct),
            eval(['vars.',vars_in_struct{i},'=[];']); 
        end
        
    elseif cell_array,
        disp('cell array encountered, is not included in WS-Browser');
    else
        eval(['vars.',vars_in_ws(i).name,'=[];']);   %add to structure array
    end
end
if isempty(vars),
    vars.none=[];  %create a dummy entry to show workspace is empty
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [j,vars]=get_vars_in_struct(name,j,vars)
global local
if nargin==1,
    j=1;
    vars=[];
end

fields=evalin('base',['fieldnames(',name,')']);	%cell array with field names
for ii=1:length(fields),
    field=[name,'.',fields{ii}];
    var_class=evalin('base',['class(',field,');']);
    if strcmp(var_class,'struct'),
        [j,vars]=get_vars_in_struct(field,j,vars);
    else
        vars{j,1}=field;
        j=j+1;
    end                       
end      



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [j,vars]=get_vars_in_cell(cell_array,string,vars,j)
%STILL TO COME ?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
function [dd, idx] = FindDisplayedData(ud)

dd  = ud;
idx = [];
rem = [];

for i = 1:length(ud)
    if ud(i).IsDisplayed
        idx = [idx i];
    else
        rem = [rem i];
    end
end
dd(rem) = [];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
function update

handles =get(gcbf,'Userdata');

%update the listbox
treeview Update

%get the selected value
string = char(get_selection);

%update the string field
set(handles.TxtString,'String',string);

%update the value field
dim = evalin('base',['size(',string,');']);
isstrct = evalin('base',['isstruct(',string,');']);
isstrng = evalin('base',['isstr(',string,');']);
if sum(dim)==2 & ~isstrct &~isstrng,
    val = evalin('base',[string,';']);
    set(handles.TxtValue,'String',num2str(val),'Enable','on');
    set(handles.BtnValue,'Enable','on');    
elseif isstrct
    set(handles.TxtValue,'String','','Enable','off');
    set(handles.BtnValue,'Enable','off');    
elseif isstrng
    val = evalin('base',[string,';']);    
    set(handles.TxtValue,'String',val,'Enable','on');
    set(handles.BtnValue,'Enable','off');
else
    set(handles.TxtValue,'String',[num2str(dim(1)),'x',num2str(dim(2))],'Enable','on');
    set(handles.BtnValue,'Enable','on');    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
function evaluate

handles =get(gcbf,'Userdata');
string = char(get(handles.TxtString,'String'));
disp([string,':']);
evalin('base',[string]);
%string,'=',

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
function edit

handles =get(gcbf,'Userdata');
string = char(get_selection);

evalin('base',['assignin(''caller'',''variable'',',string,');']);

%activeX
Excel = actxserver('Excel.Application');
set(Excel,'Visible',1);

Workbooks = Excel.Workbooks;
Workbook = invoke(Workbooks,'Add');

Sheets = Excel.ActiveWorkBook.Sheets;
sheet1 = get(Sheets,'Item',1);
invoke(sheet1,'Activate');

Activesheet = Excel.ActiveSheet;

%construct the endpoint to set the activesheetrange
dim = size(variable);
indx = 'abcdefghijklmnopqrstuvwxyz';
endp = [indx(dim(2)),num2str(dim(1))];

Activesheetrange = get(Activesheet,'Range','A1',endp);

%set the array in Excel
set(Activesheetrange,'Value',variable);

%Create a message box to get the time where we have to close excel
h=msgbox('Press OK to close Excel and retrieve the table.');
waitfor(h);

used_range=get(sheet1,'UsedRange');
variable = used_range.value;
if iscell(variable),
    variable = reshape([variable{:}],size(variable)); %convert 2 double
end


assignin('base','variable',variable); %cannot assign a field in a structure
evalin('base',[string,'=variable; clear variable;']);

%close excel
Workbook.Saved = 1;
invoke(Workbook,'Close');
invoke(Excel,'Quit');
delete(Excel);

update;
