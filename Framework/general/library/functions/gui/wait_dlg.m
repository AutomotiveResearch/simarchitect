%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
function handle=wait_dlg(string)

old_setting=get(0,'Units');
set(0,'Units','pixels');
screen=get(0,'Screensize');
set(0,'Units',old_setting);

pos=[screen(3)*0.35,...
        screen(4)*0.45,...
        screen(3)*0.3,...
        screen(4)*0.1];

handle=figure('Name','SIMarchitect: Message',...
    'NumberTitle','off',...
    'MenuBar','none',...
    'Units','Pixels',...
    'Visible','off',...
    'Resize','off',...
    'WindowStyle','modal');
set(gcf,'Pointer','watch');
txt=uicontrol(handle,'Style','Text',...
    'Units','Pixels',... 
    'FontSize',12,...
    'Position',[2 2 pos(3)-4 pos(4)-4],...
    'String',sprintf('\n%s',string));


set(handle,...
    'Position',pos,...
    'Visible','on');


    
    