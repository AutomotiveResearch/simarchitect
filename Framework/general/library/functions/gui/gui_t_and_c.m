%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function varargout = gui_t_and_c(varargin)
% DEV_GUI_1 Application M-file for Dev_GUI_1.fig
%    FIG = DEV_GUI_1 launch Dev_GUI_1 GUI.
%    DEV_GUI_1('callback_name', ...) invoke the named callback.

% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018

%%%%%Comments

persist_workdir; % remember the working directory!

if nargin == 0  % LAUNCH GUI
    
    fig = openfig(mfilename,'reuse');

    % Generate a structure of handles to pass to callbacks, and store it. 
    handles = guihandles(fig);
    
    % Get the menu data back into the GUIDATA!!
    hls = guidata(fig);
    if ~isempty(hls)
        fn = fieldnames(hls);
        for i=1:length(fn)
            if ~isfield(handles, fn{i})
                handles.(fn{i}) = hls.(fn{i});
            end
        end
    end
    
    handles = createMenu(fig, handles);
    guidata(fig, handles);
    
    %set logo in figure
    [logo, ~, alpha] = imread('SIMarchitect1000px.png');

    % Set the close function
    %set(fig, 'CloseRequestFcn', @close_request_Callback);
    
    % SIMT-3004: Changed the order of the code while MATLAB did not display
    % the figure correctly.
    
    %center figure on screen
    center_fig(fig);
    set(fig,'Visible','on');
    
    %---GUIDE generated code-----------------------------------------    
    if nargout > 0
        varargout{1} = fig;
    end
    
elseif ischar(varargin{1}) % INVOKE NAMED SUBFUNCTION OR CALLBACK
    
    try
        [varargout{1:nargout}] = feval(varargin{:}); % FEVAL switchyard
    catch
        disp(lasterr);
    end
    
end

% Close request------------------------------------------------------------------
function varargout = close_request_Callback(varargin)
persist_workdir('forget'); % Forget the "old" working directory!
closereq;

% Pushbuttons--------------------------------------------------------------------
function varargout = close_button_Callback(h, eventdata, handles, varargin)
%Callback from Cancel button
%Close figure, no further actions undertaken
close_request_Callback(varargin);
warning on

function varargout = accept_tc(h, eventdata, handles, varargin)
% License and Disclaimer Accepted
assignin('base','Accept_tc',1); %% Value of 1 to SIMarchitect Installer
close_request_Callback(varargin); % Close GUI at button click


% --------------------------------------------------------------------
function varargout = decline_tc(h, eventdata, handles, varargin)
% License and Disclaimer Declined
assignin('base','Accept_tc',0); %% Value of 0 to SIMarchitect Installer
close_request_Callback(varargin); % Close GUI at button click

% --------------------------------------------------------------------
function handles = createMenu( parent, handles )

if isfield(handles, 'menu_model')
    return;
end


function handles = getHandlesOfMenuId ( h )

while strcmp( lower(get(h, 'Type')), 'figure' ) && ~isempty( get(h, 'parent') )
    h = get(h, 'Parent');
end

handles = guidata(h);

function menuHelp ( varargin )
help_button_Callback(varargin{1}, [], getHandlesOfMenuId(varargin{1}));

function menuHelpComponent ( varargin )
SIMarchitect_open_help('', get(varargin{1}, 'UserData'));

%EOF
