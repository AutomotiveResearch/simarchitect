%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
% Open index.html and replade the frame source to that of
% the html file to be opened.
function createindex(varargin)
if nargin==1,
    modulename=varargin{1};
    sheetname = ['modules\\',modulename,'.html'];
elseif nargin==2,
    page=varargin{1};
    sheetname = [page,'.html'];
   
end
    
    file = fopen([SIMarchitect_root,'\documentation\html_help\index_module.html'],'w');
    
    %Write .html file
    fprintf(file,'<html>\n\r'); 
    
    fprintf(file,'<frameset>\n\r');
    
    fprintf(file,'\t<frameset cols="210,*">\n');
    fprintf(file,'\t\t<frameset rows="68,*">\n');
    fprintf(file,'\t\t\t<frame src="toc_title.html" frameborder="0" marginwidth="2" marginheight="2" name="toctitleframe">\n');
    fprintf(file,'\t\t\t<frame src="toc.html" frameborder="0" marginwidth="10" marginheight="15" name="tocframe">\n');
    fprintf(file,'\t\t</frameset>\n');
    eval(['fprintf(file,''\t\t<frame src="' sheetname '" frameborder="0" marginwidth="10" marginheight="15" name="mainframe">\n'');']);
    fprintf(file,'\t</frameset>\n\r');
    fprintf(file,'\t<noframes>\n');
    fprintf(file,'\t\tThis browser doesn''t support frames.<br>\n');
    fprintf(file,'\t\tPlease use the <a href="about.html">no-frame version</a>.\n');
    fprintf(file,'\t</noframes>\n\r');    
    fprintf(file,'</frameset>\n\r');    
    fprintf(file,'</html>');
    %end   
    


fclose(file);

