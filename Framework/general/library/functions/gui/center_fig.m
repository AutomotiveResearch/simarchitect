%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function center_fig(figH)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Function: center_fig
%   Description: CENTER_FIG(FIGH) Centers the figure with handle 'FIGH' on the screen, is used for GUI's
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin==0,
    figH=gcf;   %just do the curent figure if no handle input
end
if ishandle(figH)
    try
        %% Get screen dimensions in pixels
        old_units=get(0,'Units');   %save old units setting
        if ~strcmp(old_units,'pixels'),
            set(0,'Units','pixels');        %set units to pixels
            screen_dim=get(0,'screensize'); %get the dimensions
            set(0,'Units',old_units);       %set the units back to old setting
        else
            screen_dim=get(0,'screensize'); %get the dimensions
        end
        
        %% Get figure dimensions in pixels
        old_units=get(figH,'Units');            %save old units setting
        if ~strcmp(old_units,'pixels'),
            set(figH,'Units','pixels');         %set the units to pixels
            fig_pos=get(figH,'Position');       %get the position in pixels
        else
            fig_pos=get(figH,'Position');  %get the position in pixels
        end
        
        %% Calculate new position and set it to the figure
        new_pos=[0.5*screen_dim(3)-0.5*fig_pos(3),...   %left
                0.5*screen_dim(4)-0.5*fig_pos(4),...    %bottom
                fig_pos(3),...                      %width
                fig_pos(4)];                        %height
        
        set(figH,'Position',new_pos);
        set(figH,'Units',old_units);        %set the units back to old setting        
    catch
        disp(['SIMarchitect: Error ',lasterr]);
    end
end