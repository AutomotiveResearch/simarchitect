%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function varargout = gui_bus_selector(varargin)
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % gui_bus_selector(VARARGIN)
% % 
% % Author: SIMarchitect ADMIN
% % Version: 1.0 
% % Date: 08-11-2018
% % 
% % Description:
% %   This function creates the block dialog for the SIMarchitect Input Selector. 
% %   It has extra fields for connection information and help button (added part of the 
% %   textwrap function to create a good looking string for the connection information.)
% %
% %   File calls the get_param_file function.
% %
% % revision history see end of file
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
dialog_handle = -1;
errorFlag     = 0;

if nargin==3
    BlockHandle = varargin{1};
    Command     = varargin{2};
    Bus         = varargin{3};    
    OpenBlockDialog(BlockHandle, Bus);
end

if nargin==1
    switch varargin{1}
        case 'UpdateButtons'
            UpdateButtons;
            UpdateList
        case 'UpdateList'
            UpdateList
        case 'select'
            SelectSignals;
        case 'find'
            FindSignals;
        case 'refresh'
            RefreshSignals;
        case 'up'
            MoveUpSignals;
        case 'down'
            MoveDownSignals;
        case 'remove'
            RemoveSignals;
        case 'apply'
            errorFlag = ApplyDialog;
        case 'help'
            HelpDialog;
        case 'cancel'
            errorFlag = CloseDialog('cancel');
        case 'ok'
            errorFlag = CloseDialog('save');
        case 'resize',
            locResizeFig(gcbf);
        case 'ModHelp',
            locModHelp;
    end
    
    
end

if nargin==2,
    blockH = varargin{2};
    switch varargin{1}
        case 'delete'
            DeleteDialog(blockH);
        case 'nameChange'
            UpdateDialogName(blockH);
    end
end

if (errorFlag)
    errordlg(lasterr);
end

if nargout,
    varargout{1}=dialog_handle;
end


% Function: OpenBlockDialog ====================================================
% Abstract: 
%   Function to create the block dialog for the selected Bus selector block.
%
function H = OpenBlockDialog(blockH, busData)

% Check to see if the block is in a Library
if (strcmp(get_param(bdroot(blockH),'Lock'), 'on'))
  errordlg('Bus Selector must be placed in a model in order to operate.',...
	   'Error', 'modal')
  return;
end

% Check to see if block has a dialog
updateDataOnly = 0;
UserData = get_param(blockH, 'UserData');
if isfield(UserData,'InterfaceHandle')
    if ~isempty(UserData.InterfaceHandle) & ishandle(UserData.InterfaceHandle) & UserData.InterfaceHandle~=-1
        figure(UserData.InterfaceHandle);
        updateDataOnly = 1;
    end
end

% If it is update only, then we don't have to create
% the dialog again.
if ~updateDataOnly
  UserData.InterfaceHandle = CreateBlockDialog(blockH, busData);
end

Data = get(UserData.InterfaceHandle, 'UserData');
% Update the signal list
if updateDataOnly
  treeview('Create', Data.TreeList, busData);
end

% Update the selected list
if updateDataOnly
  inputArray = StripSelection(get(Data.SelectedList, 'String'));
else
  inString = get_param(blockH, 'OutputSignals');
  inputArray = DelimitString(inString, ',');
end
modifiedArray = CheckSelection(inputArray, busData);
set(Data.SelectedList, 'String', modifiedArray)

% Show the dialog
% set(H, 'visible', 'on');
set(UserData.InterfaceHandle, 'visible', 'on');

% Function: locResizeFig =======================================================
% Abstract:
%
function locResizeFig(figHandle)

Data=get(figHandle,'UserData');

set(figHandle,'Units','pixels');
figPos=get(figHandle,'Position');

allHandles=[figHandle Data.F0 Data.T0 Data.L0 Data.F1 Data.T1 Data.F2 ...
	    Data.T2 Data.TreeList Data.SelectedList ...
	    Data.UpButton Data.DownButton Data.RemoveButton Data.FindButton ...
	    Data.SelectButton Data.RefreshButton Data.OKButton ...
	    Data.CancelButton Data.HelpButton Data.ApplyButton ...
        Data.ModHelpButton Data.T2inp Data.T3inp Data.Finp Data.Tinp];  %removed Data.MuxedOutput /JE

for i=1:length(allHandles)
    set(allHandles(i),'Units','characters');
end

offset=1;
btnWidth=12;
btnHeight=1.75;
txtMove=.5;

figPos=get(figHandle,'Position');

% Start on bottom row
posApply = [figPos(3)-offset-btnWidth offset btnWidth btnHeight];

posHelp = posApply;
posHelp(1)=[posApply(1)-2*offset-btnWidth]; 

posCancel = posHelp;
posCancel(1)=[posHelp(1)-2*offset-btnWidth]; 

posOK = posCancel;
posOK(1)=[posCancel(1)-2*offset-btnWidth]; 

%% L0 = general information string
% T0 = title 'info'
% T1 = title 1 of frame 'Signals in the bus'
% T2 = title 2 of frame 'Selected Signals' 
posT0=get(Data.T0,'Position');  


posL0 = get(Data.L0,'Position');
posL0(3) = figPos(3)-4*offset;
set(Data.L0,'Position',posL0);

txt = get(Data.L0,'Userdata');
L0String = LocalTextWrap({{txt}},{},Data.L0,0);
set(Data.L0,'String',L0String);


t0e = get(Data.T0,'Extent');
L0e = get(Data.L0, 'Extent'); %for the height
T1e = get(Data.T1, 'Extent');
T2e = get(Data.T2, 'Extent');

fWidth = (figPos(3)-3*offset-(2*offset+btnWidth))/2;

posT0 = [2*offset figPos(4)-t0e(4) t0e(3) t0e(4)];

posF0(4)=L0e(4)+2*offset;
posF0=[offset posT0(2)-posF0(4) figPos(3)-2*offset posF0(4) ];

posL0=[2*offset posF0(2)+offset posF0(3)-2*offset posF0(4)-2*offset];
posT0(2)=posT0(2)-txtMove;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%position SIMarchitect items
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Tinpe = floor(get(Data.Tinp, 'Extent')); %get the extent of the text, title 'Required Inputs & Module Help'
T3inpe = floor(get(Data.T3inp, 'Extent')); %get the extent of the text (units)

posTinp= [2*offset posF0(2)-2*offset Tinpe(3) Tinpe(4)];   % the title
posTinp(2)=posTinp(2)-txtMove;

%[xmin ymin x y]
posT3inp= [ figPos(3)-4*offset-btnWidth-T3inpe(3),1,T3inpe(3),1]; %the units
posT2inp= [2*offset,1,posT3inp(1)-3*offset,1]; %the description

%dynamically size stuff
set(Data.T2inp,'Position',posT2inp);

str1 = get(Data.T2inp,'Userdata');   %get the original string 
str2 = get(Data.T3inp,'Userdata');   %get the original string 

[str1,str2] = LocalTextWrap({str1},{str2},Data.T2inp,1);%wrap original strings according to current size

set(Data.T2inp,'String',str1);
set(Data.T3inp,'String',str2);

T3inpe = floor(get(Data.T3inp, 'Extent')+[0 0 0 1]); %get the extent of the text (units)

posT2inp = [posT2inp(1) posF0(2)-T3inpe(4)-3*offset posT2inp(3) T3inpe(4)];
posT3inp = [posT3inp(1) posT2inp(2) max(1,T3inpe(3)) T3inpe(4)];

posModHelpButton = [figPos(3)-2*offset-btnWidth,... %the help button
        posT2inp(2)+posT2inp(4)-btnHeight,...
        btnWidth,...
        btnHeight];

posFinp=[offset posT2inp(2)-offset figPos(3)-2*offset posT2inp(4)+2*offset ];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


posT1=[2*offset posFinp(2)-T1e(4)-offset T1e(3) T1e(4)]; %%%%%%%%%%%%%%%%%%%%%
posF1=[offset sum(posOK([2 4]))+offset fWidth 1];
posF1(4)=posT1(2)-posF1(2);
posSelect=[sum(posF1([1 3]))-offset-btnWidth posF1(2)+offset ...
	   btnWidth btnHeight];
posRefresh = posSelect;
posRefresh(1)=posSelect(1)-offset-btnWidth;
posFind=posRefresh;
posFind(1)=posL0(1)+offset;
posTree=[posF1(1)+offset sum(posRefresh([2 4]))+offset posF1(3)-2*offset 1];
posTree(4)=sum(posF1([2 4]))-offset-posTree(2);
posT1(2)=posT1(2)-txtMove;

posT2=posT1;
posF2=posF1;
posF2(1)=sum(posF1([1 3]))+offset;
posF2(3)=figPos(3)-offset-posF2(1);
posT2(1)=posF2(1)+offset;
posUp=[posF2(1)+offset sum(posF1([2 4]))-offset-btnHeight btnWidth btnHeight];

posDown=posUp;
posDown(2)=posUp(2)-offset-btnHeight;
posRemove=posDown;
posRemove(2)=posDown(2)-offset-btnHeight;
posMuxed=[sum(posRemove([1 3]))+offset posF2(2)+offset 1 btnHeight];
posMuxed(3)=sum(posF2([1 3]))-offset-posMuxed(1);
posSelected=[posMuxed(1) sum(posMuxed([2 4]))+offset posMuxed(3) 1];
posSelected(4)=sum(posF1([2 4]))-offset-posSelected(2);

allPos=[posF0; posT0; posL0; posF1; posT1; posF2; posT2; posTree; posSelected;
	posUp;posDown;posRemove;posSelect;posRefresh;posFind;posOK;
	posCancel;posHelp;posApply;posT2inp;posT3inp;posFinp;posTinp];  %removed posMuxed /JE

%set postion SIMarchitect items
set(Data.T2inp,'Position',posT2inp);
set(Data.T3inp,'Position',posT3inp);
set(Data.Finp,'Position',posFinp);
set(Data.Tinp,'Position',posTinp);
set(Data.ModHelpButton,'Position',posModHelpButton);

set(Data.F0,'Position',posF0);
set(Data.T0,'Position',posT0);
set(Data.L0,'Position',posL0);
set(Data.F1,'Position',posF1);
set(Data.T1,'Position',posT1);
set(Data.F2,'Position',posF2);
set(Data.T2,'Position',posT2);
set(Data.TreeList,'Position',posTree);
set(Data.SelectedList,'Position',posSelected);
%set(Data.MuxedOutput,'Position',posMuxed);
set(Data.UpButton,'Position',posUp);
set(Data.DownButton,'Position',posDown);
set(Data.RemoveButton,'Position',posRemove);
set(Data.SelectButton,'Position',posSelect);
set(Data.RefreshButton,'Position',posRefresh);
set(Data.FindButton,'Position',posFind);
set(Data.OKButton,'Position',posOK);
set(Data.CancelButton,'Position',posCancel);
set(Data.HelpButton,'Position',posHelp);
set(Data.ApplyButton,'Position',posApply);

set(allHandles,'Units','normalized');


% Function: CreateBlockDialog ==================================================
%
function H = CreateBlockDialog(blockH, busData)  

UserData=get(blockH,'UserData');

gray = get(0,'defaultuicontrolbackgroundcolor');
black = [0 0 0];
white = [1 1 1];
backgroundcolor=[0.95 0.95 0.95]; %grey
foregroundcolor=[0 69 123]/255; %blue
textcolor=[0 69 123]/255; %blue
%
% Create the figure for the block dialog
%
dialogPos = [1 1 580 580];
H = figure(...
    'numbertitle', 'off',...
    'name',        ['SIMarchitect: bus selector, select input signals for con_i'], ...
    'menubar',     'none', ...
    'visible',     'off', ...
    'HandleVisibility', 'callback', ...
    'IntegerHandle','off', ...
    'color',        backgroundcolor, ...
    'Units',        'pixels', ...
    'Resize',       'on', ...
    'Position',     dialogPos);
set(H, 'DeleteFcn', {@DeleteFcn});

Data.BlockHandle = blockH; 
Data.BlockDialogHandle = H;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Create extra SIMarchitect items
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Data.Finp = uicontrol(H, ... %inputs and help Frame
		    'style', 'frame', ...
		    'backgroundcolor', backgroundcolor,...
            'foregroundcolor',foregroundcolor ...
		    );

Data.Tinp = uicontrol(H, ...  %inputs and help title
		    'style','text', ...
		    'backgroundcolor', backgroundcolor, ...
		    'String', 'Required inputs & Module help',...
            'foregroundcolor',textcolor ...
            );

Data.ModHelpButton = uicontrol(H, ...
			    'Style', 'pushbutton', ...
			    'Backgroundcolor', gray, ...
			    'Enable', 'on', ...
			    'String', 'ModHelp', ...
			    'Callback', 'gui_bus_selector ModHelp;');
            
            
[str1,str2] = create_input_string;
            
Data.T2inp = uicontrol(H, ...      %Inputs and help Text field2 (description)
            'style','text',...
            'horizontalalignment', 'left', ...
            'backgroundcolor',white,... %backgroundcolor,...
            'Userdata',str1,...
            'foregroundcolor',black... %textcolor ...
            );        
            
Data.T3inp = uicontrol(H, ...      %Inputs and help Text field3 (units)
            'style','text',...
            'horizontalalignment', 'left', ...
            'backgroundcolor',white,...%backgroundcolor,...
            'Userdata',str2,...
            'String',str2,...            
            'foregroundcolor',black... %textcolor ...
            );        
            
            
% Create MAIN frame
Data.F0 = uicontrol(H, ...
		    'style', 'frame', ...
		    'backgroundcolor', backgroundcolor,...
            'foregroundcolor',foregroundcolor ...
		    );

Data.T0 = uicontrol(H, ...
		    'style','text', ...
		    'backgroundcolor', backgroundcolor, ...
		    'String', 'Info',...
            'foregroundcolor',textcolor ...
            );

%DescStr = get_param(blockH, 'BlockDescription');
DescStr = sprintf(['Select the signals for the con_i port. The field below shows a description of the signals, the required order and the required units. '...
        'Use the ModHelp button to get more help on this module.\n\n'...
        'The left listbox shows the signals in the input bus. The right listbox shows the selections.\n'...
        'Use the Select button to select the output signals. Use the Up, Down, or Remove button to reorder the selections.']);

Data.L0 = uicontrol(H, ...
		    'style', 'text', ...
		    'backgroundcolor', backgroundcolor, ...
		    'max', 2, ...
		    'min', 0, ...
		    'horizontalalignment', 'left', ...
		    'value', [], ...
		    'Userdata', DescStr,...     %JE modified Userdata instead of String, better for resising
            'foregroundcolor',textcolor ...
            );

%
% Create 2 frames and their titles
%
Data.F1 = uicontrol(H, ...
		    'style', 'frame', ...
		    'backgroundcolor', backgroundcolor,...
            'foregroundcolor',foregroundcolor ...
            );

Data.T1 = uicontrol(H, ...
		    'style','text', ...
		    'backgroundcolor', backgroundcolor, ...
		    'String', 'Signals in the bus',...
            'foregroundcolor',textcolor ...
            );

Data.F2 = uicontrol(H, ...
		    'style', 'frame', ...
		    'backgroundcolor', backgroundcolor,...
            'foregroundcolor',foregroundcolor ...
            );

Data.T2 = uicontrol(H, ...
		    'style','text', ...
		    'backgroundcolor', backgroundcolor, ...
		    'String', 'Selected signals',...
            'foregroundcolor',textcolor ...
            );       
%
% Create the 2 listboxes to hold the Bus information
% and the selected data.
%
try
  Data.TreeList = treeview('Create', H, busData);
  set(Data.TreeList,'FontName','FixedWidth',...
      			      'Backgroundcolor', gray,...
                      'Callback','gui_bus_selector UpdateList;')
                  
catch
  errordlg('Incorrect signal specification');
  close(H)
  return
end

Data.SelectedList = uicontrol(H, ...
			      'Style', 'Listbox', ...
			      'Max', 2, ...
			      'Min', 0, ...
			      'FontName','FixedWidth', ...
			      'Callback','gui_bus_selector UpdateButtons;', ...
			      'String', {}, ...
			      'Value', [], ...
			      'Backgroundcolor', gray);

%muxValue = get_param(blockH, 'MuxedOutput');

%Data.MuxedOutput = uicontrol(H, ...
%			     'Style', 'checkbox', ...
%			     'Value', strcmp(muxValue, 'on'), ...
%			     'backgroundcolor', gray, ...
%			     'Enable', 'on', ...
%			     'HorizontalAlignment', 'left', ...
%			     'String', 'Muxed output');

% 
% Create the buttons that will be used to manipulate the selection
% list 
%
Data.UpButton = uicontrol(H, ...
			  'Style', 'pushbutton', ...
			  'backgroundcolor', gray, ...
			  'String', 'Up', ...
			  'Enable', 'off', ...
			  'Callback', 'gui_bus_selector up;');

Data.DownButton = uicontrol(H, ...
			    'Style', 'pushbutton', ...
			    'backgroundcolor', gray, ...
			    'String', 'Down', ...
			    'Enable', 'off', ...
			    'Callback', 'gui_bus_selector down;');

Data.RemoveButton = uicontrol(H, ...
			      'Style', 'pushbutton', ...
			      'backgroundcolor', gray, ...
			      'String', 'Remove', ...
			      'Enable', 'off', ...
			      'Callback', 'gui_bus_selector remove;');

%
% Create the buttons for bus list
%
Data.SelectButton = uicontrol(H, ...
			      'Style', 'pushbutton', ...
			      'backgroundcolor', gray, ...
			      'String', 'Select >>', ...
			      'Callback', 'gui_bus_selector select;');

%
% Create the refresh button
%
Data.RefreshButton = uicontrol(H, ...
			       'Style', 'pushbutton', ...
			       'backgroundcolor', gray, ...
			       'String', 'Refresh', ...
			       'Callback', 'gui_bus_selector refresh;');

%
% Create the find button
%
Data.FindButton = uicontrol(H, ...
			    'Style',           'pushbutton', ...
			    'backgroundcolor', gray, ...
			    'String',          'Find', ...
			    'Callback',        'gui_bus_selector find;');

%
% Create the "standard" buttons 
%
Data.OKButton = uicontrol(H, ...
			  'Style', 'pushbutton', ...
			  'backgroundcolor', gray, ...
			  'String', 'OK', ...
			  'Callback', 'gui_bus_selector ok;');

Data.CancelButton = uicontrol(H, ...
			      'Style', 'pushbutton', ...
			      'Backgroundcolor', gray, ...
			      'String', 'Cancel', ...
			      'Callback', 'gui_bus_selector cancel;');

Data.HelpButton = uicontrol(H, ...
			    'Style', 'pushbutton', ...
			    'Backgroundcolor', gray, ...
			    'Enable', 'on', ...
			    'String', 'Help', ...
			    'Callback', 'gui_bus_selector help;');

Data.ApplyButton = uicontrol(H, ...
			     'Style', 'pushbutton', ...
			     'backgroundcolor', gray, ...
			     'String', 'Apply', ...
			     'Callback', 'gui_bus_selector apply;');

Data.origSize=[0 0];
Data.hilitedBlk = -1;
set(H,'UserData',Data);
locResizeFig(H);

set(H,'Units','pixels');
dialogPos = get(H,'Position');
bdPos     = get_param(bdroot(gcb),'Location');

hgPos        = rectconv(bdPos,'hg');
dialogPos(1) = hgPos(1)+(hgPos(3)-dialogPos(3));
dialogPos(2) = hgPos(2)+(hgPos(4)-dialogPos(4));
% make sure the dialog is not off the screen
screenSize = get(0, 'ScreenSize');
if dialogPos(1)<0
  dialogPos(1) = 1;
elseif dialogPos(1)> screenSize(3)-dialogPos(3) 
  dialogPos(1) = (screenSize(3)-dialogPos(3))/2;
end
if dialogPos(2)<0
  dialogPos(2) = 1;
elseif dialogPos(2)> screenSize(4)-dialogPos(4) 
  dialogPos(2) = (screenSize(4)-dialogPos(4))/2;
end

Data.origSize=dialogPos(3:4);

set(H, ...
    'Position',  dialogPos, ...
    'Units',     'normalized', ...
    'UserData',  Data, ...
    'ResizeFcn', 'gui_bus_selector resize');

%
% Update the userdata only if it is a changeable block
% else show a disabled version of the gui.
%
if strcmp(get_param(blockH, 'LinkStatus'), 'none')
%   set_param(blockH, 'Figure', H)
  UserData.InterfaceHandle=H;
  set_param(blockH,'UserData',UserData);
else
  UiControls = [Data.OKButton; Data.ApplyButton; Data.RefreshButton];
  set(UiControls, 'enable', 'off');
end

% Function: ApplyDialog ========================================================
%
function errFlag = ApplyDialog
H       = gcbf;
errFlag = 0;

Data = get(H, 'UserData');

% un-hilite hilited system
if Data.hilitedBlk ~= -1
  set_param(Data.hilitedBlk, 'HiliteAncestors', 'off');
  Data.hilitedBlk = -1;
end

blockH = Data.BlockHandle;
signalList = get(Data.SelectedList, 'string');

if ~isempty(signalList)
  signals = StripSelection(signalList);
  signals = signals(:)';
  signals(2,:) = {','};
  selSignals = cat(2, signals{:});
  selSignals(end) = [];
else
  selSignals = 'empty';
end

%muxedValue = get(Data.MuxedOutput, 'value');
%muxed = 'off';
%if muxedValue==1
  muxed = 'off';
%end

% Perform the set_param only if parameters have changed
CurrentOutSignals = get_param(blockH, 'OutputSignals');
CurrentMuxed      = get_param(blockH, 'MuxedOutput');

setNeeded = 1;
if (strcmp(CurrentOutSignals, selSignals) & strcmp(CurrentMuxed, muxed))
  setNeeded = 0;
end

if setNeeded
  try 
      
    % Next add lines between Bus Selector and Mux
    % Only works with non-muxed Bus Selector! (muxed = 'off')    
    block   = get_param(blockH,'Parent');
    Mux     = [block,'/Mux'];
    temp    = get_param(Mux,'Ports');
    n_old   = temp(1);    
    if ~isempty(signalList)
        n_new = length(signalList);
    else
        n_new = 1;
    end
    
    % First delete all lines (order might have changed)
    for i=1:n_old
        delete_line(block,['BusSelector/' num2str(i)],['Mux/' num2str(i)]);
    end
    
    % Second, add new lines
    set_param(blockH, ...
        'OutputSignals',selSignals, ...
        'MuxedOutput', muxed);
    set_param(Mux,'Inputs',num2str(n_new));    % change Mux size
    for i=1:n_new
        add_line(block,['BusSelector/' num2str(i)],['Mux/' num2str(i)]);
    end
        
  catch
    errFlag = 1;
  end
end

set(H, 'UserData', Data);


% Function: CloseDialog ========================================================
%
function errorFlag = CloseDialog(action)

errorFlag = 0;
if strcmp(action, 'save')
  errorFlag = ApplyDialog;
end

if errorFlag==0
  H = gcbf;
  Data = get(H, 'userdata');
  
  % un-hilite hilited system
  if Data.hilitedBlk ~= -1
    set_param(Data.hilitedBlk, 'HiliteAncestors', 'off');
    Data.hilitedBlk = -1;
  end
  if strcmp(get_param(Data.BlockHandle, 'LinkStatus'), 'none')
    UserData = get(Data.BlockHandle,'UserData');
    UserData.InterfaceHandle = -1;
    set_param(Data.BlockHandle, 'UserData', UserData);
%     set_param(Data.BlockHandle, 'Figure', -1)
  end
  close(H)
end


% Function: HelpDialog =========================================================
%
function HelpDialog
SIMarchitect_open_help('Bus_Selector');

% Function: DeleteDialog =======================================================
%
function errorFlag = DeleteDialog(blockH)

H = get_param(blockH, 'Figure');
if ~isempty(H) & ishandle(H) & H ~= -1
  Data = get(H, 'userdata');
  
  % un-hilite hilited system
  if Data.hilitedBlk ~= -1
    set_param(Data.hilitedBlk, 'HiliteAncestors', 'off');
    Data.hilitedBlk = -1;
  end
  
  close(H);
  UserData = get(blockH,'UserData');
  UserData.InterfaceHandle = -1;
  set_param(Data.BlockHandle, 'UserData', UserData);
%   set_param(blockH, 'Figure', -1);
end


% Function: DeleteFcn ==========================================================
% Abstract: 
%   Get called when the dialog figure is deleted.
%
function DeleteFcn(H, evd)

Data = get(H, 'UserData');
% un-hilite_system
if Data.hilitedBlk ~= -1
  set_param(Data.hilitedBlk, 'HiliteAncestors', 'off');
end


% Function: UpdateDialogName ===================================================
%
function errorFlag = UpdateDialogName(blockH)

H = get_param(blockH, 'Figure');
if ~isempty(H) & ishandle(H) & H ~= -1
  set(H, 'name', ['Block Parameters: ' get_param(blockH, 'name')])
end


% Function: SelectSignals ======================================================
%
function SelectSignals

H = gcbf;

Data = get(H, 'UserData');
selected = get(Data.TreeList, 'value');
treeData = get(Data.TreeList, 'userdata');

[dispData idx] = FindDisplayedData(treeData);

if ~isempty(selected)
  newSignals = {dispData(selected).Fullname};
  existingSignals = get(Data.SelectedList, 'String');
  if isempty(existingSignals)
    allSignals = newSignals(:);
  else
    allSignals = [existingSignals(:); newSignals(:)];
  end
  set(Data.SelectedList, 'String', allSignals)
  set(Data.SelectedList, 'Value', [])
  set(Data.TreeList,     'Value', [])
end


% Function: RefreshSignals =====================================================
%
function RefreshSignals

H    = gcbf;
Data = get(H, 'UserData');
open_system(Data.BlockHandle);

% un-hilite hilited system
if Data.hilitedBlk ~= -1
  set_param(Data.hilitedBlk, 'HiliteAncestors', 'off');
  Data.hilitedBlk = -1;
end

set(H, 'UserData', Data);


% Function: FindSignals ========================================================
%
function FindSignals

H    = gcbf;
Data = get(H, 'UserData');

busStruct = get_param(Data.BlockHandle, 'BusStruct');

selected = get(Data.TreeList, 'Value');
signals  = get(Data.TreeList, 'String');

% un-hilite previously hilited blocks
if Data.hilitedBlk ~= -1
  set_param(Data.hilitedBlk, 'HiliteAncestors', 'off');
  Data.hilitedBlk = -1;
end

if ~isempty(selected) & ~isempty(signals) & length(selected) == 1
  signal = signals{selected};
  % If this signal is from another bus, we need to find its bus first
  % so we construct a "." deliminated string to represent its hierachy.
  % For example, abc.xyz.signal means the signal name is "signal", and it is
  % from a bus block "xyz", which is from another bus block "abc".
  numBlanks = length(signal) - length(deblank(fliplr(signal)));
  for i=selected-1:-1:1
    if numBlanks == 0
      break;
    end
    prevSignal = signals{i};
    if ~strcmp(prevSignal(numBlanks-2), ' ')
      signal    = [deblankall(prevSignal(1:end)) '.' ...
		   deblankall(signal)];
      numBlanks = numBlanks - 3;
    end
  end

  % when the signal is from another bus, we need to find that signal.
  signalSrc = findbussrc(busStruct, signal);

  % don't hilite the Bus Selector associated with this dialog
  if ~isempty(signalSrc)
    if signalSrc ~= Data.BlockHandle
      if ~isempty(signalSrc)
	% use 'find' scheme in hilite_system
	set_param(signalSrc, 'HiliteAncestors', 'find');
	Data.hilitedBlk = signalSrc;
      else
	msg = ['Unable to find signal named ' signals{selected} '.'];
	msgbox(msg, 'Bus Signal Locating Message', 'modal');
      end
    end
  end
end

set(H, 'UserData', Data);


% Function: SyncHilite =========================================================
%
function SyncHilite

H    = gcbf;
Data = get(H, 'UserData');

if Data.hilitedBlk ~= -1
  set_param(Data.hilitedBlk, 'HiliteAncestors', 'off');
  Data.hilitedBlk = -1;
end

set(H, 'UserData', Data);


% Function: RemoveSignals ======================================================
%
function RemoveSignals

H = gcbf;
Data = get(H, 'UserData');
selected = get(Data.SelectedList, 'value');
signalList = get(Data.SelectedList, 'string');
signalList(selected) = [];
set(Data.SelectedList, 'string', signalList);

% Highlight the next choice
if length(signalList) >= selected
  set(Data.SelectedList, 'value', selected)
else
  set(Data.SelectedList, 'value', length(signalList))
end

UpdateButtons


% Function: MoveUpSignals ======================================================
%
function MoveUpSignals

H = gcbf;
Data = get(H, 'UserData');
selected = get(Data.SelectedList, 'value');
signalList = get(Data.SelectedList, 'string');
signalList([selected-1 selected]) = signalList([selected selected-1]);
set(Data.SelectedList, 'string', signalList);
set(Data.SelectedList, 'value', selected-1)
UpdateButtons


% Function: MoveDownSignals ====================================================
%
function MoveDownSignals

H = gcbf;
Data = get(H, 'UserData');
selected = get(Data.SelectedList, 'value');
signalList = get(Data.SelectedList, 'string');
signalList([selected+1 selected]) = signalList([selected selected+1]);
set(Data.SelectedList, 'string', signalList);
set(Data.SelectedList, 'value', selected+1)
UpdateButtons


% Function: UpdateButtons ======================================================
%
function UpdateButtons

H = gcbf;
Data = get(H, 'UserData');
selected = get(Data.SelectedList, 'value');
signalList = get(Data.SelectedList, 'string');

if isempty(selected) | isempty(signalList)
  upState = 'off';
  downState = 'off';
  removeState = 'off';
else
  % we have selections and a signalList
  upState = 'on';
  downState = 'on';
  removeState = 'on';

  if length(selected) ~= 1
    % we can move only 1 selection up or down
    upState = 'off';
    downState = 'off';
  else
    % there is only 1 value selected
    if selected==1
      % first value selected
      upState = 'off';
    end    
    if selected==length(signalList)
      % last value selected
      downState = 'off';
    end
  end
  
end

set(Data.UpButton, 'Enable', upState)
set(Data.DownButton, 'Enable', downState)
set(Data.RemoveButton, 'Enable', removeState)
  
    
% Function: FindDisplayedData ==================================================
%
function [dd, idx] = FindDisplayedData(ud)

dd  = ud;
idx = [];
rem = [];

for i = 1:length(ud)
  if ud(i).IsDisplayed
    idx = [idx i];
  else
    rem = [rem i];
  end
end
dd(rem) = [];

% Function: DelimitString ======================================================
%
function array = DelimitString(str, sep)

array = {};
idx = find(str == sep);

if ~isempty(idx)
  idx = [0 idx length(str)+1];
  for i = 1:length(idx)-1
    % we have that many signals
    array{i} = str(idx(i)+1:idx(i+1)-1);
  end
else
  array{1} = str;
end


% Function: CheckSelection =====================================================
%
function modArray = CheckSelection(inArray, data)

modArray = inArray;

if isstruct(data)
  for i = 1:length(inArray)
    try
      eval(['data.' inArray{i} ';'])
    catch
      modArray{i} = ['??? ' inArray{i}];
    end   
  end
end

if iscell(data)

  for i = 1:length(inArray)

    delimArray = DelimitString(inArray{i}, '.');
    currData = data;
    preStr   = '';

    for k = 1:length(delimArray)
      currName = delimArray{k};
      found = 0;

      for j = 1:length(currData)

	if iscell(currData{j})
	  compareName = currData{j}{1};
	else
	  compareName = currData{j};
	end	  
	    
	if strcmp(currName, compareName)
	  found = 1;
	  if iscell(currData{j})
	    currData = currData{j}{2};
	  end
	  break;
	end
      end
	
      if (found==0)
	preStr = '??? ';
	break;
      end
    end
      
    modArray{i} = [preStr inArray{i}];      
  end

end

% Function: StripSelection =====================================================
%
function modArray = StripSelection(inArray)

modArray = strrep(inArray, '??? ', '');



%Subfunction: deblankall    =======================================================
function str = deblankall(str)
%
% remove blanks at either end of the string
%
str = char(str);
str = deblank(str);
str = fliplr(deblank(fliplr(str)));

%Subfunction: rectconv    =======================================================
function rout=rectconv(rin,style)
switch lower(style),

  case { 'handlegraphics','hg'}
    rout=InternalSimRect2HGRect(rin);

  case { 'simulink','sl' }
    rout=InternalHGRect2SimRect(rin);

  otherwise,
    error('Rectangle style is either ''simulink'' or ''handlegraphics''');

end



%******************************************************************************
% Function - Converts a SIMULINK rectangle [left, top, right, bottom]       ***
%  into a Handle Graphics rectangle [left, bottom, width, height].          ***
%******************************************************************************
function rout=InternalSimRect2HGRect(rin)

origRootUnits = get(0, 'Units');
set(0,'Units','pixel');
screen = get(0, 'ScreenSize');
set(0,'Units',origRootUnits);


% SIMULINK rects [left top    right bottom] - from the screen top
% HG rects       [left bottom width height] - from the screen bottom

rout=zeros(1,4);
rout(1) = rin(1);             % left is the left
rout(2) = screen(4)-rin(4);   % bottom is screen height - bottom
rout(3) = rin(3)-rin(1);      % width is right - left
rout(4) = rin(4)-rin(2);      % height is bottom - height


%******************************************************************************
% Function - Converts a Handle Graphics rectangle,                          ***
%  [left, bottom, width, height] into a SIMULINK rectangle,                 ***
%  [left, top, right, bottom].                                              ***
%******************************************************************************
function rout=InternalHGRect2SimRect(rin)

origRootUnits = get(0, 'Units');
set(0,'Units','pixel');
screen = get(0, 'ScreenSize');
set(0,'Units',origRootUnits);

% HG rects       [left bottom width height] - from the screen bottom
% SIMULINK rects [left top    right bottom] - from the screen top

rout=zeros(1,4);
rout(1) = rin(1);                   % left is the left
rout(2) = screen(4)-rin(2)-rin(4);  % top is screen height - bottom - height
rout(3) = rin(1)+rin(3);            % right is left + width
rout(4) = screen(4)-rin(2);         % bottom is screen height - bottom

%******************************************************************************
function src = findbussrc(busStruct, signal)
src = [];

[signal, signalStruct] = strtok(signal, '.');

% clean up the signal's name first, i.e., remove blanks and '+' or '-', etc.
minusPlusSignIdx = [findstr(signal, '-') findstr(signal, '+')];
if ~isempty(minusPlusSignIdx)
  signal = signal(minusPlusSignIdx+1:end);
end
signal = deblankall(signal);

if ~isempty(busStruct)
  for i=1:length(busStruct)
    %
    % Note: this deblankall can be avoided if we always set signal name
    % without leading or trailing blanks.
    %
    if strcmp(deblankall(busStruct(i).name), signal) 
      if isempty(signalStruct)
	src = busStruct(i).src;
	return;
      else
	subBusStruct = busStruct(i).signals;
	src = findbussrc(subBusStruct, signalStruct);
	if ~isempty(src)
	  return;
	end
      end
    end
  end
end

% end findbussrc

function [str1,str2] = create_input_string
%This function creates 2 cell arrays with strings for the description field and for the units field.
%The strings are taken from the parameterfiles of the SIMarchitect modules that are connected to the 
%busselector.


%try to find SIMarchitect module first. Assuming that it is directly connected to the busselector.
BlockHandle = get_param(gcb,'Handle');
ports=get_param(BlockHandle,'PortConnectivity');
ModuleHandle=ports(2).DstBlock;
flag=1;
if ~isempty(ModuleHandle),
    [Pfile directory] = get_param_file(ModuleHandle,[]);
else
    Pfile=-1;
    con_info.descr = 'No SIMarchitect module connected to this selector. Therefore no connection information available.';
    con_info.unit = '';        
    flag=0;    
end

%%try to run Pfile
if Pfile~=-1,    
    try 
        [con_info,flag] = local_run_file(Pfile,directory);
    catch
        con_info.descr = 'Error executing parameterfile. Therefore no connection information available.';
        con_info.unit = '';        
        flag=0;
    end
elseif flag
    %file not found
    con_info.descr = 'Parameterfile for module not found. Therefore no connection information available.';
    con_info.unit = '';
    flag=0;
end

% create the cell array with strings
if flag,
    str1=[];str2=[];
    rows = length(con_info);
    for i=1:rows,
        str1 = [str1;{['(',num2str(i),') ',con_info(i).descr]}]; 
        str2 = [str2;{con_info(i).unit}];
    end
else
    str1 = {con_info.descr}; 
    str2 = {con_info.unit};
end

function [con_info,flag]=local_run_file(Pfile,directory)
LOCAL_VARS.flag=0;
LOCAL_VARS.Pfile=Pfile;
LOCAL_VARS.directory=directory;
LOCAL_VARS.flag2=1;
if ~strcmp(LOCAL_VARS.directory,pwd),
    LOCAL_VARS.cur=pwd;
    cd(LOCAL_VARS.directory);
    LOCAL_VARS.flag=1;
end

eval(LOCAL_VARS.Pfile);
if ~exist('con_info','var')==1,
    con_info.descr = 'No information available in Parameterfile. Try the help page for this module.';
    con_info.unit = '';
    LOCAL_VARS.flag2=0;
end

if LOCAL_VARS.flag,
    cd(LOCAL_VARS.cur);
end
flag = LOCAL_VARS.flag2;
% end of function str = create_input_string
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% LocalWrapAtWidth %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [OutString1,OutString2]=LocalTextWrap(InString1,InString2,UIHandle,Indent)

OutString1={};
OutString2={};
Para1 = InString1{1};
NumNewLines=size(Para1,1);
if ~isempty(InString2), %when con_info has to be resized, the 2nd column also has to be resized.
    flag = 1;
    Para2 = InString2{1};
else
    flag = 0;
end

UIPosition=get(UIHandle,'Position');
UIWidth=UIPosition(3);
UIHeight=UIPosition(4);
TempObj=copyobj(UIHandle,get(UIHandle,'Parent'));
set(TempObj,'Visible','off','Max',100);


for LnLp=1:NumNewLines,  
    WrappedCell=LocalWrapAtWidth(Para1{LnLp,1},TempObj,UIWidth,Indent);
    if flag,        
        tmp=[];
        for i=1:size(WrappedCell,1)-1,
            tmp = [tmp;{' '}];
        end        
        OutString2=[OutString2;Para2(LnLp,1);tmp];
    end
    OutString1=[OutString1;WrappedCell];
end % for LnLp


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function WrappedCell=LocalWrapAtWidth(Para,TempObjHandle,UIWidth,Indent)

Width=size(Para,2);
Para=[Para ' '];
SpaceLocs=[find(Para==' ')];
WrappedCell={};

if ~isempty(SpaceLocs),
    EndOfPara=logical(0);  
    NeedToWrapLine=logical(0);
    LocLowIndex=0;    LocHighIndex=1;  
    LocLow=1;         LocHigh=SpaceLocs(LocHighIndex)-1;  
    while ~EndOfPara,  
        while ~NeedToWrapLine,
            TrialString=Para(LocLow:LocHigh);
            set(TempObjHandle,'String',TrialString);
            TempExtent=get(TempObjHandle,'Extent');
            NeedToWrapLine=TempExtent(3)>=UIWidth;
            if ~NeedToWrapLine,
                if LocHighIndex==length(SpaceLocs),
                    NeedToWrapLine=logical(1);
                else,
                    LocHighIndex=LocHighIndex+1;
                    LocHigh=SpaceLocs(LocHighIndex)-1;
                end % if LocHighIndex 
            else,
                % Check that the number of words is >1       
                if (LocHighIndex-LocLowIndex)>1,
                    LocHighIndex=LocHighIndex-1;
                    LocHigh=SpaceLocs(LocHighIndex)-1;       
                end % if
            end % if ~NeedToWrapLine      
        end % while ~NeedToWrapLine 
        
        WrappedCell=[WrappedCell;{Para(LocLow:LocHigh)}];
        
        if Indent,
            Para = [Para(1:LocHigh),'     ',Para(LocHigh+1:end)]; %indent all subsequent lines
        end
        
        LocLowIndex=LocHighIndex;
        LocHighIndex=LocHighIndex+1;
        
        LocLow=SpaceLocs(LocLowIndex)+1;
        if Indent,
            SpaceLocs=SpaceLocs+5; %because of indent
        end
        
        if LocHighIndex<=length(SpaceLocs),
            LocHigh=SpaceLocs(LocHighIndex)-1;
            NeedToWrapLine=logical(0);
        else
            EndOfPara=logical(1);
        end % if
    end % while ~EndOfPara  
    % There are no spaces in the text so it can't be wrapped.  
else,  
    WrappedCell={Para};
end % if ~isempty

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function locModHelp
%try to find SIMarchitect module first. Assuming that it is directly connected to the busselector.
BlockHandle = get_param(gcb,'Handle');
ports=get_param(BlockHandle,'PortConnectivity');
ModuleHandle=ports(2).DstBlock;

if ~isempty(ModuleHandle),
    try
        SIMarchitect_open_help_module( get_param(ModuleHandle,'MaskType') );
    catch
        
    end
else
    warndlg('No SIMarchitect module found that is connected to the selector.');
end



function UpdateList
% Update the listboxes
H = gcbf;
fig_ud=get(H,'Userdata');
if strcmp(get(H,'SelectionType'),'open')
    H  = gcbo;
    ud = get(H, 'Userdata');
    [dd idx] = FindDisplayedData(ud);
    
    sel = get(H,'value');
    
    if H == fig_ud.TreeList,
        switch dd(sel).IsExpanded
        case -1
            % element is not expandable
            %% For the busselector GUI. Double-Click results in selection.    
            
            SelectSignals;
            return;        
        otherwise
            treeview Update        
        end
    elseif H == fig_ud.SelectedList
        RemoveSignals;
    end
end