%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function graph_eval_print_fig
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Purpose: Creates a new figure with the same axeses as the Results figure
% Needs: 
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ud=get(gcbf,'Userdata');    %get the settings
def_line_width = 1.5;
%display wait message
waitH=wait_dlg('Analysing data, please wait...');
drawnow;


%find out if show hidden handles is on, if so set it off and turn it on again at end of function
hh_flag=strcmp(lower(get(0,'showhiddenhandles')),'on');
if hh_flag
    set(0,'ShowHiddenHandles','off');
end

axeses=[];
for i=1:ud.axis_num,
   axeses=ud.handles.axis;
end

%create new figure
f2=figure('PaperUnits','centimeters',...
    'Units','Centimeters',...
    'PaperOrientation','portrait',...
    'PaperPositionMode','manual',...
    'PaperPosition',[2.5 5 16 22],...
    'PaperType','A4',...
    'Visible','off');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:ud.axis_num,
   % Get the data and remove unwanted fields
   ax_ud=get(axeses(i),'Userdata');		% to create legend
   
   data=get(axeses(i));
   kids=get(axeses(i),'Children');
   if ~isempty(kids),
%      data=rmfield(data,'Tag');
%      data=rmfield(data,'CurrentPoint');
%      data=rmfield(data,'Children');
%      data=rmfield(data,'Title');
%      data=rmfield(data,'Type');
%      data=rmfield(data,'XLabel');
%      data=rmfield(data,'YLabel');
%      data=rmfield(data,'ZLabel');
%      data=rmfield(data,'Color');
%      data=rmfield(data,'XColor');
%      data=rmfield(data,'YColor');
      data=rmfield(data,'BeingDeleted');

      
      eval(['subplot(',num2str(ud.axis_num),'1',num2str(i),')']);
      eval(['handle',num2str(i),'=gca;']);
	  data.Position=eval(['get(handle',num2str(i),',''Position'');']);%get(gca,'Position');
      data.Parent=f2;	% Change parent
      
      % Get the children
      lines=get(kids);
      lines=rmfield(lines,'Type');
      lines=rmfield(lines,'BeingDeleted');
      lines=rmfield(lines,'Annotation');

%Origineel      
%      % Make target figure active and create axes
%      set(0,'CurrentFigure',f2)
%      eval(['set(handle',num2str(i),',data)']);										% assign axes with data
%      eval(['set(handle',num2str(i),',''Tag'',''axes',num2str(i),''');']);	% assign tag
      
      % Make target figure active and create axes
      set(0,'CurrentFigure',f2);
%      eval(['set(handle',num2str(i),',data)']);										% assign axes with data
      eval(['set(handle',num2str(i),',''Tag'',''axes',num2str(i),''');']);	% assign tag
      eval(['set(handle',num2str(i),',''Box'',''on'');']);

      % Set XGrid property to same value as in data
      if strcmpi(data.XGrid, 'on')
        eval(['set(handle',num2str(i),',''XGrid'',''on'');']);
      else
        eval(['set(handle',num2str(i),',''XGrid'',''off'');']);
      end
      
      % Set YGrid property to same value as in data
      if strcmpi(data.YGrid, 'on')
        eval(['set(handle',num2str(i),',''YGrid'',''on'');']);
      else
        eval(['set(handle',num2str(i),',''YGrid'',''off'');']);
      end
      
      for j=1:length(lines)
         eval(['lines(',num2str(j),').Parent=handle',num2str(i),';lines(',num2str(j),').LineWidth=def_line_width;']);		% Change parent for kids
      end
      
      % Create dummy data, to replace
      hold on
      for j=1:length(kids),
         plot(1,1);
      end
      kids2=[]; %initialise
      eval(['kids2=get(handle',num2str(i),',''children'');']);
      
      % Assign lines with data
      for j=1:length(lines),
         set(kids2(j),lines(j))
      end
      legend(strrep(ax_ud.y,'_','\_'));
   end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% fig = make_fig(title_, 'OuterPosition', fig_win, [1, 1, 1], title_width, 'figure', 'figure');

if hh_flag
    set(0,'ShowHiddenHandles','on');
end
set(f2,'visible','on');
set_logo_print_fig(f2, 0.25, 'SouthEast');
try
    delete(waitH);
end


