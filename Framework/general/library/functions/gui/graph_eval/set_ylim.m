%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function set_ylim(ax,lims,reset)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%
% Description: set_ylim(ax,lims,reset) setst the y-limits in the current SIMarchitect
%   plot_gui.
% 
% ax = array with the number of the axis where the limits need to be set
% lims = cell array with the limits in the following form: {[min1 max1],[min2 max2]}
% reset = flag that indicates whether or not to reset the y-lims of the 
%         other axis. If not given as a parameter to this function, 0 is assumed.
% 
% Examples:
%   set_ylim([1 3],{[5 10],[-100 20]}); -> set the limits of axis 1 and 3
%       and does not reset axis 2 and 4.
%
%   set_ylim(1);    -> resets the limits of all axis
%   set_ylim([],{},1); -> resets the limits of all axis
%  
%   set_ylim([1],{[5 10]},1); -> set the limits of axis 1 and resets all others.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if nargin==2,
    reset = 0;
elseif nargin==1,
    reset=1;
    ax=[];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gui_handle=findall(0,'Type','figure','tag','graph_eval_figure');
gui_handle=gui_handle(1); %simply take first one if there are more.
ud = get(gui_handle,'UserData');
if ~isempty(ax),
    for i=1:length(ax),
        set(ud.handles.axis(ax(i)),'Ylim',lims{i});
    end
else %reset all
    for i=1:4,
        set(ud.handles.axis(i),'YlimMode','auto');
    end
    return
end

if reset,
    for i=1:4,
        if ~sum(ax==i),
            set(ud.handles.axis(i),'YlimMode','auto');
        end
    end
end
    
