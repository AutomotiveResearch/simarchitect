%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function graph_eval(varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Purpose: main file for graphical evaluation of data
% Needs: 
%   - graph_eval_browser
%	- graph_eval_fig.m
%   - graph_eval_get_vars.m
%	- graph_eval_limits.m
%   - graph_eval_print_fig.m
%   - graph_eval_zoom_fcn.m
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin==0
    action='create';
else
    action=varargin{1};
end

switch action
case 'create'
    ud=create;
    if ~isempty(ud),
        set(ud.handles.fig,'Userdata',ud);    %set the userdata
    end
    
case 'clear_figures'
    ud = get(gcbf,'Userdata');    
    ud = clear_figures(ud);
    set(ud.handles.fig,'Userdata',ud);    %set the userdata
    
case 'clear_data'
    ud = get(gcbf,'Userdata');        
    ud = clear_data(ud);
    set(ud.handles.fig,'Userdata',ud);    %set the userdata
    
case 'individual_limits'
    ud = get(gcbf,'Userdata');
    ud = individual_limits(ud);
    status=get(ud.handles.ChkIndividual,'value');
    if status,
        for i=1:4,
            ud = graph_eval_limits(ud,'reset_lim',i);
        end
    else
        ud = graph_eval_limits(ud,'reset_lim',0);
    end
    set(ud.handles.fig,'Userdata',ud);    %set the userdata
    
case 'number_of_plots'
    ud = get(gcbf,'Userdata');
    ud = number_of_plots(ud);
    set(ud.handles.fig,'Userdata',ud);    %set the userdata
    
case 'check_multiple'
    ud = get(gcbf,'Userdata');
    ud = check_multiple(ud);   
    set(ud.handles.fig,'Userdata',ud);    %set the userdata
    
case 'plot_variable'
    ud = get(gcbf,'Userdata');
    if nargin==2,
        ud=plot_variable(ud,varargin{2});
    else
        ud=plot_variable(ud);
    end
    set(ud.handles.fig,'Userdata',ud);    %set the userdata
    
case 'ChkXvect'
    ud = get(gcbf,'Userdata');
    ud = ChkXvect(ud);
    set(ud.handles.fig,'Userdata',ud);    %set the userdata

    
    
case 'line_marker'
    ud = get(gcbf,'Userdata');
    ud = line_marker(ud);
    set(ud.handles.fig,'Userdata',ud);    %set the userdata
    
case 'auto_refresh',
    ud = get(gcbf,'Userdata');
    ud.auto_refresh =get(ud.handles.ChkAutoRefresh,'Value');
    set(ud.handles.fig,'Userdata',ud);    %set the userdata
    
case 'refresh'
    if nargin==3,
        ud=varargin{3};
    else
        ud = get(gcbf,'Userdata');
    end
    
    if nargin==2,
        if strcmp(varargin{2},'initial'),
            ud = refresh(ud,'initial');
        else
            ud = refresh(ud);
        end
    else
        ud = refresh(ud);
    end
    set(ud.handles.fig,'Userdata',ud);    %set the userdata
    
case 'set_indicator'
    ud = get(gcbf,'Userdata');
    ud = set_indicator(ud);
    set(ud.handles.fig,'Userdata',ud);    %set the userdata
    
case 'zoom_fcn'
    ud = get(gcbf,'Userdata');    
    if nargin==2,
        if strcmp(varargin{2},'reset'),
            ud = zoom_fcn(ud,'reset');
        else
            ud = zoom_fcn(ud);
        end
    else
        ud = zoom_fcn(ud);
    end
    set(ud.handles.fig,'Userdata',ud);    %set the userdata    
   
case 'change_tab'
    ud = get(gcbf,'Userdata');
    ud = change_tab(ud);
    
case 'close_fig'
    ud = get(gcbf,'Userdata');    
    ud = close_fig(ud);
    
case 'start_up'
    ud = get(gcbf,'Userdata');        
    ud = start_up(ud);
    set(ud.handles.fig,'Userdata',ud);    %set the userdata    
    
case 'legend_toggle'
    ud = get(gcbf,'Userdata');            
    ud = legend_toggle(ud);
    set(ud.handles.fig,'Userdata',ud);    %set the userdata    
    
case 'hold_toggle'
    ud = get(gcbf,'Userdata');                
    ud = hold_toggle(ud);
    set(ud.handles.fig,'Userdata',ud);    %set the userdata    
    
case 'help'
    SIMarchitect_open_help('Results_Plot_GUI');

otherwise
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%% Subfunctions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ud=create    
wait_fig=wait_dlg('Initialising data');         
drawnow; %display waiting dialog

%%%%% Call in the figure
ud=graph_eval_fig('create');      %file that creates the figure
ud.wait_fig = 1;

%%%%%defining global structure that will contain all relevant information information is stored in userdata
ud.axis_num=[];		            %number of axises currently visible
ud.x_min=[];			        %vector containing the minimum value of the x-vector for each dataset
ud.x_max=[];			        %vector containing the maximum value of the x-vector for each dataset
ud.x_lim=[0 1;0 1;0 1;0 1];	    %2 column matrix containing the minimum and maximum value of the x-axis for each axis
ud.hold_status=[0];             %the 'hold' status (1=hold on ; 0=hold of)
ud.check_multiple=[0];		    %in case of multiple datasets, plot multiple at once (=1) or not (-0)
ud.crrnt_axes=[];	            %handle of current intended axis (not always equal to handle returned by gca)
% ud.ax_color = [0.7 0.7 0.7];    %the backgroundcolor of the normal axis
% ud.active_ax_color =  [0.8 0.8 0.85];  %the backgroundcolor of the active axis
ud.ax_color = [1 1 1];    %the backgroundcolor of the normal axis
ud.active_ax_color =  [0.9 0.9 0.95];  %the backgroundcolor of the active axis
ud.ChkXvect=0;                  %checkbox for seperate x-vector selection.
ud.LineMarker=[1 1 1 1];        %indicates whether the plotted lines should be lines(1) or markers (0)
ud.auto_refresh=0;              %flag for automatic refresh after simulation of a model.
ud.ChkIndividual=0;             %flag for setting limits of x-axises individually.



%%%MANAGE callbacks, pop-up menu's, and buttondown functions

%Toolbar callbacks
set(ud.handles.toolbar.Btn_Zoom,...
    'ClickedCallback','graph_eval(''zoom_fcn'')');

set(ud.handles.toolbar.Btn_XZoom,...
    'ClickedCallback','graph_eval(''zoom_fcn'')');

set(ud.handles.toolbar.Btn_YZoom,...
    'ClickedCallback','graph_eval(''zoom_fcn'')');

set(ud.handles.toolbar.Btn_ZoomReset,...
    'ClickedCallback','graph_eval(''zoom_fcn'',''reset'')');

set(ud.handles.toolbar.Btn_HoldOn,...
    'ClickedCallback','graph_eval(''hold_toggle'')');

set(ud.handles.toolbar.Btn_Grid,...
    'ClickedCallback','grid; set(gcbo,''State'',''off'');');

set(ud.handles.toolbar.Btn_Legend,...
    'ClickedCallback','graph_eval(''legend_toggle'')');

set(ud.handles.toolbar.Btn_line_marker,...
    'ClickedCallback','graph_eval(''line_marker'')');


%%

set(ud.handles.ListboxDataset,...
    'Callback','treeview Update',...
    'Enable','off');

set(ud.handles.ChkMultiple,...
    'Callback','graph_eval(''check_multiple'')');

set(ud.handles.ChkAutoRefresh,...
    'Callback','graph_eval(''auto_refresh'')');

% callbacks of tabs are set in 'change_tabs' sub-function
set(ud.handles.ListboxY,...
    'Callback','treeview Update');

set(ud.handles.ListboxX,...
    'Callback','treeview Update');

set(ud.handles.BtnPlot,...
    'Callback','graph_eval(''plot_variable'')');

set(ud.handles.ChkXvect,...
    'Callback','graph_eval(''ChkXvect'')');

set(ud.handles.MenuNumPlots,...
    'String','1|2|3|4', ...
    'Value',4,...
    'CallBack','graph_eval(''number_of_plots'')');

set(ud.handles.BtnLegend,...
    'Callback','graph_eval(''legend_toggle'')');

set(ud.handles.BtnGrid,...
    'callback','grid');

set(ud.handles.BtnZoomReset,...
    'callback','graph_eval(''zoom_fcn'',''reset'')');

set(ud.handles.ChkHold,...
    'Callback','graph_eval(''hold_toggle'')');

set(ud.handles.ChkZoomOn,...
    'callback','graph_eval(''zoom_fcn'')');   

set(ud.handles.ChkZoomXon,...
    'callback','graph_eval(''zoom_fcn'')');   

set(ud.handles.ChkZoomYon,...
    'callback','graph_eval(''zoom_fcn'')');   


set(ud.handles.BtnLineMarker,...
    'callback','graph_eval(''line_marker'')');  


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set(ud.handles.EWindow,...
    'callback','graph_eval_limits([],''set_lim'',0);');

set(ud.handles.Slider,...
    'callback','graph_eval_limits([],''slider'');');

set(ud.handles.BtnReset,...
    'callback','graph_eval_limits([],''reset_lim'',0);');    

set(ud.handles.EXmax,...
    'callback','graph_eval_limits([],''set_lim'',0);');   
set(ud.handles.EXmin,...
    'callback','graph_eval_limits([],''set_lim'',0);');   


set(ud.handles.ChkIndividual,...
    'callback','graph_eval(''individual_limits'');');    

for i=0:3,
    eval(['set(ud.handles.BtnReset',num2str(i+1),...
            ',''Callback'',''graph_eval_limits([],''''reset_lim'''',',num2str(i+1),');'');']);
    
    eval(['set(ud.handles.EXmin',num2str(i+1),...
            ',''Callback'',''graph_eval_limits([],''''set_lim'''',',num2str(i+1),');'');']);
    
    eval(['set(ud.handles.EXmax',num2str(i+1),...
            ',''Callback'',''graph_eval_limits([],''''set_lim'''',',num2str(i+1),');'');']);
    
end


set([ud.handles.BtnRefresh,ud.handles.BtnRefresh2],...
    'callback','graph_eval(''refresh'')');

set(ud.handles.BtnPrintFig,...
    'callback','graph_eval_print_fig');

set(ud.handles.BtnClearData,...
    'callback','graph_eval(''clear_data'')');

set(ud.handles.BtnClearFig,...
    'callback','graph_eval(''clear_figures'')');

set(ud.handles.BtnExit,...
    'callback','graph_eval(''close_fig'')');

set(ud.handles.BtnHelp,...
    'callback','graph_eval(''help'')');

set(ud.handles.axis,'ButtonDownFcn','graph_eval(''set_indicator'')');



ud.crrnt_axes=get(ud.handles.fig,'CurrentAxes');	% current axis is first results axis
ud.axis_num=get(ud.handles.MenuNumPlots,'value');

%%%%% RUN SOME SUBFUNCTIONS
ud=start_up(ud);
set(ud.handles.fig,'Visible','on','HandleVisibility', 'callback');
ud = graph_eval_zoom('initialise',ud,ud.handles.fig);

try
    delete(wait_fig);  %delete waitbar may already been done by user
end
ud.wait_fig = 0;

%end function create(x_vect)    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function ud=clear_data(ud)
%make sure user wants this
y=questdlg('Continuing this command will clear all your data from the workspace. Do you wish to continue?',...
    'SIMarchitect: Question','Yes','No','Yes');
if strcmp(y,'Yes'),
    %clear the data
    evalin('base',['clear all']);
    %make changes to ud
    ud.datasets_num=[0];     			%number of datasets loaded
    ud.datasets = '';                   %names of datasets
    ud.x_vect = '';                     %names of default x-vectors
    ud.x_min=[];						%vector containing the minimum value of the x-vector for each dataset
    ud.x_max=[];						%vector containing the maximum value of the x-vector for each dataset
    ud.x_lim=[0 1;0 1;0 1;0 1];		    %2 column matrix containing the minimum and maximum value of the x-axis for each axis
    ud.hold_status=[0];     		    %the 'hold' status (1=hold on ; 0=hold of)
    ud.check_multiple=[0];				%in case of multiple datasets, plot multiple at once (=1) or not (-0)
    ud.crrnt_axes=ud.handles.axis(1);   %handle of current intended axis (not always equal to handle returned by gca)
    ud.LineMarker=[1 1 1 1];            %indicates whether the plotted lines should be markers or lines
    ud=clear_figures(ud);               %subfunction
    ud=refresh(ud);                     %sub function
    ud=set_indicator(ud);               %sub function
    disp('-------------- Data cleared from workspace --------------');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function ud=clear_figures(ud,varargin)

if isempty(varargin),
    for i=1:ud.axis_num,
        h=[]; %initialise
        h=ud.handles.axis(i);
        axes(h);
        legend(h,'off');
        delete(get(h,'Children'));         
        set(h,'Userdata','','Xgrid','on','Ygrid','on');
        zoom off
    end
    axes(ud.crrnt_axes);
else
    h=[]; %initialise
    h=ud.handles.axis(varargin{1});
    if ~isempty(h),
        axes(h);
        legend(h,'off');
        delete(get(h,'Children'));         
        set(h,'Userdata','','Xgrid','on','Ygrid','on');
        zoom off
    end
end

ud.hold_status=0;
set([ud.handles.toolbar.Btn_XZoom,...
        ud.handles.toolbar.Btn_YZoom,...
        ud.handles.toolbar.Btn_Zoom,...
        ud.handles.toolbar.Btn_HoldOn],...
        'State','off');
    
set([ud.handles.ChkHold,...
        ud.handles.ChkZoomXon,...
        ud.handles.ChkZoomYon],...
        'value',0);

ud=set_indicator(ud);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function ud=individual_limits(ud)

ud.ChkIndividual=get(ud.handles.ChkIndividual,'value');
gen_objects=[ud.handles.BtnReset;ud.handles.EXmin;ud.handles.EXmax;
            ud.handles.TWindow;ud.handles.EWindow;ud.handles.Slider];
        
ind_objects={'ud.handles.BtnReset','ud.handles.EXmin','ud.handles.EXmax'};

    x0=0.81;
    y0=0.455;   %top of section
    y_ofset=0.035;
    
if ud.ChkIndividual==1,
    set(ud.handles.ChkIndividual,'Position',[x0 y0-6*y_ofset+0.006 0.15 0.02]);	%set checkbox in correct position
    action1='''off''';
    action2='''on''';
else            
    set(ud.handles.ChkIndividual,'Position',[x0 y0-4*y_ofset+0.006 0.15 0.02]);	%set checkbox in correct position
    action1='''on''';
    action2='''off''';
end

for ii=1:size(gen_objects,1),	%the general objects
    eval(['set(gen_objects(',num2str(ii),'),''enable'',',action1,');']);
    eval(['set(gen_objects(',num2str(ii),'),''visible'',',action1,');']);
end
for i=1:ud.axis_num,	%the individual objects
    for ii=1:size(ind_objects,2),
        eval(['set(',ind_objects{ii},num2str(i),',''enable'',',action2,');']);
        eval(['set(',ind_objects{ii},num2str(i),',''visible'',',action2,');']);
    end
end
for i=4:-1:ud.axis_num+1,	%the individual objects of not visible axis
    for ii=1:size(ind_objects,2),
        eval(['set(',ind_objects{ii},num2str(i),',''enable'',''off'');']);
        eval(['set(',ind_objects{ii},num2str(i),',''visible'',''off'');']);
    end
end    


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function ud=number_of_plots(ud)

ud.axis_num=get(ud.handles.MenuNumPlots,'value');
for i=1:4,
    axes(ud.handles.axis(i));legend off;
end

x_pos=[0.055 0.725]; %[0.035 0.745];
y0=0.04; y1=0.96;
range=y1-y0;
ofset=.04;

pos1=[y0 range];
pos2=[range/2+ofset*1/2+y0 range/2-ofset/2;y0 range/2-ofset/2];
pos3=[range/3*2+ofset*2/3+y0 range/3-ofset*2/3; range/3+ofset*1/3+y0 range/3-ofset*2/3;    y0 range/3-ofset*2/3];
pos4=[range*3/4+ofset*3/4+y0 range/4-ofset*3/4; range/4*2+ofset*2/4+y0 range/4-ofset*3/4; range/4+ofset*1/4+y0 range/4-ofset*3/4; y0 range/4-ofset*3/4];

for i=(ud.axis_num+1):4,
    eval(['set(ud.handles.axis(',num2str(i),'),''visible'',''off'',''Userdata'','''')']);
    eval(['axes(ud.handles.axis(',num2str(i),'))']);
    ylabel('');
    cla;
end
for i=ud.axis_num:-1:1,
    eval(['set(ud.handles.axis(',num2str(i),'),''visible'',''on'')']);
    eval(['set(ud.handles.axis(',num2str(i),'),''position'',[x_pos(1) pos',...
            num2str(ud.axis_num),'(',num2str(i),',1) x_pos(2) pos',...
            num2str(ud.axis_num),'(',num2str(i),',2)])']);
    eval(['axes(ud.handles.axis(',num2str(i),'))']);
    ax_ud=get(gca,'userdata');
    if ~isempty(ax_ud),
        num_plots = length(ax_ud.y);
        legend_txt = strrep(ax_ud.y(1:num_plots),'_','\_');
        legend(legend_txt);
    end
end
ud.crrnt_axes=gca;


ud=set_indicator(ud);
ud=individual_limits(ud);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        

function ud=plot_variable(ud,varargin)

var2plot.x={}; var2plot.y={}; var_base=[]; var_rest=[];
try
    %% Find out what y-vectors to plot
    if ~isempty(varargin),
        %when called from a restore action
        var2plot.y=varargin{1};
        var2plot.x=varargin{2};
    else
        %%normal case
        sel = graph_eval_browser('select',ud.handles.ListboxY); % the selection in the listbox (cell array)
        %if plot multiple is on try to plot other variables as well with identical names but in different base-structure
        if ud.check_multiple,
            sel_xvectors = ud.x_vect(get(ud.handles.ListboxDataset,'Value'));
            for i=1:size(sel,2),                
                %only perform check if variable to plot is part of a structure
                isafield=~isempty(findstr(sel{i},'.'));
                if isafield,
                    var2plot.y{end+1} = sel{i};
                    [var_base var_rest] = strtok(sel{i},'.');
                    for j = 1:length(sel_xvectors),  %for all datasets
                        x_vect_base = strtok(sel_xvectors{j},'.');
                        try
                            if sum(strcmp([x_vect_base,var_rest],var2plot.y))==0
                                a=evalin('base',['(',[x_vect_base,var_rest],'(1)==',[x_vect_base,var_rest],'(1));']);  %fastest way to check whether this variable exists                                                            
                                var2plot.y{end+1} = [x_vect_base,var_rest];
                            end
                        end                        
                    end
                else %this selected variable is not part of a structure
                    var2plot.y{end+1,1} = sel{i};
                end %if isafield,
            end %for i=1:size(var2plot,2)
        else %plot multiple is not on
            var2plot.y = sel;
        end %if ud.check_multiple
        
        
        
        %% Find matching x-vectors for the y-vectors %%
        
        % get the candidates
        
        if ud.ChkXvect
            %checkbox for selecting non-default x-vectors is on
            pos_x_vectors=graph_eval_browser('select',ud.handles.ListboxX); % the selection in the listbox (cell array);
            if ud.check_multiple,
                %plot multiple is on, so try to find the same x-vectors in the other datasets.
                sel=pos_x_vectors; pos_x_vectors=[];
                %only perform check if variable to plot is part of a structure
                isafield=~isempty(findstr(sel{i},'.'));
                if isafield,
                    %pos_x_vectors{end+1} = sel{i};
                    [var_base var_rest] = strtok(sel{i},'.');
                    for j = 1:ud.datasets_num,  %for all datasets
                        x_vect_base = strtok(ud.x_vect{j},'.'); %the base of the default x-vector
                        try
                            if sum(strcmp(pos_x_vectors,[x_vect_base,var_rest]))==0,
                                a=evalin('base',['(',[x_vect_base,var_rest],'(1)==',[x_vect_base,var_rest],'(1));']);  %fastest way to check whether this variable exists                                
                                pos_x_vectors{end+1} = [x_vect_base,var_rest];
                            end
                        end                        
                    end
                else %this selected variable is not part of a structure
                    pos_x_vectors{end+1,1} = sel{i};
                end %if isafield,        
            end        
        else
            pos_x_vectors=ud.x_vect; %the default x-vectors
        end
        
        % make a selection from the candidates
        for i=1:length(var2plot.y)
            [var_base var_rest] = strtok(var2plot.y{i},'.');
            %find a matching x-vector
            flag = 0;
            dim_var2plot=evalin('base',['size(',var2plot.y{i},',1);']);
            valid_x_vect1=[]; valid_x_vect2=[];
            for ii=1:length(pos_x_vectors),  %get the valid and obvious x-vector(s)                        
                x_vect=pos_x_vectors{ii};              
                try
                    dim_x_vect=evalin('base',['size(',pos_x_vectors{ii},',1);']);
                catch
                    dim_x_vect=-1; %in case it doesn't exist anymore
                end
                if dim_x_vect==dim_var2plot,
                    if strcmp(var_base,strtok(x_vect,'.')) | isempty(strfind(x_vect,'.'))
                        flag = 1;
                        valid_x_vect1 = [valid_x_vect1,ii] ; %found a matching x-vector that is part of the same structure or not part of a struct
                    elseif flag~=1
                        flag = [2]; %found a matching x-vector that is part of a diff structure continue looking for one that is part of same struct
                        valid_x_vect2 = [valid_x_vect2,ii] ;
                    end
                end
            end
            
            %decide which one to chose
            if flag == 1,
                valid_x_vect = valid_x_vect1;
            elseif flag ==2,
                valid_x_vect = valid_x_vect2;
            elseif flag ==0
                h=warndlg(sprintf('No matching x-vector found for:\n%s\n\n Or selected x-vector does not match.',var2plot.y{i}),...
                    'SIMarchitect: Warning');
                waitfor(h);
                return                
            end       
            
            if length(valid_x_vect)==1,
                var2plot.x{end+1} = pos_x_vectors{valid_x_vect};         
            else    %more than one valid x-vector found within same base structure, ask user which one to use, after checking that tey're not identical
                ch=0; %initialise variable choice
                for ii = 2:length(valid_x_vect),
                    if ud.same_x_vect(valid_x_vect(1),valid_x_vect(ii))~=1,
                        %at least one of the valid x-vectors differs from another one, so let user make the choice
                        pos_x_vectors1 = pos_x_vectors(valid_x_vect);
                        ch = graph_eval_menu(sprintf(['Which x-vector do you want to use to plot: \n%s'],var2plot.y{i}),pos_x_vectors1);
                        break
                    else
                        ch = valid_x_vect(1);
                    end                
                end
                if ch ~=0,
                    var2plot.x{end+1} = pos_x_vectors1{ch};                
                else
                    return %user closed menu without making selection
                end
            end
        end
    end %if nargin==1,
catch
    h=warndlg('Could not plot requested variable(s).');
    waitfor(h);
    var2plot.x={};
    var2plot.y={};
end

%% some administration %%%
%% register some stuff 
axes(ud.crrnt_axes);	%could not be done before, because it makes the legend invisible
tag=get(ud.crrnt_axes,'tag');      %in order to set same tag for new axes that will be made
lim_flag=strcmp(get(ud.crrnt_axes,'XlimMode'),'manual'); %if axislimits have been set manually, or by zooming, resore them again after plotting.
try
    plot_axis=str2num(tag(end));	        %the number of the plot
catch
    %can happen in some unexpected cases
    errordlg('An error occured in the time-plots GUI; please close it and start again','SIMarchitect: Error')
    return
end
if ud.hold_status,
    colornum=length(findobj(ud.crrnt_axes,'Type','Line')); 	%number of lines already plotted in this axis
else
    colornum=0;
end

%needed to style the lines
colorselection=['b','r','g','m','y','k','c'];
if ud.LineMarker(plot_axis),
    linestyle=char('-','-.','-*','-+','-o'); %plot lines
else
    linestyle=char('*','o','x','.','+'); %plot markers
end

num_plots=length(var2plot.x);
if num_plots>35,
    uiwait(msgbox('Only plotting first 35 variables.','SIMarchitect: warning','modal'));
    num_plots=35;
end


%%Plotting
colornum=colornum+1;
err_flag=zeros(1,num_plots);
for i=1:num_plots,
    color=[colorselection(colornum-(floor((colornum-1)/7))*7),linestyle(floor(((colornum-1)/7)+1),:)];	%color and linestyle
    try
        evalin('base',['plot(',var2plot.x{i},',',var2plot.y{i},',''',color,''')']);			    % actual plotting
        colornum=colornum+1;
    catch
        err_flag(i) = 1;
    end   
    if i==1, evalin('base',['hold on']); end;    
end

if ~ud.hold_status, evalin('base',['hold off']); end; %disable hold on if necessary



%% make sure same x limits are placed again
% don't do this when plotting with a non default x-vector
if lim_flag,
    set(gca,'XLim',ud.x_lim(plot_axis,:));
else
    ud=graph_eval_limits(ud,'reset_lim',plot_axis);
end

%%register the plotted variables in the userdata of the axes
ax_ud=get(get(ud.handles.fig,'CurrentAxes'),'userdata');
indx = find(err_flag==0);  %only add the succeeded plots  to the userdata
if isempty(ax_ud),
    num_plots_tot = length(indx);                  %total number of plots in this axis (needed for legend)
    ax_ud.y = var2plot.y(indx);
    ax_ud.x = var2plot.x(indx);    
else
    num_plots_tot = length(ax_ud.y)+length(indx);  %total number of plots in this axis
    ax_ud.y = {ax_ud.y{:},var2plot.y{indx}};
    ax_ud.x = {ax_ud.x{:},var2plot.x{indx}};
end

%Set the axes tag and ButtonDownFcn. And set correct handle in userdata
ud.crrnt_axes=gca;

set(ud.crrnt_axes,'userdata',ax_ud,...
    'Color',ud.ax_color,...
    'ButtonDownFcn','graph_eval(''set_indicator'')',...
    'tag',tag,...
    'Ycolor',[0 0 0],...
    'XColor',[0 0 0]);



%%set legend and grid color and axis labels

legend_txt = strrep(ax_ud.y(1:num_plots_tot),'_','\_');
legend off;
if ~isempty(legend_txt),
    if length(legend_txt{1})==1, %legend_txt has to be at least two characters for legend to work.
        legend_txt=[legend_txt{1},' '];
    end
    legend(legend_txt);
end
grid on;

%%Set indicator
ud=set_indicator(ud);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        

function ud=refresh(ud,varargin)
%set the data in the plot listbox        
init_flag=0;
if ~isempty(varargin),
    if strcmp(varargin{1},'initial'),        
        init_flag=1;
    end
end

if ~ud.wait_fig
    wait_fig=wait_dlg('Initialising data');         drawnow; %display waiting dialog
    ud.wait_fig_refresh=1;
else
    ud.wait_fig_refresh=0;
end

ud = get_datasets(ud);
[data,ud]=graph_eval_get_vars(ud);

if ~init_flag,   %record current visual status of listbox
    ud_old.DisplayedY=graph_eval_browser('IsDisplayed',ud.handles.ListboxY);            
    ud_old.DisplayedX=graph_eval_browser('IsDisplayed',ud.handles.ListboxX);            
end

set(ud.handles.ListboxY,'String','','Value',1);
set(ud.handles.ListboxX,'String','','Value',1);
graph_eval_browser('Create',ud.handles.ListboxY,data);
graph_eval_browser('Create',ud.handles.ListboxX,data);

if ud.datasets_num>1,
    set(ud.handles.ChkMultipleExtra,'Enable','on');    
    set(ud.handles.ChkMultiple,'Enable','on');    
    ud.check_multiple = get(ud.handles.ChkMultiple,'Value');
    if ud.check_multiple
        set(ud.handles.ListboxDataset,'Enable','on','Value',1);
    else
        set(ud.handles.ChkMultipleExtra,'Enable','on');    
        set(ud.handles.ListboxDataset,'Enable','off','Value',[]);
    end    
else
    set([ud.handles.ChkMultiple,ud.handles.ListboxDataset,ud.handles.ChkMultipleExtra],'Enable','off','Value',1);   
    ud.check_multiple=[0];
end

%try to plot same data again, data may be updated
if ~init_flag,
    echo off
    warning off    
    for i=1:ud.axis_num,
        try
            plots=[];  %initialise
            ax_ud=get(ud.handles.axis(i),'userdata');
            ud=clear_figures(ud,i);   %clear the current axes, so that if data doesn't exist anymore it also doesn't show

            if ~isempty(ax_ud),
                ud.crrnt_axes=ud.handles.axis(i);
                ud=plot_variable(ud,ax_ud.y,ax_ud.x);
                ud=graph_eval_limits(ud,'reset_lim',i);
            end
        end
    end
    warning on 
    echo on

    %try to expand same structures in listbox
    graph_eval_browser('recreate',ud.handles.ListboxY,ud_old.DisplayedY);
    graph_eval_browser('recreate',ud.handles.ListboxX,ud_old.DisplayedX);

    set(ud.handles.fig,'CurrentAxes',ud.handles.axis(1));
    ud=set_indicator(ud);
end
    
try
    if ud.wait_fig_refresh==1;
        delete(wait_fig);  %delete waitbar may already been done by user
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ud=hold_toggle(ud)

if gcbo==ud.handles.toolbar.Btn_HoldOn, %toolbar clicked
    state=get(ud.handles.toolbar.Btn_HoldOn,'State');
    if strcmp(state,'on'), value = 1; else  value = 0;  end
    tag=get(ud.crrnt_axes,'Tag');
    set(ud.handles.ChkHold,'Value',value);
else %checkbox clicked
    value=get(ud.handles.ChkHold,'Value');
    tag=get(ud.crrnt_axes,'Tag');
    if value, state = 'on'; else  state = 'off';  end
    set(ud.handles.toolbar.Btn_HoldOn,'State',state);
end

if value
   set(ud.handles.axis,'Nextplot','add');
   ud.hold_status=1;   
else
   set(ud.handles.axis,'Nextplot','replace');
   ud.hold_status=0;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        

function ud=set_indicator(ud)
try
    ud.crrnt_axes=get(ud.handles.fig,'CurrentAxes');

    %back-up case
    if isempty(ud.crrnt_axes),
        ud.crrnt_axes=findobj('tag','results_axes1');
        disp('indicator back-up used')
    end
    
    for i=1:ud.axis_num,
        eval(['set(findobj(''tag'',''results_axes',num2str(i),'''),''Color'',ud.ax_color);']);
    end       
    set(ud.crrnt_axes,'color',ud.active_ax_color);
    
    % Line/Marker issue
    tag = get(ud.crrnt_axes,'Tag');
    axisNo = str2num(tag(end));
    if get(ud.handles.RadioLine,'Value')~=ud.LineMarker(axisNo),
        set(ud.handles.RadioLine,'Value',ud.LineMarker(axisNo));
        set(ud.handles.RadioMarker,'Value',~ud.LineMarker(axisNo));
    end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        

function ud=zoom_fcn(ud,varargin)
try
    if isempty(varargin),
        callback_object = gcbo;
        if strcmp(get(callback_object,'Type'),'uitoggletool'),
            state = strcmp(get(callback_object,'State'),'on');
        else
            state = get(callback_object,'Value');
        end
        
%        if callback_object == ud.handles.toolbar.Btn_XZoom |callback_object == ud.handles.ChkZoomXon,
        if callback_object == ud.handles.toolbar.Btn_XZoom ||callback_object == ud.handles.ChkZoomXon,
            if state
                set(ud.handles.ChkZoomXon,'value',1);
                set(ud.handles.ChkZoomYon,'value',0);
                set(ud.handles.ChkZoomOn,'value',0);            
                
                set(ud.handles.toolbar.Btn_XZoom,'State','on');
                set(ud.handles.toolbar.Btn_YZoom,'State','off');
                set(ud.handles.toolbar.Btn_Zoom,'State','off');            

                set(ud.handles.Slider,'Visible','on');
                graph_eval_zoom('off',ud.handles.fig);
                graph_eval_zoom('xonly',ud.handles.fig);
            else
                set(ud.handles.ChkZoomXon,'value',0);
                set(ud.handles.toolbar.Btn_XZoom,'State','off');
                graph_eval_zoom('off',ud.handles.fig);
            end
                
        elseif callback_object == ud.handles.toolbar.Btn_YZoom |callback_object == ud.handles.ChkZoomYon,
            if state            
                set(ud.handles.ChkZoomXon,'value',0);
                set(ud.handles.ChkZoomYon,'value',1);
                set(ud.handles.ChkZoomOn,'value',0);            
                
                set(ud.handles.toolbar.Btn_XZoom,'State','off');
                set(ud.handles.toolbar.Btn_YZoom,'State','on');
                set(ud.handles.toolbar.Btn_Zoom,'State','off');            

                set(ud.handles.Slider,'Visible','on');
                graph_eval_zoom('off',ud.handles.fig); 
                graph_eval_zoom('yonly',ud.handles.fig);
            else
                set(ud.handles.ChkZoomYon,'value',0);
                set(ud.handles.toolbar.Btn_YZoom,'State','off');
                graph_eval_zoom('off',ud.handles.fig);
            end
            
        elseif callback_object == ud.handles.toolbar.Btn_Zoom |callback_object == ud.handles.ChkZoomOn,            
            if state                        
                set(ud.handles.ChkZoomXon,'value',0);
                set(ud.handles.ChkZoomYon,'value',0);
                set(ud.handles.ChkZoomOn,'value',1);            
                
                set(ud.handles.toolbar.Btn_XZoom,'State','off');
                set(ud.handles.toolbar.Btn_YZoom,'State','off');
                set(ud.handles.toolbar.Btn_Zoom,'State','on');            
                
                set(ud.handles.Slider,'Visible','off');
                graph_eval_zoom('on',ud.handles.fig);
            else
                set(ud.handles.ChkZoomOn,'value',0);
                set(ud.handles.toolbar.Btn_Zoom,'State','off');
                set(ud.handles.Slider,'Visible','on');
                graph_eval_zoom('off',ud.handles.fig);
            end
            
        end
        
    elseif varargin{1}=='reset',
        
        if ~ud.ChkIndividual,
            flag=1;
            ud.ChkIndividual=1;
            set(ud.handles.Slider,'Visible','on');
        else
            flag=0;
        end
        
        for i=1:ud.axis_num,
            set(ud.handles.axis(i),'YlimMode','auto');
            ud=graph_eval_limits(ud,'reset_lim',i);
        end
        if flag,
            ud.ChkIndividual=0;
        end
            
        set(ud.handles.ChkZoomXon,'value',0);
        set(ud.handles.toolbar.Btn_XZoom,'State','off');
        set(ud.handles.ChkZoomYon,'value',0);
        set(ud.handles.toolbar.Btn_YZoom,'State','off');        
        set(ud.handles.ChkZoomOn,'value',0);
        set(ud.handles.toolbar.Btn_Zoom,'State','off');        
                
        axes(ud.crrnt_axes);
        graph_eval_zoom('reset',ud.handles.fig);
        graph_eval_zoom('off',ud.handles.fig);
        
        set(ud.handles.toolbar.Btn_ZoomReset,'State','off'); %make sure it is always off.

%         %% Make legends visible again
%         legends=findobj(ud.handles.fig,'Tag','legend');        %get the handle of all legends
%         % find the legend belonging to the current axes (if any), by looking at its 'userdata.PlotHandle'
%         leg_handle=[];
%         for i=1:length(legends),
%             ud_legend=get(legends(i),'Userdata');
%             if ud_legend.PlotHandle==ud.crrnt_axes,
%                 %leg_handle=legends(i);
%                 axes(legends(i));
%                 break
%             end            
%         end        
        
    end
catch
    h=warndlg('Error occured');
    waitfor(h);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        

function ud=change_tab(ud)
%% toggle the tab's in the GUI
try
    handle=gcbo;
    if isempty(handle), %in startup case
        handle=ud.handles.BtnTabPlotSection;
    end
    
    if strcmp(get(handle,'Tag'),'tab_plot_section'),
        %disable and enable tabs and set selector (obscures line under button)
        set(handle,...
            'Position',[ 0.8 0.948 0.082 0.038 ],...              
            'ButtonDownFcn','');
        set(ud.handles.BtnTabControlSection,...
            'Position',[ 0.884 0.948 0.08 0.035 ],...
            'ButtonDownFcn','graph_eval(''change_tab'')');
        
        set(handle,'Units','Pixels');
        tab_pos=get(handle,'Position');
        set(ud.handles.Selector,'Units','Pixels','Position',[tab_pos(1)+1,tab_pos(2)-3,tab_pos(3)-3,6]);
        set(handle,'Units','Normalized');
        
        %% activate the plot_section
        set(ud.handles.plot_section,'Visible','on');
        
        %% deactivate the plot_control section
        set(ud.handles.control_section,'Visible','off');
        
        %% set the listboxes visibility as it was.
        ud=ChkXvect(ud);
        
    else
        %disable and enable tabs and set selector (obscures line under button)
        set(handle,...
            'Position',[ 0.882 0.948 0.082 0.038 ],...              
            'ButtonDownFcn','');
        set(ud.handles.BtnTabPlotSection,...
            'Position',[ 0.802 0.948 0.08 0.035 ],...
            'ButtonDownFcn','graph_eval(''change_tab'')');

        set(handle,'Units','Pixels');
        tab_pos=get(handle,'Position');
        set(ud.handles.Selector,'Units','Pixels','Position',[tab_pos(1)+1,tab_pos(2)-3,tab_pos(3)-3,6]);
        set(handle,'Units','Normalized');

        
        %% deactivate the plot_section
        set(ud.handles.plot_section,'Visible','off');        
        %% activate the plot_control section
        set(ud.handles.control_section,'Visible','on');
        
        ud=individual_limits(ud);    %set individual limits or not (depending on previous settings)
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                

function ud=close_fig(ud)

%% register some stuff for next start up
%what's plotted
echo off
try
    for i=1:ud.axis_num,        
        h=[];
        ax_ud = get(ud.handles.axis(i),'userdata');
        if ~isempty(ax_ud),
            eval(['ud_old.axis',num2str(i),'Y=cellstr(ax_ud.y);']);
            eval(['ud_old.axis',num2str(i),'X=cellstr(ax_ud.x);']);            
        else
            eval(['ud_old.axis',num2str(i),'Y=[];']);
            eval(['ud_old.axis',num2str(i),'X=[];']);            
        end
    end 
    %what's visible in plot listbox
    ud_old.DisplayedY=graph_eval_browser('IsDisplayed',ud.handles.ListboxY);
    ud_old.DisplayedX=graph_eval_browser('IsDisplayed',ud.handles.ListboxX);    
    ud_old.axis_num=ud.axis_num;  %axis number
    
    %current figure size and position
    ud_old.Position = get(ud.handles.fig,'Position');
    
    %save the userdata
    eval(['save ''',SIMarchitect_root,'\temp\ge_userdata'' ud_old']);
catch
    errdlg=('lasterr');
end
echo on
close(ud.handles.fig);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                

function ud=start_up(ud)
try
    
    %load the old userdata
    try
        eval(['load ''',SIMarchitect_root,'\temp\ge_userdata''']);
    catch
        ud_old=[];
        ud_old.axis1=[];
        ud_old.axis2=[];
        ud_old.axis3=[];
        ud_old.axis4=[];
        ud_old.DisplayedY=[];
        ud_old.DisplayedX=[];        
        ud_old.axis_num=[];
        ud_old.x_vect=[];
    end
    
    ud=refresh(ud,'initial');
    
    %% try to make listbox look the same
    graph_eval_browser('recreate',ud.handles.ListboxY,ud_old.DisplayedY);
    graph_eval_browser('recreate',ud.handles.ListboxX,ud_old.DisplayedX);    
    
    %% try to plot the same variables
    for i=1:ud_old.axis_num,
        if eval(['iscell(ud_old.axis',num2str(i),'Y)']),
            if eval(['~isempty(ud_old.axis',num2str(i),'Y{1})']),
                Xvars=eval(['ud_old.axis',num2str(i),'X;']);
                Yvars=eval(['ud_old.axis',num2str(i),'Y;']);                
                if ~isempty(Yvars),
                    ud.crrnt_axes=ud.handles.axis(i);
                    try
                        ud=plot_variable(ud,Yvars,Xvars);
                    catch
                        ud=clear_figures(ud,i);
                    end
                end
            end
        else
            ud=clear_figures(ud,i);
        end
    end
    set(ud.handles. fig,'CurrentAxes',ud.handles.axis(1));

    %try to restore previous figure size and position
    set(ud.handles.fig,'Position',ud_old.Position);

catch
    ud=clear_figures(ud); %try to at least clear all figures if something didn't work out
    set(ud.handles. fig,'CurrentAxes',ud.handles.axis(1));
end
%run some subfunctions
ud=individual_limits(ud); %set it to general limits-setting
ud=set_indicator(ud);
ud=change_tab(ud);
ud=graph_eval_limits(ud,'reset_lim',0);     

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                

function ud=legend_toggle(ud)
%toggles the visibility state of the current axis
try
    
    legend('toggle');
    
%     % set the button state to 'off' again (even if button on 'control tab' was pressed, just to make sure it is always 'off')
%     set(ud.handles.toolbar.Btn_Legend,'State','off');
%     
%     % make the registered current axes also the real current axes (might be different due to legend stuff)
%     axes(ud.crrnt_axes);
%    
%     %get the handle of all legends
%     legends=findobj(ud.handles.fig,'Tag','legend');
%     
%     % find the legend belonging to the current axes (if any), by looking at its 'userdata.PlotHandle'
%     leg_handle=[];
%     for i=1:length(legends),
%         ud_legend=get(legends(i),'Userdata');
%         if ud_legend.PlotHandle==ud.crrnt_axes,
%             leg_handle=legends(i);
%             break
%         end
%     end
%     
%     % if we haven't found one, then the current plot has no legend and we have to create it, otherwise we're going to delete it
%     if isempty(leg_handle),
%         ax_ud = get(ud.crrnt_axes,'userdata');
%         leg = legend(strrep(ax_ud.y,'_','\_'));
%         set(leg,'Color',ud.ax_color);
%     else
%         legend off
%     end    

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

function ud=ChkXvect(ud)

ud.ChkXvect = get(ud.handles.ChkXvect,'Value');
if ud.ChkXvect,
    %decrease size of Yvector listbox and show Xvector listbox
    set(ud.handles.ListboxY,'Position',[0.82 0.45 0.161 0.483]);
    set(ud.handles.ListboxX,'Visible','on','Position',[0.82 0.18 0.161 0.26]);
    set([ud.handles.Xtext,ud.handles.Ytext],'Visible','on');
    
    if ~ud.ChkIndividual, %make sure individual limits setting is turned on
        set(ud.handles.ChkIndividual,'Value',1);
    end
        

else
    %increase size of Yvector listbox and hide Xvector listbox
    set(ud.handles.ListboxY,'Position',[0.82 0.18 0.161 0.753]);
    set([ud.handles.ListboxX,ud.handles.Xtext,ud.handles.Ytext],'Visible','off');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

function ud=line_marker(ud)

%% make sure the toolbar button is switched to its off-state again
set(ud.handles.toolbar.Btn_line_marker,'State','off');

%% get number of current plot
tag       = get(ud.crrnt_axes,'tag');      %in order to set same tag for new axes that will be made
plot_axis = str2num(tag(end));	           %the number of the plot        
flag      = ud.LineMarker(plot_axis);

ud.LineMarker(plot_axis) = ~flag; %toggle the state

%needed to style the lines
if ud.LineMarker(plot_axis),
    linestyles=char('-','-.','-*','-+'); %plot lines
    markers = 'none';
else    
    linestyles = 'none';
    markers =char('*','o','x','.','+'); %plot markers
end

%get the lines in the plot
kids=get(ud.handles.axis(plot_axis),'Children');
lines=get(kids);    

%%Change the line styles
for i=1:length(lines),    
    if ud.LineMarker(plot_axis),
        linestyle = linestyles(floor((i/8)+1),:);
        marker = markers;
    else
        linestyle = linestyles;
        marker = markers(floor((i/8)+1),:);
    end
    
    set(kids(i),'Linestyle',linestyle,'Marker',marker);
    if i==1, evalin('base',['hold on']); end;    
end

if ~ud.hold_status, evalin('base',['hold off']); end; %disable hold on if necessary

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

function ud=get_datasets(ud)

all_vars=evalin('base','whos');        %this is a structure with all variables in the workspace
datasets={};

for i=1:size(all_vars,1)                    
    if strcmp(all_vars(i).class,'struct'),
        fields=evalin('base',['fieldnames(',all_vars(i).name,')']);	%cell array with field names
        if sum(strcmp(fields,'output'))>=1,
            if evalin('base',['isstruct(',[all_vars(i).name,'.output'],');']),
                fields=evalin('base',['fieldnames(',[all_vars(i).name,'.output'],')']);
                if sum(strcmp(fields,'t'))>=1 & length(fields)>1,  %check if field 't' exists and if more fields exist
                    if  evalin('base',['isnumeric(',[all_vars(i).name,'.output.t'],')']), % make sure it's numeric
                        datasets{end+1,1} = all_vars(i).name;
                    end
                end
            end
        end
    end                  
end

dim = size(datasets);
x_vect = cell(size(dim));
for i=1:max(dim),
    x_vect{i} = [datasets{i},'.output.t'];
end
ud.x_vect = x_vect;
ud.datasets= datasets;
ud.datasets_num = length(datasets);

treeview('Create',ud.handles.ListboxDataset,datasets);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

function ud=check_multiple(ud)

ud.check_multiple = get(ud.handles.ChkMultiple,'Value');
if ud.check_multiple
    set(ud.handles.ListboxDataset,'Enable','on','Value',1);
else
    set(ud.handles.ListboxDataset,'Enable','off','Value',[]);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

%EOF
