%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function [data,ud]=graph_eval_get_vars(ud)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Purpose:  Returns a all plotable variables in the workspace in a structure called data.
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data=[];
all_vars=evalin('base','whos');        %this is a structure with all variables in the workspace

x_vect=ud.x_vect;
try
    ud.datasets=0;             %number of datasets loaded
    ud.x_vect=[];
    ud.x_min=[];
    ud.x_max=[];
    ud.length=[];
    ii=1;
    for i=1:max(size(x_vect)),         %get the x-limits of all x-vectors
        try
            x_min=evalin('base',['min(',x_vect{i},'(1))']);
            x_max=evalin('base',['max(',x_vect{i},'(end))']);
            ud.x_vect{ii}=x_vect{i};
            ud.x_min(ii)=x_min;
            ud.x_max(ii)=x_max;
            ud.length(ii)=evalin('base',['length(',x_vect{i},');']);
            ud.datasets=ud.datasets+1;
            ii=ii+1;
        end
    end
    
    for i=1:ud.datasets, %check for identical vectors
        for ii=1:ud.datasets,
            if i ~= ii, %looking at different vectors
                if ud.length(i) == ud.length(ii) &...
                        ud.x_min(i) == ud.x_min(ii) &...
                        ud.x_max(i) == ud.x_max(ii), %same length, same start and end so potentially identical
                    ud.same_x_vect(i,ii) = evalin('base',[ 'sum(',ud.x_vect{i},'-',ud.x_vect{ii},')<1e-10;']); %== is not an option due to machine accuracy
                else
                    ud.same_x_vect(i,ii) = 0;
                end
            else
                ud.same_x_vect(i,ii) = 1;
            end            
        end        
    end
    set(ud.handles.fig,'Userdata',ud);    %set the data
    
    if ud.datasets>0,
        for i=1:size(all_vars,1)                    
            if strcmp(all_vars(i).class,'struct'),
                [data]=get_vars_in_struct(all_vars(i).name,data,ud);
            elseif ((sum(max(all_vars(i).size)==ud.length)>=1) & strcmp(all_vars(i).class,'double')),   
                eval(['data.',all_vars(i).name,'=[];']);
            end                  
        end
    end;            
end

if isempty(data),
    data.none=[]; %to make sure 'none' is displayed
end



%% SUBFUNCTIONS      
function [data]=get_vars_in_struct(name,data,ud)


fields=evalin('base',['fieldnames(',name,')']);	%cell array with field names
for ii=1:length(fields),
    field=[name,'.',fields{ii}];
    
    try
        class_field=evalin('base',['class(',field,');']);
        if strcmp(class_field,'struct'),
            [data]=get_vars_in_struct(field,data,ud);
        else
            temp=evalin('base',['size(',field,');']);            
            if  ((sum(max(temp)==ud.length)>=1) & strcmp(class_field,'double')),
                eval(['data.',field,'=[];']);
            end                       
        end      
    end
end
