%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function output = graph_eval_browser(varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GRAPH_EVAL_BROWSER(ACTION,list HANDLE, DATA);
% Action:
%   'select'
%   'refresh'
%
% FIGURE must be a hanlde to a valid figure
% DATA must be a structure
%
% SIMarchitect ADMIN 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isempty(varargin),
    %if no arguments given, try to refresh the browser
    warndlg('This is a subfunction for the Graph Eval figure, it should be called with arguments','SIMarchitect: Message');
else
    action=varargin{1};
    switch action
    case 'Create'
        listH=varargin{2};
        data= varargin{3};
        listH=treeview('Create',listH,data);
        set(listH,'Callback','graph_eval_browser Update');
    case 'select'
        listH=varargin{2};
        output=get_selection(listH);
    case 'refresh'
        listH=varargin{2};
        data= varargin{3};
        refresh_browser(listH,data);
    case 'IsDisplayed'
        listH=varargin{2};
        output=get_displayed(listH);
    case 'CreateTree'
        ud=varargin{2};
        output=CreateTree(ud);
    case 'recreate'
        listH=varargin{2};
        old_string=varargin{3};
        recreate(listH,old_string);
        output=[];
    case 'Update'
        Update
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function create_browser(varargin)

figH=varargin{1};
data = varargin{2};

listH=treeview('Create',figH,data);
set(listH,'Units',        'normalized', ...
    'Position',           [0.05 0.1 0.9 0.88],...
    'Visible',            'on',...
    'Enable',             'on', ...
    'Tag',                'TheListBox');    %create listbox

set(figH,'Visible','on')
%    'Callback',           'graph_eval_browser Update',...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
function [output] = get_selection(listH)

% Get the current selections
selected=get(listH,'Value');
if isempty(selected),
    warndlg('Nothing selected');
    output=[-1];
    return
end
ud = get(listH, 'UserData');
[dd idx] = FindDisplayedData(ud);

ii=1;
output={};
for i=1:length(selected),
    if dd(selected(i)).IsExpanded==-1,
        output{ii}=[dd(selected(i)).Fullname];
        ii=ii+1;
    else
        errordlg('An expandable variable is selected in the listbox, these cannot be plotted.','SIMarchitect: Error');
        output=[-1];
        return
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
function [output] = get_displayed(listH)

ud = get(listH, 'UserData');
[dd idx] = FindDisplayedData(ud);

output=dd;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
function refresh_browser(listH,data)

listH=treeview('Create',listH,data);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
function [dd, idx] = FindDisplayedData(ud)

dd  = ud;
idx = [];
rem = [];

for i = 1:length(ud)
    if ud(i).IsDisplayed
        idx = [idx i];
    else
        rem = [rem i];
    end
end
dd(rem) = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
function listString = CreateTree(ud)
% Create the string to be set in the tree listbox.

listString = {};
idx = 0;
for i = 1:length(ud)
  gap    = '            ';  % one space
  
  switch ud(i).IsExpanded
    case -1
      expand = '';
    case 0
      expand = '+ ';
    case 1
      expand = '- ';
    end
  
  
  indent = [gap(ones(1,3*ud(i).Level)) expand];
  if ud(i).IsDisplayed
    idx = idx + 1;
    listString{idx,1} = [indent ud(i).Signal];
  end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                
function recreate(listH,old_string)
%try to expand same elements in new list as were in old string
ud=get(listH,'Userdata');   %get current userdata
if ~isempty(ud),
    length_ud=size(ud,2);
    for i=1:size(old_string,2),
        try
            for ii=1:length_ud,
                if strcmp(old_string(i).Fullname,ud(ii).Fullname),
                    ud(ii).IsDisplayed=1;
                    if old_string(i).IsExpanded,
                        ud(ii).IsExpanded=old_string(i).IsExpanded;
                        ch = ii + ud(ii).Children;
                        for k = ch
                            ud(k).IsDisplayed = 1;
                        end
                    end
                    break
                end
            end
        end
    end
end
set(listH,'Userdata',ud); 
string=CreateTree(ud);
set(listH, 'String',string);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Update
% Update the listbox containing the tree
H = gcbf;

if strcmp(get(H,'SelectionType'),'open')
  UpdateTreeView;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function UpdateTreeView
%% Same (sub)function as in treeview.m. Modified the case -1, it is now issues a plot command

H  = gcbo;
ud = get(H, 'Userdata');
[dd idx] = FindDisplayedData(ud);

sel = get(H,'value');
if length(sel)==1,
    switch dd(sel).IsExpanded
    case -1
        % element is not expandable
        graph_eval('plot_variable')
        return;
        
    case 0
        % expand element and set display state of its children to TRUE
        dd(sel).IsExpanded = 1;
        index = idx(sel); %index into ud
        ud(idx) = dd;
        ud = UpdateChildren(ud, index, 1);
        
    case 1
        % collapse element and set display state of its children to FALSE
        dd(sel).IsExpanded = 0;
        index = idx(sel); %index into ud
        ud(idx) = dd;
        ud = UpdateChildren(ud, index, 0);
        
    end
else %more than one selected
    for i=1:length(sel),
        if dd(sel(i)).IsExpanded==-1,
            graph_eval('plot_variable');
            return
        end
    end
end
    

ListboxTop = get(H,'ListboxTop'); %04122001 JE
set(H, 'UserData', ud)
set(H, 'String', CreateTree(ud));
SetValue(H,dd,sel,ListboxTop); %04122001 JE (see function)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function selected = SetValue(H,dd,sel,ListboxTop)
%% Make what's displayed more intuitive JE


%figure out number of visible items in list
units=get(H,'Units');
set(H,'Units','Points');
pos=get(H,'Position');
points=get(H,'FontSize')+2;
NumDisp=ceil(pos(4)/points)-1;
set(H,'Units',units);

%figure out number of items in list and the number of extra items compared to previous
NumList = length(get(H,'String'));
NumExt = max( NumList - length(dd), 0);


if (sel + NumExt + 3 - ListboxTop) > NumDisp,
    value = max(sel,min((sel+NumExt+3),NumList+1));
    set(H,'ListboxTop',max(min((value - NumDisp),sel),1))
else
    set(H,'ListboxTop',ListboxTop); %set listboxtop the same as previous
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ud = UpdateChildren(ud, i, top)
%% Same (sub)function as in treeview.m. 

switch ud(i).IsExpanded
  
  case 0
    ch = i + ud(i).Children;
    for k = ch
      ud(k).IsDisplayed = 0;
      if (ud(k).IsExpanded ~= -1)
	% expandable, then recurse
	ud = UpdateChildren(ud, k, 0);
      end
    end

  case 1
    ch = i + ud(i).Children;
    for k = ch
      ud(k).IsDisplayed = top;
      if (ud(k).IsExpanded ~= -1)
	% expandable, then recurse
	newtop = top & ud(k).IsExpanded;
	ud = UpdateChildren(ud, k, newtop);
      end
    end
    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Revision history
% 17/03/2002 JE:    Added the double-click functionality: When a single variable in the list is double-clicked it is directly plotted in the current axis.
%                   For this the callback of the listbox is now set to this function with the parameter 'Update'. The 'Update', the 'UpdateTreeView'
%                   and the 'UpdateChildren' functions are copied from 'treeview.m'. Only modification is that in 'UpdateTreeView'
%                   for case -1 a plot command is issued. The 'SetValue' function is also copied, however this was already a JE made function for providing 
%                   more intuitive behaviour of listbox.