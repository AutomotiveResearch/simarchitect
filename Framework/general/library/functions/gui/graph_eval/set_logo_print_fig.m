%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function set_logo(fig, scaling_factor, location)
logo = 'logoSIMarchitect.jpg';
if exist(logo)
    ax = axes;
    fig_pos = get(fig, 'Position');
    logo_map = imread(logo); image(logo_map);
    [logo_h, logo_w, dummy] = size(logo_map);
    logo_w = logo_w * scaling_factor; logo_h = logo_h * scaling_factor;
    switch location % Note: R2011b and R2015b seem to use different starting points... difference not resolved in code here.
        case 'NorthEast', pos1 = fig_pos(3) - logo_w + 1; pos2 = fig_pos(4) - logo_h + 1;
        case 'SouthWest', pos1 = 1; pos2 = 1;
%         case 'SouthEast', pos1 = fig_pos(3) - logo_w + 1; pos2 = 1;
        case 'SouthEast', pos1 = fig_pos(3) + 435 ; pos2 = fig_pos(4)+15; 
%         case 'SouthEast', pos1 = fig_pos(3) - logo_w + 1 - 120; pos2 = 1 + 30;
        case 'East', pos1 = fig_pos(3) - logo_w + 1; pos2 = round((fig_pos(4) - logo_h) / 2);
    end
    
    set(ax, 'Parent', fig, 'Units', 'Pixels', 'Position', [pos1, pos2, logo_w, logo_h], 'Visible', 'off');
    set(ax, 'Units', 'Normalized') % parent required for panel
    ud = get(fig, 'UserData'); ud.logo = ax; set(fig, 'UserData', ud)
end
