%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function ud=graph_eval_limits(ud,option,num)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Purpose:  Set limits in plots. Belongs to graph_eval GUI.
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if isempty(ud),
    ud=get(gcbf,'Userdata');
    flag=1;
else
    flag=0;
end
switch option
case 'set_lim'      
    switch num
    case 0    % a general set
        if strcmp(get(gcbo,'Tag'),'axis_window'),
            %get object handles
            edit_field=gcbo;
            
            window=str2num(get(edit_field,'String'));
            if window<=0,
                errordlg('Value must be >0','SIMarchitect')
                return
            end
            crrnt_x_min=min(min(ud.x_lim)); %the current minimum x-value
            update_slider(ud,window,crrnt_x_min); %sub function
            
            %set the axis setting to the new value
            xmin=crrnt_x_min; %(crrnt_value/max_step)*window;
            Xlim=[xmin,xmin+window];
            
            %apply new limits to all available axises and save data in ud
            for i=1:ud.axis_num,
                set(ud.handles.axis(i),'XLim',Xlim,'XlimMode','manual')
                ud.x_lim(i,:)=Xlim;
            end
            
            %set the value in the x_max box
            set(ud.handles.EXmax,'string',num2str(Xlim(1)+window)); %set the value in the window box
            
        else
            
            Xmin_str=get(ud.handles.EXmin,'string');
            Xmax_str=get(ud.handles.EXmax,'string');
            Xlim_str=[Xmin_str,' ', Xmax_str];
            Xlim=str2num(Xlim_str); %now in doubles
            if (Xlim(2)-Xlim(1))<=0,
                errordlg('Xmin must be < than Xmax and both must be valid numbers','SIMarchitect')
                return
            end
            
            for i=1:ud.axis_num,
                set(ud.handles.axis(i),'Xlim',[Xlim],'XlimMode','manual');
            end
            
            
            window=Xlim(2)-Xlim(1);                
            set(ud.handles.EWindow,'string',num2str(window)); %set the value in the window box
            update_slider(ud,window,Xlim(1)); %sub function
            
            %save data in ud
            ud.x_lim(1:4,1)=Xlim(1);
            ud.x_lim(1:4,2)=Xlim(2);
        end
        
    otherwise	% a specific set for one axes
        num_str=num2str(num); %prevents having to call num2str each time
        eval(['Xmin_str=get(ud.handles.EXmin',num_str,',''string'');']);
        eval(['Xmax_str=get(ud.handles.EXmax',num_str,',''string'');']);
        
        Xlim_str=[Xmin_str,' ', Xmax_str];
        Xlim=str2num(Xlim_str); %now in doubles
        if (Xlim(2)-Xlim(1))<=0,
            errordlg('Xmin must be < than Xmax and both must be valid numbers','SIMarchitect')
            return
        end
        
        set(ud.handles.axis(num),'Xlim',Xlim,'XlimMode','manual');
        ud.x_lim(num,1)=str2num(Xmin_str);
        ud.x_lim(num,2)=str2num(Xmax_str);
        
    end%switch num
    
case 'reset_lim'
    switch num
    case 0	%general reset
        if ud.datasets>=1,
            %figure out the limits
            Xmin_str=num2str(min(ud.x_min));
            Xmax_str=num2str(max(ud.x_max));
            Xlim_str=[Xmin_str,' ',Xmax_str];
            Xlim=str2num(Xlim_str); %now in doubles
            window=Xlim(2)-Xlim(1);
            
            %set the limits
            if max(ud.x_max)-min(ud.x_min)>0,   %Values must be increasing
                for i=1:ud.axis_num,                   
                    set(ud.handles.axis(i),'Xlim',Xlim,'XlimMode','auto');
                end
            end
            eval(['set(ud.handles.EXmax,''string'',',Xmax_str,');']);
            eval(['set(ud.handles.EXmin,''string'',',Xmin_str,');']);
            
            set(ud.handles.EWindow,'string',num2str(window)); %set the value in the window box
            update_slider(ud,window,Xlim(1)); %sub function
            
            %save data in ud
            ud.x_lim(1:4,1)=Xlim(1);
            ud.x_lim(1:4,2)=Xlim(2);
        else
            %no data available set limits to [0 1]
            window=1;
            Xmax='1';
            Xmin='0';
            set(ud.handles.EWindow,'string',num2str(window)); %set the value in the window box                
            eval(['set(ud.handles.EXmax,''string'',',Xmax,');']);
            eval(['set(ud.handles.EXmin,''string'',',Xmin,');']);                
            update_slider(ud,window,0); %sub function
        end
    otherwise	%specific reset
        %get handle of lines
        h=[]; X=[]; %initialise
        h=ud.handles.axis(num);
        X=findobj(h,'type','line');
        
        %get the absolute minimum and maximum of lines in plot
        X=get(X,'Xdata');	%cell array with limits
        if iscell(X),		%when multiple lines are plotted in a graph it's a cell otherwise an array
            for i=1:length(X),limits(i,:)=[X{i}(1) X{i}(end)]; end
            limits=[min(min(limits)) max(max(limits))];
        elseif isempty(X),
            limits=[0 1];
        else           
            limits=[min(X) max(X)];
        end
        
        %set the limits      
        eval(['set(h,''Xlim'',[',num2str(limits),'],''XlimMode'',''auto'');']);      
        % Set values in display
        if ud.ChkIndividual,
            eval(['j1=ud.handles.EXmin',num2str(num),';']);
            eval(['j2=ud.handles.EXmax',num2str(num),';']);
            eval(['set(j1,''string'',num2str(limits(1)));']);
            eval(['set(j2,''string'',num2str(limits(end)));']);
        else    % can be the case during a refresh action.
            set(ud.handles.EXmin,'String',num2str(min(ud.x_min)));
            set(ud.handles.EXmax,'String',num2str(max(ud.x_max)));            
            window=max(ud.x_max)-min(ud.x_min);
            set(ud.handles.EWindow,'string',num2str(window)); %set the value in the window box
            update_slider(ud,window,0); %sub function            
        end
        
        ud.x_lim(num,:)=limits;
        
    end %switch num
    
case 'slider' %callback from the slider
    
    %get object handles
    slider=gcbo;
    
    %get object properties
    crrnt_value=get(slider,'value');
    window=get(slider,'Userdata');
    slider_step=get(slider,'SliderStep');
    max_step=slider_step(2);
    
    
    %calculate new limts    
    xmin=(crrnt_value/max_step)*window+min(ud.x_min);
    Xlim=[xmin,xmin+window];
    
    %apply new limits to all available axises
    for i=1:ud.axis_num,
        set(ud.handles.axis(i),'Xlim',Xlim);         
        ud.x_lim(i,:)=Xlim;
    end
    
    %set the value in the window, x_max and x_min box
    set(ud.handles.EWindow,'string',num2str(window)); %set the value in the window box
    set(ud.handles.EXmax,'string',num2str(Xlim(1)+window)); %set the value in the window box
    set(ud.handles.EXmin,'string',num2str(Xlim(1))); %set the value in the window box
    
end %switch option
if flag,
    set(gcbf,'Userdata',ud);
end





function update_slider(ud,window,crrnt_x_min)
%calculate the number of steps
x_min=min(min(ud.x_min),crrnt_x_min);
x_max=max(max(ud.x_max),crrnt_x_min+window);

num_steps=(x_max-x_min)/window-1;  %the number of steps        
if num_steps>0,
    max_step=1/num_steps;
    crrnt_value=abs(max_step*(crrnt_x_min-x_min)/window);
    crrnt_value=max((min(crrnt_value,1)),0);    %limit between 0 and 1
else
    max_step=inf;
    crrnt_value=0;
end

%apply the new values to the slider
set(ud.handles.Slider,...
    'SliderStep',[min(0.2*max_step,1), max_step],...
    'Value',crrnt_value,...
    'Userdata',window);

