%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function bool = menu_test_models (varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%
% Description: menu for opening test models, also tries to change work-directory and opens the corresponding help file
%              bool=1 means a model is opened, bool=0 means the menu has been closed
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

bool   = 0; % default
choice = 1; % When there is one model, choice is always one!

if nargin==0,
    test_models = get_installed_example_models; %get_installed_models;
    
    if isempty(test_models)
        uiwait( msgbox('There are no examples installed.', 'Open examples') );
        return;
    end
        
    % When there is more then one model, please choice for the user.
    if length(test_models) ~= 1
        menu_items = {};
        for i=1:length(test_models)
            if isfield(test_models{i}, 'name')
                menu_items{i} = test_models{i}.name;
            end
        end
        
         choice=menu('Choose a test model', menu_items); %returns a number
        
        if choice == 0
            return;
        end
    end
    
    example = test_models{choice};
    
    % Check the information provided of the example!
    if ~isfield(example, 'dir') || ~isfield(example, 'model')
        msgbox('Example is not correctly installed, cannot open the model.', 'Open examples');
        return;
    end
    
    if choice
        try
            %open the model
            cd([SIMarchitect_root, '\', example.dir]);
            eval(['open_system(''',SIMarchitect_root,'\', example.dir, '\', example.model, ''')']);
            
            p_dir = char(get_param([gcs,'/ParameterSetting'],'MaskValues'));
            p_dir_actual = [SIMarchitect_root, '\', example.dir, '\parameterfiles'];
            
            if ~strcmp(p_dir,p_dir_actual),
                %try to change the working directory    
                try
                    set_param([gcs,'/ParameterSetting'],'MaskValues',{[SIMarchitect_root,'\', example.dir, '\parameterfiles']});
                    cd parameterfiles;
                    answ=questdlg(['SIMarchitect automatically changed the working directory for this example-model, do you want to save '...
                            'this change (recommended)?'],'SIMarchitect question','Yes','No','Yes');
                    if strcmp(answ,'Yes'),
                        save_system
                    end
                end
            end
            
            %open the help file
%            menu_test_model(example.help);
            bool=1;

        catch
            bool=0;
            errordlg(['Error occured: ',lasterr],'SIMarchitect');
            return
        end       
    end

end
