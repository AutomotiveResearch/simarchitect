%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function subtract_struct(command,field_input)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%
% Description: subtract_struct(command) subtracts the content of the second
% (and third etc.) structure from the corresponding fields of the first
% structure. The result is placed in a new structure in the base WS with
% the name: "second"_new, "third"_new etc.
%
% It is intended to be used with SIMarchitect structures. The 'parameters' and
% 'info' fields are skipped. However it can be used with any other
% structure as well.
%
% command = cell array with strings
% field_input = not required for command line input
% 
% Example:
%  subtract_struct({'dataset','(1:4)';'dataset_1','(1:4)';'dataset_2','(1:4)'});
%  This creates 2 new structures named: dataset_1_new and dataset_2_new.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global local


if nargin==1,
    field_input = char(command{1,1});
end

fields=evalin('base',['fieldnames(',field_input,')']);	%cell array with field names
for ii=1:length(fields),
   field=[field_input,'.',fields{ii}];
   class=evalin('base',['class(',field,');']);
   if strcmp(class,'struct') & isempty(strfind(field,'parameters')) & isempty(strfind(field,'info')),
       subtract_struct(command,field);
   elseif isempty(strfind(field,'parameters')) & isempty(strfind(field,'info'))     %skipping the 'parameters' and 'info' fields
       %We're now at the lowest level in a branch.
       
       %remove the name of the structure from the variable 'field'
       [a field] = strtok(field,'.');
       field = field(2:end); %remove the '.'
       
       for i=2:size(command,1),
           %create a string to be evaluated. String has the following format:
           string = [command{1,1},'.',field,command{1,2},'-',command{i,1},'.',field,command{i,2}];

           %create a field in the new structure with the result of the
           %subtraction.
           evalin('base',[command{i,1},'_new.',field,'=[',string,'];']); 
       end

   end                       
end      

%EOF