%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function create_struct(command,newname,field_input)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%
% Description: create_struct(command,newname) places the content of several
% structures (the structure needs to be identical) behind eachother in the
% variable 'structure' in the base workspace.
%
% It is intended to be used with SIMarchitect structures. The 'parameters' and
% 'info' fields are skipped.
%
% command = cell array with strings
% newname = string with the name of the new structure that is created in
%           the base WS
% field_input = not required for command line input
% 
% Example:
%  create_struct({'dataset','(1:4)';'dataset_2','(10)'},'newname');
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global local


if nargin==2,
    field_input = char(command{1,1});
end

fields=evalin('base',['fieldnames(',field_input,')']);	%cell array with field names
for ii=1:length(fields),
   field=[field_input,'.',fields{ii}];
   class=evalin('base',['class(',field,');']);
   if strcmp(class,'struct') & isempty(strfind(field,'parameters')) & isempty(strfind(field,'info')),
       create_struct(command,newname,field);
   elseif isempty(strfind(field,'parameters')) & isempty(strfind(field,'info'))     %skipping the 'parameters' and 'info' fields
       %We're now at the lowest level in a branch.
       
       %remove the name of the structure from the variable 'field'
       [a field] = strtok(field,'.');
       field = field(2:end); %remove the '.'
       
       %create a string to be evaluated. String has the following format:
       % "name_var1(begin:end);name_var2(begin2:end2)"
       string = [];
       for i=1:size(command,1),
           string = [string,';',command{i,1},'.',field,command{i,2}];
       end
       evalin('base',[newname,'.',field,'=[',string(2:end),'];']); %2 because of initial ',' in string
   end                       
end      

