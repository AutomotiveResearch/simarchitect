%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function test_batch_run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
%
% Description: This m-file serves as a template to set-up a batch run of simulations with SIMarchitect. 
%               !!!FIRST SAVE THIS FILE UNDER A DIFFERENT NAME !!!
%              There are 2 main issues to handle before a batch can be run successfully:
%   1) A cell array with the names of the parameter(s) that have to be varied has to be constructed. And another
%      cell array with the values for these parameters must also be defined.
%   2) The simulation loop has to be set up. Within the loop, the parameter structure has to be modified before the
%      start of a simulation. After a simulation the output data has to be handled to some extent.
%
%   This file gives an example how to do this.
%   
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set_param(bdroot(gcs),'SimulationCommand','Update') %Initialise the model
% parameters.active=1; 								% Set Simulink model parameter for reading parameters from Matlab WS. 
evalin('base','parameters.active=1;')                               % Set Simulink model parameter for reading parameters from Matlab WS. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%the parameter values that are to be used
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% First of all the names of the parameters that need to be modified at each simulation need to be defined in a
% cell-array with strings.
% NOTE: In this template it is assumed that for each run, the same parameters are modified in between simulations.
%
% Secondly the values for the parameters that will be modified have to be defined for each simulation. The required 
% format for use with the function 'modify_param_struct' is shown below:
% e.g.   param2modify={'parameters.parameterA','parameters.parameterB'};
%        value={ ...
%               {value1_parameterA,value1_parameterB},...
%               {value2_parameterA,value2_parameterB} ...
%              };
% NOTE: in case the parameter that is modified is the parameterfile that is loaded ('parameters. ... .Parameterfile')
%       the name of the parameterfile has to be entered as a string (WITHOUT THE EXTENSION).

param2modify={'parameters.powertrain.model.Init_area','parameters.powertrain.model.Init_volume'}; % (row definition)
    
    
value={ {10,10}  %1st value setting
        {20,20}  %2nd value setting
        {30,30}  %3rd value setting
        {40,40}  %4th value setting
    };
    

num_simulations=max(size(value)); %the number of simulations that will be ran


%% Locations for saving and names that will be used for saving (WITHOUT '\' at the end!)
save_dir= 'C:\Local files\Refurbishment SIMarchitect files\Test_model';

save_names={'1',...		%1st value setting
            '2',...		%2nd value setting
            '3',...		%3rd value setting
            '4',...		%4th value setting
        };


%The indices of the simulations that will be executed    
sim2run=(1:1:num_simulations);

%% Initialise WS
try
    evalin('base',['save(''',[save_dir],'\parameters_file'',''parameters'');']);% save a copy of the default structure 'parameters' in a file
%     eval(['save(''',[save_dir],'\parameters_file'',''parameters'');']);  % save a copy of the default structure 'parameters' in a file
catch
    disp(['_______SIMarchitect____________________________________________________'])   
    disp('ERROR: in saving parameterfile')
end

flag=0; %error flag    
err={};
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%simulation loop
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~flag
    for i=sim2run,
        %% Load the default parameter structure in the WS
        try,
            eval(['load(''',[save_dir],'\parameters_file'')']);
        catch
            bool=-1;
            disp(['_______SIMarchitect____________________________________________________'])
            disp(['WARNING: An error occured during the batch-run!! (on run# ',num2str(i),')']);    %issue a warning 
            disp(['ERROR was: ',lasterr,' During loading of parameters.']);        
            disp(['__________________________________________________________________'])        
        end
        
        %%% change parameters
        % param2modify= cell array with strings
        % value = cell array with values in an order that corresponds to the order of parameters in param2modify
%         [bool,parameters]=modify_param_struct(parameters,param2modify,value{i});    %a function where the structure 'parameters' is modified
        [bool,parameters]=modify_param_struct(parameters,param2modify,value{i});                    % a function where the structure 'parameters' is modified
        assignin('base', 'parameters', parameters)

        if bool~=-1,
            %%% start simulation
            try %to make sure batch doesn't stop when an error occurs
%                 yout_test=sim(bdroot(gcs),[0 20]);
%                 yout_test=sim('C:\Local files\Refurbishment SIMarchitect files\Test_model\Test_model_2015b_batchfile_fnc.slx',[0 20]);
               sim(bdroot(gcs),[0 10],simset('SrcWorkspace','base','DstWorkspace ','base')); % Can be used to test batch file
%                stop_fcn
			catch
                try
                    disp(['_______SIMarchitect____________________________________________________'])                
                    disp(['WARNING: An error occured during the batch-run!! (on run# ',num2str(i),')']);    %issue a warning in workspace
                    disp(['ERROR was: ',lasterr]);
                    disp(['__________________________________________________________________'])        
                    flag(i)=1;
                    err{i}=lasterr;                    
                end
            end
            
            
            %%% deal with output
            % At least the structure 'data_set' that is created has to be renamed, to prevent it from being overwritten by a subsequent
            % data_set, that will be created from the following simulation. In this default setting it is saved in a mat file, and 
            % afterwards the data_set and parameters are cleared to minimise memory usage.
            %
            % NOTE: for optimisation problems, the output can also be used to generate the new parameter values for the next simulation
            %%%
            try
                data_set = evalin('base','data_set');
                %store simulation time to be able to list it after batch
                time{i}=data_set.info.SimulationTime;            
                %store in 'data_set.info' some info concerning this run, which parameters were modified and with what values
                data_set.info.batch_info.params_modified=param2modify;
                data_set.info.batch_info.values=value{i};
                %save the data_set under the specified name
                eval(['save(''',[save_dir],'\',[save_names{i}],''',''data_set'');']);
                %clear the crrnt WS
%                 clear data_set parameters
                clear data_set parameters
                evalin('base','clear parameters data_set')
                
            catch
                try
                    disp(['_______SIMarchitect____________________________________________________'])                
                    disp(['WARNING: An error occured during the save process!! (on run# ',num2str(i),')']);    %issue a warning in workspace
                    disp(['ERROR was: ',lasterr]);
                    disp(['__________________________________________________________________'])        
                    flag(i)=1;
                    err{i}=lasterr;
                end
            end
        else
            disp(['_______SIMarchitect____________________________________________________'])        
            disp(['WARNING: An error occured during the batch-run!! (on run# ',num2str(i),')']);    %issue a warning in workspace
            disp(['Occured during modifying parameter structure.']);    %issue a warning in workspace
            disp(['__________________________________________________________________'])        
            flag(i)=1;
            err{i}=lasterr;
        end
        
    end
    
    %display simulation times
    disp(['_______SIMarchitect________->_SIMULATION TIMES_________________________'])    
    for i=1:length(sim2run),
        if~isempty(time{sim2run(i)}),
            disp(['Simulation time run #',num2str(sim2run(i)),': ',time{sim2run(i)}]);
        end
    end
    evalin('base',['clear all;']);                    % delete everything from WS                                                     
    eval(['delete(''',[save_dir],'\parameters_file.mat'');']);      % delete the temp-file that was used to store the parameters
    disp(['__________________________________________________________________'])            
end

%display whether or not errors occured
if sum(flag==1)>=1,
    disp(['_______SIMarchitect_________->_ERROR REPORT____________________________'])    
    disp('WARNING: Error(s) occured during the batch run.')
    for i=1:length(sim2run),
        if isempty(err{sim2run(i)}),
            disp(['Run #',num2str(sim2run(i)),': OK']);
        else
            disp(['Run #',num2str(sim2run(i)),': ',err{sim2run(i)}]);
        end
    end
    disp('check the command window for more details.')
    disp(['__________________________________________________________________'])            
else
    disp('___________SIMarchitect___________________________________')    
    disp('No errors occured during batch run')
    disp('_____________________________________________________')    
end


