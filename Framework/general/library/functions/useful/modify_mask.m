%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function modify_masks(varargin)

if nargin==0,
    handles=find_system(gcs,'MaskVariables',['parameterfile=&1;']);
else
    handles={gcb}; 
end    
for i=1:length(handles)
    %% This m-file sets all attributes to a block that are necessary for the SIMarchitect blocks
    handle=get_param(handles(i),'Handle');
    handle=handle{1};
    block_name=get_param(handle,'Name');
    
    %set tag
    set_param(handle,'Tag',block_name);
    
    % install mask
    set_param(handle,'Mask','on');
    set_param(handle,'MaskType',block_name);
    set_param(handle,'MaskPromptString','Enter the name of the parameterfile (without the extension):');
    set_param(handle,'MaskVariables','parameterfile=&1;')
    

    init_string=sprintf(['component=gcb; \n',...
            '[params bool component]=mask_fcn(parameterfile,component); \n',...
            'fields=fieldnames(params); \n',...
            'for counter_i=1:length(fields), \n',...
            'eval([fields{counter_i},''=params.'',fields{counter_i},'';'']); \n',...
            'end \n',...        
            'if bool==0,\n',...
            'eval([''disp(''''File loaded: '',parameterfile,''.m  Into: '',component,'''''');'']); \n',...   	      
            'elseif bool==1 \n',...
            'eval([''disp(''''Parameters loaded from parameter-structure into: '',component,'''''');'']); \n',...            
            'end \n']);
    set_param(handle,'MaskInitialization',init_string)
    
    help_string=sprintf(['eval(''createindex(get_param(gcb,''''MaskType''''))''); \n',...
            'eval([''web file:///'''''',SIMarchitect_root,''\\documentation\\html_help\\index_module.html'''' -browser'']);']);
    set_param(handle,'MaskHelp',help_string);
    
    %optical
    %    set_param(handle,'BackgroundColor','LightBlue');
    set_param(handle,'DropShadow','on');
    
    % set copy function
    set_param(handle,'CopyFcn','copy_fcn(gcbh)');
    
    % set delete function
    set_param(handle,'DeleteFcn','delete_fcn(gcbh)');
    
    % set name change function
    set_param(handle,'NameChangeFcn','name_change_fcn(gcbh)');
    %set_param(handle,'NameChangeFcn','');
    
    % set move function
    set_param(handle,'MoveFcn','move_fcn(gcbh)');        
    
end
