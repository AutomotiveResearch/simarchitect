%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function varargout = expand_table(varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
% Description: This function expands 1d and 2d tables and their indices to larger tables and indices.
%              Syntax: (1)  [new_tabx new_tabt] = expand_table(old_tabx,old_tabt,[length of  new table])
%                      (2)  [new_tabx new_taby new_tabt] = expand_table(old_tabx,old_taby,old_tabt,[#rows #cols])
%                           Where 'tabx' is the row index and 'taby' is the column index.
%                      (3)  [new_tabx new_taby new_tabz new_tabt] = 
%                               expand_table(old_tabx,old_taby,old_tabz,old_tabt,[#rows #cols #'sheets'])
%                           Where 'tabx' is the row index, 'taby' is the column index and 'tabz' is the 'sheet' index .
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%input handling
if nargin == 3, % a 1d table
    in_tabx = varargin{1};
    in_tabt = varargin{2};
    dimt = [1 varargin{3}]; %length of the table (1 row multiple columns)

    %check/correct form
    in_dim = size(in_tabt);
    if in_dim(1)>in_dim(2),
        in_tabt = in_tabt';
        in_dim = size(in_tabt);
    end        
    if in_dim(2)>dimt(2),
        errordlg(['The table you are trying to expand is larger than the target table. ',...
                    'Please reduce the size of the base table.'],'SIMarchitect: Error');
        varargout = {-1;-1};
        return        
    end
    
    %create index
    out_tabx = [in_tabx,ceil(in_tabx(end))+1:ceil(in_tabx(end))+dimt(2)-in_dim(2)];
    
    %create table
    out_tabt = zeros(dimt);
    out_tabt(1:in_dim(2)) = in_tabt;

    %provide output
    varargout = {out_tabx,out_tabt};

elseif nargin == 4, % a 2d table
    in_tabx = varargin{1};
    in_taby = varargin{2};    
    in_tabt = varargin{3};
    dimt = varargin{4};
    
    in_dim = size(in_tabt);    

    %check/correct form??
    if sum(in_dim>dimt)>0,,
        errordlg(['The table you are trying to expand is larger than the target table. ',...
                    'Please reduce the size of the base table.'],'SIMarchitect: Error');
        varargout = {-1;-1;-1};
        return        
    end
    
    %create indices
    out_tabx = [in_tabx,ceil(in_tabx(end))+1:ceil(in_tabx(end))+dimt(1)-in_dim(1)];
    out_taby = [in_taby,ceil(in_taby(end))+1:ceil(in_taby(end))+dimt(2)-in_dim(2)];
    
    %create table
    out_tabt = zeros(dimt);
    out_tabt(1:in_dim(1),1:in_dim(2)) = in_tabt;
    
    %Provide output
    varargout = {out_tabx,out_taby,out_tabt};  
    
elseif nargin == 5, % a 3d table
    in_tabx = varargin{1};
    in_taby = varargin{2};
    in_tabz = varargin{3};
    in_tabt = varargin{4};
    dimt = varargin{5};
    
    in_dim = size(in_tabt);    

    %check/correct form??
    if sum(in_dim>dimt)>0,,
        errordlg(['The table you are trying to expand is larger than the target table. ',...
                    'Please reduce the size of the base table.'],'SIMarchitect: Error');
        varargout = {-1;-1;-1};
        return        
    end
    
    %create indices
    out_tabx = [in_tabx,ceil(in_tabx(end))+1:ceil(in_tabx(end))+dimt(1)-in_dim(1)];
    out_taby = [in_taby,ceil(in_taby(end))+1:ceil(in_taby(end))+dimt(2)-in_dim(2)];
    out_tabz = [in_tabz,ceil(in_tabz(end))+1:ceil(in_tabz(end))+dimt(3)-in_dim(3)];    
    
    %create table
    out_tabt = zeros(dimt);
    out_tabt(1:in_dim(1),1:in_dim(2),1:in_dim(3)) = in_tabt;
    
    %Provide output
    varargout = {out_tabx,out_taby,out_tabz,out_tabt};  
    
else
    errordlg('Incorrect call of function expand_table.(See help expand_table)','SIMarchitect: Error');
    varargout{1} = -1;
    return
end
