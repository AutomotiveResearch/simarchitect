%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function copy_fcn(block_handle)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author: SIMarchitect ADMIN
% Version: 1.0 
% Date: 08-11-2018
% Description: This function is the default 'CopyFcn' of the component models in the SIMarchitect library. Purposes:
%	- It places: 2 from blocks, a goto block and a bus selector block and connects them in an appropriate way, to
%		handle all signals.
%	- In the case that there is already a block with the same mask-type (which should be the same as the block name
%	  in the library) in the model, it renames the block, and all other blocks that are inserted.
%
%   -this function also calls the copy param_file function, which attempts to copy the default parameterfile to a 
%    suitable location
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%________________________________________________________________________________________________________
%
% 0) Get information about copied Module
% 1) Add the SIMarchitect Selector to con_i
% 2) Add FROM for Selector 
% 3) Add GOTO for con_o
% 4) Add FROM to the databus connector
% 5) Section that carries out parameterfile stuff 
%
%________________________________________________________________________________________________________

%________________________________________________________________________________________________________
%
% 0) Get some Module info
%
block_position=get_param(block_handle,'Position'); %position of caller block 
%e.g [left top right bottom] (the four corners in pixels on the screen) (top left is 0,0)
block_name=get_param(block_handle,'Name'); % the name of the caller block
block_handle=get_param(block_handle,'handle'); % get handle
block_mask=get_param(block_handle,'Mask');
system_name=gcs;	%name of the system

% set_param(block_handle,'BackgroundColor','yellow'); 
set_param(block_handle,'LinkStatus','inactive'); 

con_i_position=get_param(block_handle,'InputPorts');
if ~isempty(con_i_position), 
    con_i_position=con_i_position(1,:); 
end; % NOTE assumes first port is always the con_i port (don't know how to look for tag)

con_o_position=get_param(block_handle,'OutputPorts');
if ~isempty(con_o_position), 
    con_o_position=con_o_position(1,:); 
end; % NOTE assumes first port is always the con_o port (don't know how to look for tag)

%________________________________________________________________________________________________________
flag_tot = 0;


%________________________________________________________________________________________________________
%
% 1) insert SIMarchitect-Selector
%
if strcmp(block_mask,'on') & ~isempty(con_i_position),
    %%% add bus_selector block to be connected to con_i port
    newname_bus_selector=[system_name,'/selector_',block_name]; % name is selector_"blockname"
    flag=find_system(gcs,'SearchDepth',1,'Name',['selector_',block_name]);  
    if isempty(flag), % Selector block doesn't exist already...
        newposition_bus_selector=[block_position(1),block_position(2)-5-15,block_position(1)+10,block_position(2)-5];
        
        try 
            add_block('mdl_parts/BusSelector',newname_bus_selector,...
                'Position',newposition_bus_selector,...
                'Orientation','left',...
                'LinkStatus','none',...
                'MoveFcn','move_fcn_sub(gcbh)',...
                'Tag','SIMarchitect_selector',...
                'ShowName','off');
            %connect to the con_i
            bus_selector_outputport_position=get_param(newname_bus_selector,'OutputPorts');
            line1=add_line(system_name,[bus_selector_outputport_position;bus_selector_outputport_position(1)-10,...
                    bus_selector_outputport_position(2);con_i_position]);
        catch
            try 
                load_system('mdl_parts');
                add_block('mdl_parts/BusSelector',newname_bus_selector,...
                    'Position',newposition_bus_selector,...
                    'Orientation','left',...
                    'LinkStatus','none',...
                    'MoveFcn','move_fcn_sub(gcbh)',...
                    'Tag','SIMarchitect_selector',...
                    'ShowName','off');
                
                bus_selector_outputport_position=get_param(newname_bus_selector,'OutputPorts');
                line1=add_line(system_name,[bus_selector_outputport_position;bus_selector_outputport_position(1)-10,...
                        bus_selector_outputport_position(2);con_i_position]);                    
            catch
                errordlg(sprintf(['Cannot find the BusSelector or the model parts library.\n']));
                msgtext=strvcat( 'Cannot find the BusSelector or the model parts library.' ,...
                                 'Check if the SIMarchitect directories are available on the Matlab path');
                msgbox(msgtext,'ERROR: SIMarchitect path definitions','warn');
                return
            end
        end
    end
end


%________________________________________________________________________________________________________
%
% 2) Insert FROM (with 'data_bus') for SIMarchitect-Selector
%
if ~isempty(con_i_position), %only if there's an input port
    
    newname_from1=[system_name,'/from_main_',block_name]; % name is from_main_"blockname"
    flag=find_system(gcs,'SearchDepth',1,'Name',['from_main_',block_name]);  
    if isempty(flag), %block doesn't exist already...
        
        goto_tag_from1=['data_bus'];	%the name of the main bus connector goto tag
        if strcmp(block_mask,'on'),
            newposition_from1=[block_position(1)+25,block_position(2)-5-15,block_position(1)+50,block_position(2)-5];
        else
            newposition_from1=[block_position(1),block_position(2)-5-15,block_position(1)+25,block_position(2)-5];
        end
        
        try 
            add_block('built-in/from',newname_from1,...
                'GotoTag',goto_tag_from1,...
                'Position',newposition_from1,...
                'Orientation','left',...
                'ForegroundColor','gray',...
                'MoveFcn','move_fcn_sub(gcbh)',...
                'Tag','SIMarchitect_from1',...
                'ShowName','off');
            
            from1_port_position=get_param(newname_from1,'OutputPorts');
            if strcmp(block_mask,'on'),
                %connect to the bus_selector
                bus_selector_inputport_position=get_param(newname_bus_selector,'InputPorts');
                line1=add_line(system_name,[from1_port_position;bus_selector_inputport_position]);
            else
                %connect to the con_i
                line1=add_line(system_name,[from1_port_position; from1_port_position(1)-10,...
                        from1_port_position(2);con_i_position]);
            end
        end
    end
end


%________________________________________________________________________________________________________
%
% 3) Add GOTO block to be connected to con_o port
%
if ~isempty(con_o_position), %only if there's an output port
    newname_goto=[system_name,'/goto_',block_name];  % name is: goto_"blockname"
    goto_tag_goto=[block_name];	%the name of output port connector goto tag: "blockname"    
    flag=find_system(gcs,'SearchDepth',1,'Name',['goto_',block_name]);  
    if isempty(flag), %block doesn't exist already...
        
        newposition_goto=[block_position(3)-25,block_position(2)-5-15,block_position(3),block_position(2)-5];
        try
            add_block('built-in/goto',newname_goto,...
                'GotoTag',goto_tag_goto,...
                'Position',newposition_goto,...
                'Orientation','left',...
                'ForegroundColor','gray',...
                'Tag','SIMarchitect_goto',...
                'MoveFcn','move_fcn_sub(gcbh)',...
                'ShowName','off');
            %connect to the con_o
            goto_port_position=get_param(newname_goto,'InputPorts');
            signal_name=block_name;
            line2=add_line(system_name,[con_o_position;con_o_position(1)+10,con_o_position(2);goto_port_position]);
            set_param(line2,'Name',signal_name);
        end
        
    else %block exists already, so we have to call in the name_change_fcn later to get all tags etc, right.
        flag_tot = 1;
    end
    
end


%________________________________________________________________________________________________________
%
% 4) Add FROM block to be connected to Bus-Connector
%
if ~isempty(con_o_position), % a. %only if there's an output port
    
    % Define name for FROM block
    newname_from2=[system_name,'/from_',block_name];  % name is: from_"blockname"
    
    flag_goto_tag=find_system(gcs,'SearchDepth',1,'BlockType','From','GotoTag',block_name);
    
    if isempty(flag_goto_tag), % b.
        
        flag_newname=find_system(gcs,'SearchDepth',1,'BlockType','From','Name',['from_',block_name]);
        % check if name is unique
        if ~isempty(flag_newname),
            newname_from2=[newname_from2,'_x'];
        end
        
        goto_tag_from2=goto_tag_goto;	%identical to goto-block goto_tag
    
        % Connect FROM block to main or sub-connector if possible
        % Get name of connector
        connector=[];
        connector=find_system(system_name,'SearchDepth',1,'Name','sub_connector'); %cell array
        
        if isempty(connector),
            connector=find_system(system_name,'SearchDepth',1,'Name','main_connector');
        end
        if ~isempty(connector), % c.
            connector=connector{1}; %character array
            % find out about free ports
            ports_old=get_param(connector,'PortConnectivity');  %returns a structure, the field 'SrcBlock' is important, -1 indicates unconnected
            indx=[];
            for i=1:length(ports_old),
                if ~isempty(ports_old(i).SrcBlock), %prevent warning
                    if ports_old(i).SrcBlock==-1,
                        indx=i;
                        break
                    end
                end
            end
            if isempty(indx),       % no free ports
                % create a port and get position
                ports_num=length(ports_old);
                eval(['set_param(connector,''Inputs'',''',num2str(ports_num),''');']);    %not +1 because there is also one output port
                % resize connector
                connector_pos=get_param(connector,'Position');
                new_con_pos=[connector_pos(1),connector_pos(2),max(connector_pos(3)+30,connector_pos(1)+50),connector_pos(4)];
                set_param(connector,'Position',new_con_pos);
                
                % get free port position
                ports_new=get_param(connector,'PortConnectivity');  %returns a structure, the field 'SrcBlock' is important, -1 indicates unconnected
                free_port_position=ports_new(end-1).Position;  %last port-1 is free port (last port is output port)
                
                % reposition all already connected FROM blocks
                for i=1:length(ports_new)-2,
                    %reposition only the FROM blocks or the constant 0 or constant 1, not the user added blocks
                    if strcmp(get_param(ports_new(i).SrcBlock,'BlockType'),'From') |...
                            strcmp(get_param(ports_new(i).SrcBlock,'Tag'),'none') |...
                            strcmp(get_param(ports_new(i).SrcBlock,'Tag'),'one'),
                        % do not reposition blocks that have more than 1 destination block (connector + ???) 
                        source=get_param(ports_new(i).SrcBlock,'PortConnectivity');
                        if length(source.DstBlock)==1,
                            set_param(ports_new(i).SrcBlock,'Position',[ports_new(i).Position(1)-15,ports_new(i).Position(2)+20,ports_new(i).Position(1)+15,ports_new(i).Position(2)+40]);
                        end
                        clear source
                    end
                end
                
                % reposition everything connected to output of connector
                port_shift = ports_old(end).Position(1) - ports_new(end).Position(1);            
                for i=1:length(ports_new(end).DstBlock),
                    pos=get_param(ports_new(end).DstBlock(i),'Position');
                    pos(1)=pos(1)-port_shift;
                    pos(3)=pos(3)-port_shift;
                    set_param(ports_new(end).DstBlock(i),'Position',pos);
                end
                % delete all lines connected to outport and redraw them
                delete_line(system_name,ports_new(end).Position);
                connector_name=get_param(connector,'Name');
                for i=1:length(ports_new(end).DstBlock),
                    DstBlock=get_param(ports_new(end).DstBlock(i),'Name');
                    line=add_line(system_name,[connector_name,'/1'],[DstBlock,'/1'],'autorouting','on');
                end
                
            else
                % connect to free port, create free port position
                free_port_position=ports_old(indx).Position;
            end
            newposition_from2=[free_port_position(1)-15,free_port_position(2)+20,free_port_position(1)+15,free_port_position(2)+40];
            try
                add_block('built-in/from',newname_from2,...
                    'GotoTag',goto_tag_from2,...
                    'Position',newposition_from2,...
                    'Orientation','up',...
                    'ForegroundColor','black',...
                    'ShowName','off');
                
                from_port_position=get_param(newname_from2,'OutputPorts');
                line=add_line(system_name,[from_port_position;free_port_position]);
            end 
            
            % Didn't find a suitable connector (e.g. when using/creating a Simulink subsystem or outside the SIMarchitect environment)
            % place it above inserted block and make it red
        else 
            newposition_from2=[block_position(3)-25,block_position(2)-25-15,block_position(3),block_position(2)-25];
            try
                add_block('built-in/from',newname_from2,...
                    'GotoTag',goto_tag_from2,...
                    'Position',newposition_from2,...
                    'Orientation','right',...
                    'ForegroundColor','red',...
                    'ShowName','off');
            end
        end % c.
    end % b.
end % a.
if flag_tot == 1,
    name_change_fcn(block_handle);
end

%________________________________________________________________________________________________________
%
% 5) Section that deals with the parameterfile of the block (copies it to working directory, if necessary)
%________________________________________________________________________________________________________

if strcmp(block_mask,'on'),
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % a. get name of file this block is looking for
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    parameterfile=[];
    try
        variables=char(get_param(block_handle,'MaskVariables'));
        values=char(get_param(block_handle,'MaskValues'));
        for i=1:size(variables,1),
            if strcmp(deblank(variables(i,:)),'parameterfile=&1;'),
                parameterfile=values(i,:);
                %% make sure it has .m extension
                if length(parameterfile)>2,
                    if ~strcmp(parameterfile(end-2:end),'.m'),
                        parameterfile=[parameterfile,'.m'];
                    end
                else    %short filename, for sure no extension
                    parameterfile=[parameterfile,'.m'];
                end
            end
        end
    catch
        errordlg(lasterr);
        return
    end

    if isempty(parameterfile),
        return %there is no parameterfile on the mask of the copied system. (E.g. with visualisation blocks.)
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % b) get Working directory
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (~isempty(parameterfile)),
        %get work directory, if cannot be found set it to SIMarchitect_root\temp and communicate this
        save_options_block=find_system(bdroot(system_name),'SearchDepth',1,'Tag','ParameterSetting');   % look for GUI block with parameterfile directory on its mask
        if ~isempty(save_options_block),    %found the block
            try
                mask_values=get_param(char(save_options_block),'MaskValues'); %the mask variables
                work_dir=mask_values{1};
                if ~strcmp(work_dir(end),'\'),  %make sure it ends with '\'
                    work_dir=[work_dir,'\'];
                end
            catch
                errordlg(lasterr);
                return
            end    
            if isdir(work_dir),
                work_dir_flag=1;
            else
                %the work_dir is not a directory, may be due to copying of model or something
                y=questdlg(['Directory: ',work_dir,' does not exist, do you want to create it?'],...
                    'SIMarchitect: Question','Yes','No','Yes');
                if strcmp(y,'Yes'),
                    mkdir(work_dir);
                elseif strcmp(y,'No'),
                    work_dir_flag=0;
                end
            end            
        else
            work_dir_flag=0; %we're not in a default SIMarchitect model, so copy it to the temp folder
        end
    end
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % c. Decide whether we have to copy the file. Copy if:
    %       - file is found, but is not in working directory of the current model
    %       - it is a file without a working directory, and Matlab is looking at library file check if file available
    loc_paramfile=which(parameterfile);
    try 
        loc_paramfile=which(['lib_',parameterfile]);
    end
    
    if work_dir_flag,
        
        %check if file in work dir
        if exist([work_dir,parameterfile],'file')==2,                
            copy_flag=0;
            disp(['______']);
            disp(['The parameterfile for ',block_name,' that will be used is:']);
            disp(['   ',work_dir,parameterfile]);
        elseif  ~isempty(loc_paramfile)
            copy_flag=1;
            source=loc_paramfile;
            destination=[work_dir,parameterfile];
        else
            copy_flag=0;
            msgbox(['The parameterfile: ',parameterfile,' could not be found in the parameterfile-directory,',...
                    ' nor on the matlab path',...
                    ' and is also not a default parameterfile.',...
                    ' In order for the simulation to work, make sure the file is in the parameterfile-directory.',...
                    ' Or change the parameterfile for this module.'],...
                'SIMarchitect: Message');            
            disp(['______']);
            disp(['The parameterfile for ',block_name,', named ',parameterfile,' is NOT found.']);
        end
        
    elseif ~isempty(loc_paramfile)
        
        %not in a default model, so copy file to temp folder if matlab is looking at library file otherwise use the one found
        if strmatch(lower([SIMarchitect_root,'\library\parameterfiles']),lower(loc_paramfile)),
            %means Matlab is looking at this library file, so we have to copy it
            copy_flag=1;
            source=loc_paramfile;
            destination=[SIMarchitect_root,'\temp\',parameterfile];
            disp(['______']);
            disp(['The default parameterfile for ',block_name,' that will be used is:']);
            disp(['   ',destination]);
        else
            copy_flag=0;
            disp(['______']);
            disp(['The parameterfile for ',block_name,' that will be used is:']);
            disp(['   ',loc_paramfile]);
        end
        
    else %file not found and it is a model without a working directory
        
        copy_flag=0;
        msgbox(['The parameterfile: ',parameterfile,' could not be found in on the matlab path',...
                ' and is also not a default parameterfile.',...
                ' In order for the simulation to work, you have to make sure Matlab is able to find the file.',...
                ' Or change the parameterfile for this module.'],...
            'SIMarchitect: Message');
        disp(['______']);
        disp(['The parameterfile for ',block_name,', named ',parameterfile,' is NOT found.']);
        
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Copy file if necessary
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if copy_flag,
        %copy source to destination and echo results
        [status msg]=copyfile(source,destination,'writable');
        if status==0,
            disp(['ERROR: Error copying file: ',parameterfile,' :',msg]);
        else
            disp(['COPIED ',source]);
            disp(['    TO ',destination])
        end
        
    end
    disp(['______']);
    
end    
