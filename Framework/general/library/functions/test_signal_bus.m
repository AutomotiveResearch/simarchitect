%{ 
SIMarchitect is based on original work produced by TNO
Original work Copyright (c) 2018 [Netherlands Organisation for Applied Scientific Research - TNO]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
function test_signal_bus(block, signal_name)

signal_size=size(signal_name);
n_new=signal_size(1);

temp=get_param([block '/Demux'],'Ports');
n_old=temp(2);
Demux=[block '/Demux'];
%Mux=  [block '/Mux']; 
BusCreator = [block '/Bus Creator'];

if n_new == n_old
    for i=1:n_new
        %delete_line(block,['Demux/' num2str(i)],['Mux/' num2str(i)]);
        %line=add_line(block,['Demux/' num2str(i)],['Mux/' num2str(i)]);
        
        delete_line(block,['Demux/' num2str(i)],['Bus Creator/' num2str(i)]);
        line=add_line(block,['Demux/' num2str(i)],['Bus Creator/' num2str(i)]);   
        set_param(line,'Name',signal_name{i});
    end
else
	for i=1:n_old
		%delete_line(block,['Demux/' num2str(i)],['Mux/' num2str(i)]);
        delete_line(block,['Demux/' num2str(i)],['Bus Creator/' num2str(i)]);
    end
	set_param(Demux,'Outputs',num2str(n_new));
	%set_param(Mux,'Inputs',num2str(n_new));
    set_param(BusCreator,'Inputs',num2str(n_new));
	for i=1:n_new
	    %line=add_line(block,['Demux/' num2str(i)],['Mux/' num2str(i)]);
        line=add_line(block,['Demux/' num2str(i)],['Bus Creator/' num2str(i)]);
        set_param(line,'Name',signal_name{i});
    end
end