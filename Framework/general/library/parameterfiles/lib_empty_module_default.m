%  ============================================================
%   SIMarchitect default dummy parameterfile 
%
%   Filename:  empty_module_default.m
%
%	description : parameter file for SIMarchitect empty module
%	author      : 
%
%   
%  ============================================================

dummy_par=0; % file cannot be without at least 1 parameter/variable 

% This is an empty parameterfile for the empty module. 
% Please save it under a different name, and then select that parameterfile
% as the parameterfile for this module. For more details on how to include 
% your own module, please refer to the HTML documentation.


%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = 'signal input 1';
con_info(1).unit = '[...]';
con_info(2).descr = 'signal input 2';
con_info(2).unit = '[...]';

%%%%%%%%%%%
%revision history
% 
