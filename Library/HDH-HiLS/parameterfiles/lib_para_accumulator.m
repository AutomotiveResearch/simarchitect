%   Filename:  para_accumulator.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================


dat.comment = 'Hydraulic accumulator data';
dat.filename = 'para_accumulator.m';
dat.version = '1';
dat.lastModified = '24.02.2014';
dat.modifiedFrom = 'Jonas Fredriksson';

dat.gas.comment = 'gas properties';
dat.gas.adiabaticindex.comment = 'adiabatic index';
dat.gas.adiabaticindex.unit = '-';
dat.gas.adiabaticindex.value = 1.4;

dat.vol.precharge.comment = 'Precharge gas volume';
dat.vol.precharge.unit = 'm3';
dat.vol.precharge.value = 0.064;

dat.pressure.precharge.comment = 'Precharge gas pressure';
dat.pressure.precharge.unit = 'Pa';
dat.pressure.precharge.value = 120*10^5;

dat.vol.initial.comment = 'Initial gas volume';
dat.vol.initial.unit = 'm3';
dat.vol.initial.value = 0.044;

dat.pressure.max.comment = 'Maximum gas pressure';
dat.pressure.max.unit = 'Pa';
dat.pressure.max.value = 325*10^5;

dat.capacity.comment = 'system capacity';
dat.capacity.unit = 'kWh';
dat.capacity.value = 0.17;

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = 'Q  (Accumulator Flow Input)';
con_info(1).unit = '[m^3/s]';


%%%%%%%%%%%
%revision history
% 
