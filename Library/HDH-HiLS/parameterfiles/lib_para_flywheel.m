%   Filename:  para_flywheel.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'Flywheel data';
dat.filename = 'para_flywheel.m';
dat.version = '2';
dat.lastModified = '01.10.2013';
dat.modifiedBy = 'Jonas Fredriksson';

dat.inertia.comment = 'Inertia for flywheel';
dat.inertia.unit = '-';
dat.inertia.value = 1;

dat.speedlimit.comment = 'Speed limits for flywheel';
dat.speedlimit.lower.comment = 'lower flywheel speed limit';
dat.speedlimit.lower.unit = 'rad/s';
dat.speedlimit.lower.value = 0;
dat.speedlimit.upper.comment = 'upper flywheel speed limit';
dat.speedlimit.upper.unit = 'rad/s';
dat.speedlimit.upper.value = inf;

dat.loss.comment = 'friction torque loss = f(rotational speed)';
dat.loss.speed.Comment = 'rotational speed';
dat.loss.speed.Unit = 'rad/s';
dat.loss.speed.vec = [0  10000];
dat.loss.torqueloss.Comment = 'torque friction loss';
dat.loss.torqueloss.Unit = 'Nm';
dat.loss.torqueloss.vec = [0  0.1];

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = 'M_in      (Flywheel Torque Input)';
con_info(1).unit = '[Nm]';
con_info(2).descr = 'J_in       (Flywheel Inertia Input)';
con_info(2).unit = '[kg*m^2]';

%%%%%%%%%%%
%revision history
% 
