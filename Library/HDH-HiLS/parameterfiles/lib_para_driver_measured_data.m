%   Filename:  para_driver_measured_data.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

%-------------------------------------------------------------------------------------------------------------------
% Driver Data Input
%-------------------------------------------------------------------------------------------------------------------
% load 
% data.Drv_AccPedl_Rat = 
% data.Drv_BrkPedl_Rat = 

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = 'Put none, since the input of the module is already define in the parameter file.';
con_info(1).unit = '[-]';

%%%%%%%%%%%
%revision history
% 
