%   Filename:  para_elmachine.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'Electric machine model data';
dat.filename = 'para_elmachine.m';
dat.version = '1';
dat.lastModified = '18.04.2013';
dat.modifiedBy = 'Jonas Fredriksson';

dat.inertia.comment = 'Electric machine inertia';
dat.inertia.unit = 'kgm^2';
dat.inertia.value = 0.007;

dat.timeconstant.comment = 'Electric machine time constant';
dat.timeconstant.unit = '-';
dat.timeconstant.value = 0.01;

dat.maxtorque.comment = 'Electric machine maximum torque = f(speed)';
dat.maxtorque.speed.Comment = 'Index vector - machine speed';
dat.maxtorque.speed.Unit = 'rad/s';
dat.maxtorque.speed.vec = [0.0 1000.0 2000.0 3000.0 4000.0 5000.0 6000.0 7000.0 8000.0 9000.0]*2*pi/60;
dat.maxtorque.torque.Comment = 'maximum torque';
dat.maxtorque.torque.Unit = 'Nm';
dat.maxtorque.torque.vec = [118 118 120 120 113 97 83 69 58 47];

dat.mintorque.comment = 'Electric machine minimum torque = f(speed)';
dat.mintorque.speed.Comment = 'Index vector - machine speed';
dat.mintorque.speed.Unit = 'rad/s';
dat.mintorque.speed.vec = [0.0 1000.0 2000.0 3000.0 4000.0 5000.0 6000.0 7000.0 8000.0 9000.0]*2*pi/60;
dat.mintorque.torque.Comment = 'minimum torque';
dat.mintorque.torque.Unit = 'Nm';
dat.mintorque.torque.vec = [-118 -118 -120 -120 -113 -97 -83 -69 -58 -47];

dat.controller.comment = 'Electric machine pi controller for speed control';
dat.controller.p.Comment = 'proportional constant';
dat.controller.p.Unit = '-';
dat.controller.p.value = 20;
dat.controller.i.Comment = 'integral constant';
dat.controller.i.Unit = '-';
dat.controller.i.value = 2;

dat.elecpowmap.comment = 'Electric machine power  map = f(voltage, machine torque, machine speed)';
dat.elecpowmap.motor.voltage.Comment = 'Index vector - electric machine voltage';
dat.elecpowmap.motor.voltage.Unit = 'V';
dat.elecpowmap.motor.voltage.vec = [0 400 800];
dat.elecpowmap.motor.speed.Comment = 'Index vector - electric machine speed';
dat.elecpowmap.motor.speed.Unit = 'rad/s';
dat.elecpowmap.motor.speed.vec = [0.0 4500.0 9000.0]*2*pi/60;
dat.elecpowmap.motor.torque.Comment = 'Index vector - electric machine torque';
dat.elecpowmap.motor.torque.Unit = 'Nm';
dat.elecpowmap.motor.torque.vec = [0    60   120];
dat.elecpowmap.motor.elecpow.Comment = 'electric power map for driving';
dat.elecpowmap.motor.elecpow.Unit = 'W';
dat.elecpowmap.motor.elecpow.map(1,:,:)= [0 0 0;0 34000 68000;0 68000 136000;];
dat.elecpowmap.motor.elecpow.map(2,:,:)= [0 0 0;0 34000 68000;0 68000 136000;];
dat.elecpowmap.motor.elecpow.map(3,:,:)= [0 0 0;0 34000 68000;0 68000 136000;];

dat.elecpowmap.generator.voltage.Comment = 'Index vector - electric machine voltage';
dat.elecpowmap.generator.voltage.Unit = 'V';
dat.elecpowmap.generator.voltage.vec = [0 400 800];
dat.elecpowmap.generator.speed.Comment = 'Index vector - electric machine speed';
dat.elecpowmap.generator.speed.Unit = 'rad/s';
dat.elecpowmap.generator.speed.vec = [0.0 4500.0 9000.0]*2*pi/60;
dat.elecpowmap.generator.torque.Comment = 'Index vector - electric machine torque';
dat.elecpowmap.generator.torque.Unit = 'Nm';
dat.elecpowmap.generator.torque.vec = [-120  -60   0];
dat.elecpowmap.generator.elecpow.Comment = 'electric power map for generating';
dat.elecpowmap.generator.elecpow.Unit = 'W';
dat.elecpowmap.generator.elecpow.map(1,:,:)=[0 -45000 -90000;0 -23000 -45000;0 0 0;];
dat.elecpowmap.generator.elecpow.map(2,:,:)=[0 -45000 -90000;0 -23000 -45000;0 0 0;];
dat.elecpowmap.generator.elecpow.map(3,:,:)=[0 -45000 -90000;0 -23000 -45000;0 0 0;];


%--------------------------------------------------------------------------
% Thermal data (optional, leave unmodified if not needed)
%--------------------------------------------------------------------------
dat.Rth.comment = 'thermal resistance to the cooling fluid';
dat.Rth.unit = 'K/W';
dat.Rth.value = 0.01;

dat.cm.comment = 'thermal capacity';
dat.cm.unit = 'J / K';
dat.cm.value = 125e3;

dat.coolingFluid.comment = 'properties of the cooling fluid';
dat.coolingFluid.c.comment = 'specific thermal capacity';
dat.coolingFluid.c.unit = 'J / (kg K)';
dat.coolingFluid.c.value = 4190;

dat.coolingFluid.flowTemp.comment = 'global temperature of cooling fluid downstream heat exchanger to ambient air';
dat.coolingFluid.flowTemp.unit = 'K';
dat.coolingFluid.flowTemp.value = 273 + 25;

dat.alpha.comment = 'weighting factor between inlet and outlet temperature of cooling fluid for calculating the heat flow from cooling fluid to point mass';
dat.alpha.unit = '-';
dat.alpha.value =0.5;

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = 'Omega_ref                   (Requested Electric Machine Speed)';
con_info(1).unit = '[rad/s]';
con_info(2).descr = '                                       (Switch Speed/Torque control)';
con_info(2).unit = '[Boolean]';
con_info(3).descr = 'M_em,des                    (Requested Electric Machine Torque)';
con_info(3).unit = '[Nm]';
con_info(4).descr = '                                       (Electric Machine Actual Cooling Flow)';
con_info(4).unit = '[kg/s]';
con_info(5).descr = 'u                                     (Electric Machine Voltage Input)';
con_info(5).unit = '[V]';
con_info(6).descr = 'Omega_em                  (Electric Machine Rotational Speed Input)';
con_info(6).unit = '[rad/s]';


%%%%%%%%%%%
%revision history
% 
