%   Filename:  para_chassis.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'Chassis model data';
dat.filename = 'para_chassis.m';
dat.version = '1.1';
dat.lastModified = '18.04.2013';
dat.modifiedBy = 'Jonas Fredriksson';

dat.vehicle.mass.comment = 'vehicle mass';
dat.vehicle.mass.unit = 'kg';
dat.vehicle.mass.value = 6000;

dat.brakeactuator.timeconstant.comment = 'brake actuator time constant';
dat.brakeactuator.timeconstant.unit = 's';
dat.brakeactuator.timeconstant.value = 0.1;

dat.aero.comment = 'aerodynamical load parameters';
dat.aero.af.comment = 'frontal area';
dat.aero.af.unit = 'm^2';
dat.aero.af.value = 4;
dat.aero.cd.comment = 'aerodynamical drag coefficient';
dat.aero.cd.unit = '-';
dat.aero.cd.value = 0.432;

dat.wheel.comment = 'wheel parameters';
dat.wheel.radius.comment = 'wheel radius';
dat.wheel.radius.unit = 'm';
dat.wheel.radius.value = 0.4;
dat.wheel.inertia.comment = 'wheel inertia';
dat.wheel.inertia.unit = 'kgm^2';
dat.wheel.inertia.value = 24;
dat.wheel.rollingres.comment = 'rolling resistance coefficient';
dat.wheel.rollingres.unit = '-';
dat.wheel.rollingres.value = 0.006;

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = 'M_brake               (Requested Chassis Brake Torque)';
con_info(1).unit = '[Nm]';
con_info(2).descr = 'M_drive                (Chassis Torque Input)';
con_info(2).unit = '[Nm]';
con_info(3).descr = 'J_powertrain         (Chassis Inertia Input)';
con_info(3).unit = '[kgm^2]';
con_info(4).descr = '                           (Drivecycle Slope)';
con_info(4).unit = '[rad]';
%%%%%%%%%%%
%revision history
% 
