%   Filename:  para_mechanical_aux_system.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'Auxiliary system modeling data';
dat.filename = 'para_mech_auxiliary.m';
dat.version = '1';
dat.lastModified = '26.02.2014';
dat.modifiedBy = 'Jonas Fredriksson';

dat.inertia.comment = 'inertia of mechanical auxiliary';
dat.inertia.unit = 'kgm^2';
dat.inertia.value = 0.1;

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = 'Requested Power';
con_info(1).unit = '[W]';
con_info(2).descr = 'Rotational Speed';
con_info(2).unit = '[rad/s]';

%%%%%%%%%%%
%revision history
% 
