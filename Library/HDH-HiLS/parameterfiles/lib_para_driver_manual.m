%   Filename:  para_driver_manual.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================
%-------------------------------------------------------------------------------------------------------------------
% Gearshift Logics
%-------------------------------------------------------------------------------------------------------------------

dat.vecto.comment = 'Gear shift logics for manual transmissions';
dat.vecto.filename = 'para_gearshift_manual_example1.m';
dat.vecto.version = '3';
dat.vecto.lastModified = '12.05.2014';
dat.vecto.modifiedBy = 'Gerard Silberholz';

% THE FOLLOWING PARAMETERS HAVE TO BE SET TO THE SAME VALUES AS USED IN THE
% DRIVER_MANUAL PARAMETER FILE FOR CERTIFICATION

% -------------------------------------------------------------------------
% Driver_manual parameters
% -------------------------------------------------------------------------
dat.vecto.clutchtime.comment = 'time from start of pressing the clutch pedal to start of releasing the clutch pedal';
dat.vecto.clutchtime.unit = 's';
dat.vecto.clutchtime.value = 0.5;

% THE FOLLOWING PARAMETERS HAVE TO BE SET TO THE SAME VALUES AS USED IN THE
% CHASSIS PARAMETER FILE FOR CERTIFICATION

% -------------------------------------------------------------------------
% Vehicle parameters
% -------------------------------------------------------------------------
dat.vecto.vehicle.mass.comment = 'vehicle mass';
dat.vecto.vehicle.mass.unit = 'kg';
dat.vecto.vehicle.mass.value = 6000.0;

%--------------------------------------------------------------------------
% Wheel parameters
%--------------------------------------------------------------------------
dat.vecto.wheel.comment = 'wheel parameters';
dat.vecto.wheel.radius.comment = 'wheel radius';
dat.vecto.wheel.radius.unit = 'm';
dat.vecto.wheel.radius.value = 0.4;
dat.vecto.wheel.inertia.comment = 'wheel inertia';
dat.vecto.wheel.inertia.unit = 'kgm^2';
dat.vecto.wheel.inertia.value = 24.00;
dat.vecto.wheel.rollingres.comment = 'rolling resistance coefficient';
dat.vecto.wheel.rollingres.unit = '-';
dat.vecto.wheel.rollingres.value = 0.006;

%--------------------------------------------------------------------------
% Aerodynamical vehicle properties
%--------------------------------------------------------------------------
dat.vecto.aero.comment = 'aerodynamical load parameters';
dat.vecto.aero.af.comment = 'frontal area';
dat.vecto.aero.af.unit = 'm^2';
dat.vecto.aero.af.value = 4;
dat.vecto.aero.cd.comment = 'aerodynamical drag coefficient';
dat.vecto.aero.cd.unit = '-';
dat.vecto.aero.cd.value = 0.432;

% THE FOLLOWING PARAMETERS HAVE TO BE SET TO THE SAME VALUES AS USED IN THE
% FINAL GEAR MODEL PARAMETER FILE FOR CERTIFICATION

%--------------------------------------------------------------------------
% Final gear parameters
%--------------------------------------------------------------------------
dat.vecto.fg.comment = 'final gear parameters';
dat.vecto.fg.ratio.comment = 'final gear ratio';
dat.vecto.fg.ratio.unit = '-';
dat.vecto.fg.ratio.value = 4.9;

% THE FOLLOWING PARAMETERS HAVE TO BE SET TO THE SAME VALUES AS USED IN THE
% TRANSMISSION PARAMETER FILE FOR CERTIFICATION

%--------------------------------------------------------------------------
% Gear parameters
%--------------------------------------------------------------------------
dat.vecto.gear.number.comment = 'Gear number';
dat.vecto.gear.number.unit = '-';
dat.vecto.gear.number.vec = [0 1 2 3 4 5 6];
dat.vecto.gear.ratio.comment = 'Gear ratio';
dat.vecto.gear.ratio.unit = '-';
dat.vecto.gear.ratio.vec = [0 6.7 4 2.3 1.4 1 0.75];
dat.vecto.gear.efficiency.comment = 'Gear efficiency';
dat.vecto.gear.efficiency.unit = '-';
dat.vecto.gear.efficiency.vec = [0 0.95 0.95 0.95 0.95 0.95 0.95];


% THE FOLLOWING PARAMETERS HAVE TO BE SET TO THE SAME VALUES AS USED IN THE
% ENGINE PARAMETER FILE FOR CERTIFICATION

%--------------------------------------------------------------------------
% ICE parameters
%--------------------------------------------------------------------------
dat.vecto.ICE.maxtorque_speed.comment = 'ICE full load speed';
dat.vecto.ICE.maxtorque_speed.unit = 'rad/s';
dat.vecto.ICE.maxtorque_speed.vec = [570.0  800.0 1400.0 3000.0 3200.0 3400.0]*2*pi/60;

dat.vecto.ICE.maxtorque_torque.comment = 'ICE full load torque';
dat.vecto.ICE.maxtorque_torque.unit = 'Nm';
dat.vecto.ICE.maxtorque_torque.vec = [380 540 580 630 540 155];

dat.vecto.ICE.maxtorque_torque.comment = 'ICE friction torque';
dat.vecto.ICE.maxtorque_torque.unit = 'Nm';
dat.vecto.ICE.maxtorque_friction.vec = [-72  -75  -82 -145 -154 -154];

dat.vecto.ICE.ratedspeed.comment = 'ICE rated speed';
dat.vecto.ICE.ratedspeed.unit = 'rad/s';
dat.vecto.ICE.ratedspeed.value= 3000*2*pi/60;


% THE FOLLOWING PARAMETERS HAVE TO BE SET ACCORDING TO THE
% DOCUMENTATION/REGULATION

%--------------------------------------------------------------------------
% gear shift polygon and behaviour definition
%--------------------------------------------------------------------------
dat.vecto.downshift_torque.comment = 'downshift_torque';
dat.vecto.downshift_torque.unit = 'Nm';
dat.vecto.downshift_torque.vec = [-155 191 630];

dat.vecto.downshift_speed.comment = 'downshift_speed';
dat.vecto.downshift_speed.unit = 'rad/s';
dat.vecto.downshift_speed.vec = [748 748 1815]*2*pi/60;

dat.vecto.upshift_torque.comment = 'upshift_torque';
dat.vecto.upshift_torque.unit = 'Nm';
dat.vecto.upshift_torque.vec = [-155 383 630];

dat.vecto.upshift_speed.comment = 'upshift_speed';
dat.vecto.upshift_speed.unit = 'rad/s';
dat.vecto.upshift_speed.vec = [2122 2122 3122]*2*pi/60;

dat.vecto.skipgears.comment = 'skip gears when upshifting active or not, 0=skip gears not active, 1=skip gears active';
dat.vecto.skipgears.unit = '-';
dat.vecto.skipgears.value = 0;

dat.vecto.startgearengaged.comment = 'start gear is engaged specified time before drive away (testcycle speed changes from 0 to bigger than 0), align with dat.clutchtime.driveaway.value at para_driver_manual.m';
dat.vecto.startgearengaged.unit = 's';
dat.vecto.startgearengaged.value = 1.5;

dat.vecto.startgearactive.comment = 'calculation of highest possible startgear active or not, 0=calculation not active, 1=calculation active';
dat.vecto.startgearactive.unit = '-';
dat.vecto.startgearactive.value = 1;

%  ========================================================================

dat.comment = 'Driver Model parameter and gear shift logics';
dat.filename = 'para_driver_manual_example1.m';
dat.version = '1';
dat.lastModified = '20.11.2013';
dat.modifiedBy = 'Gerard Silberholz';

dat.clutchtime.comment = 'time from start of pressing the clutch pedal to start of releasing the clutch pedal';
dat.clutchtime.unit = 's';
dat.clutchtime.value = 0.5;

dat.clutchtime.open.comment = 'time constant for pressing the clutch pedal';
dat.clutchtime.open.unit = '-';
dat.clutchtime.open.value = 0.01;

dat.clutchtime.close.comment = 'time constant for releasing the clutch pedal';
dat.clutchtime.close.unit = '-';
dat.clutchtime.close.value = 0.02;

dat.clutchtime.driveaway.comment = 'time from start of releasing the fully pressed clutch pedal to fully released clutch pedal at drive away';
dat.clutchtime.driveaway.unit = 's';
dat.clutchtime.driveaway.value = 3;

dat.controller.comment = 'driver pid controller for vehicle speed control';
dat.controller.p.comment = 'proportional constant';
dat.controller.p.unit = '-';
dat.controller.p.value = 1.2;
dat.controller.i.comment = 'integral constant';
dat.controller.i.unit = '-';
dat.controller.i.value = 0.8;
dat.controller.d.comment = 'derivative constant';
dat.controller.d.unit = '-';
dat.controller.d.value = 0;
dat.controller.k.comment = 'anti wind up constant for integrator if speed cannot be tracked even though accelerator pedal is fully pressed';
dat.controller.k.unit = '-';
dat.controller.k.value = 1;

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = 'V_vehicle      (Actual vehicle velocity)';
con_info(1).unit = '[m/s]';
con_info(2).descr = 'Omega_in     (Transmission Input Speed)';
con_info(2).unit = '[rad/s]';
con_info(3).descr = '                       (Transmission Actual Gear Number)';
con_info(3).unit = '[-]';
con_info(4).descr = '                       (Clutch dissenggaged or not)';
con_info(4).unit = '[Boolean]';
con_info(5).descr = '                       (Accelerator Pedal Rate)';
con_info(5).unit = '[-]';
con_info(6).descr = '                       (Drivecycle Time and Speed)';
con_info(6).unit = '[-]';

%%%%%%%%%%%
%revision history
% 
