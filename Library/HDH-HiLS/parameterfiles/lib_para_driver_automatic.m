%   Filename:  para_driver_automatic.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'Driver Model parameter';
dat.filename = 'para_driver_automatic.m';
dat.version = '1';
dat.lastModified = '20.11.2013';
dat.modifiedBy = 'Gerard Silberholz';

dat.controller.comment = 'driver pid controller for vehicle speed control';
dat.controller.p.comment = 'proportional constant';
dat.controller.p.unit = '-';
dat.controller.p.value = 0.3;
dat.controller.i.comment = 'integral constant';
dat.controller.i.unit = '-';
dat.controller.i.value = 0.08;
dat.controller.d.comment = 'derivative constant';
dat.controller.d.unit = '-';
dat.controller.d.value = 0;
dat.controller.k.comment = 'anti wind up constant for integrator if speed cannot be tracked even though accelerator pedal is fully pressed';
dat.controller.k.unit = '-';
dat.controller.k.value = 1;

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = 'V_vehicle      (Actual vehicle velocity)';
con_info(1).unit = '[m/s]';
con_info(2).descr = '                    (Drivecycle Time and Speed)';
con_info(2).unit = '[-]';


%%%%%%%%%%%
%revision history
% 
