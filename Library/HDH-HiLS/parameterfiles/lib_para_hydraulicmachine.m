%   Filename:  para_hydraulicmachine.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'hydraulic machine data';
dat.filename = 'para_hydraulicmachine.m';
dat.version = '1';
dat.lastModified = '22.05.2013';
dat.modifiedBy = 'Jonas Fredriksson';

dat.timeconstant.comment = 'machine time constant';
dat.timeconstant.unit = '-';
dat.timeconstant.value = 0.1;

dat.inertia.comment = 'machine inertia';
dat.inertia.unit = 'kgm^2';
dat.inertia.value = 0.5;

dat.displacement.comment = 'displacement volume';
dat.displacement.unit = 'm^3';
dat.displacement.value = 0.00210;

dat.volefficiency.comment = 'volumetric efficiency = f(x, pressure, speed)';
dat.volefficiency.x.comment = 'Index vector - ctrl';
dat.volefficiency.x.unit = '-';
dat.volefficiency.x.vec = [-1 0 1];
dat.volefficiency.pressure.comment = 'Index vector - pressure difference accumulator/reservior';
dat.volefficiency.pressure.unit = 'Pa';
dat.volefficiency.pressure.vec = [0 400000000];
dat.volefficiency.speed.comment = 'Index vector - speed';
dat.volefficiency.speed.unit = 'rad/s';
dat.volefficiency.speed.vec = [0 600];
dat.volefficiency.efficiency.comment = 'volumetric efficiency map';
dat.volefficiency.efficiency.unit = '-';
dat.volefficiency.efficiency.map(:,:,1) = [0.9 0.9; 0.9 0.9];
dat.volefficiency.efficiency.map(:,:,2) = [0.9 0.9; 0.9 0.9];
dat.volefficiency.efficiency.map(:,:,3) = [1/0.9 1/0.9; 1/0.9 1/0.9];

dat.mechefficiency.comment = 'mechanical efficiency = f(x, pressure, speed)';
dat.mechefficiency.x.comment = 'Index vector - ctrl';
dat.mechefficiency.x.unit = '-';
dat.mechefficiency.x.vec = [-1 0 1];
dat.mechefficiency.pressure.comment = 'Index vector - pressure';
dat.mechefficiency.pressure.unit = 'Pa';
dat.mechefficiency.pressure.vec = [0 400000000];
dat.mechefficiency.speed.comment = 'Index vector - speed';
dat.mechefficiency.speed.unit = 'rad/s';
dat.mechefficiency.speed.vec = [0 600];
dat.mechefficiency.efficiency.comment = 'mechanical efficiency map';
dat.mechefficiency.efficiency.unit = '-';
dat.mechefficiency.efficiency.map(:,:,1) = [1/0.9 1/0.9; 1/0.9 1/0.9];
dat.mechefficiency.efficiency.map(:,:,2) = [1/0.9 1/0.9; 1/0.9 1/0.9];
dat.mechefficiency.efficiency.map(:,:,3) = [0.9 0.9; 0.9 0.9];

dat.controller.comment = 'Local PID-controller parameters';
dat.controller.p.comment = 'Proportional constant';
dat.controller.p.unit = '-';
dat.controller.p.value = 0.01;
dat.controller.i.comment = 'integral constant';
dat.controller.i.unit = '-';
dat.controller.i.value = 0.004;

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = '                           (Requested Hydraulic Machine Speed)';
con_info(1).unit = '[rad/s]';
con_info(2).descr = '                           (Switch - Speed / Torque)';
con_info(2).unit = '[Boolean]';
con_info(3).descr = '                           (Hydraulic Machine Requested Torque)';
con_info(3).unit = '[Nm]';
con_info(4).descr = 'P_acc                (Hydraulic Pressure in (Accumulator)';
con_info(4).unit = '[Pa]';
con_info(5).descr = 'P_res                 (Hydraulic Pressure in (Reservoir/sump)';
con_info(5).unit = '[Pa]';
con_info(6).descr = 'Omega_pm       (Hydraulic Machine Rotational Speed Input)';
con_info(6).unit = '[rad/s]';

%%%%%%%%%%%
%revision history
% 
