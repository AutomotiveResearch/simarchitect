%   Filename:  para_battery.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'battery data';
dat.filename = 'para_battery.m';
dat.version = '1';
dat.lastModified = '28.02.2014';
dat.modifiedFrom = 'Gerard Silberholz';

dat.ns.comment = 'number of cells connected in series';
dat.ns.unit = '-';
dat.ns.value = 94;

dat.np.comment = 'number of cells connected in parallel';
dat.np.unit = '-';
dat.np.value = 4;

dat.capacity.comment = 'cell capacity';
dat.capacity.unit = 'Ah';
dat.capacity.value = 2.5;

dat.initialSOC.comment = 'initial state of charge (as % of cell capacity)';
dat.initialSOC.unit = '%';
dat.initialSOC.value = 60;

dat.ocv.comment = 'open circuit voltage = f(state of charge)';
dat.ocv.soc.Comment = 'Index vector - state of charge';
dat.ocv.soc.Unit = '%';
dat.ocv.soc.vec = [0 10 90 100];
dat.ocv.ocv.Comment = 'open circuit voltage';
dat.ocv.ocv.Unit = 'V';
dat.ocv.ocv.vec = [2.9 3.2 3.3 3.5];

dat.resi.discharge.comment = 'discharge R0, R and C = f(state of charge)';
dat.resi.discharge.soc.Comment = 'Index vector - state of charge';
dat.resi.discharge.soc.Unit = '%';
dat.resi.discharge.soc.vec = [0 100];

dat.resi.discharge.R0.Comment = 'R0';
dat.resi.discharge.R0.Unit = 'ohm';
dat.resi.discharge.R0.vec = [0.008 0.008];

dat.resi.discharge.R.Comment = 'R';
dat.resi.discharge.R.Unit = 'ohm';
dat.resi.discharge.R.vec = [0.01 0.01];

dat.resi.discharge.C.Comment = 'C';
dat.resi.discharge.C.Unit = 'F';
dat.resi.discharge.C.vec = [1300 1300];

dat.resi.charge.comment = 'charge R0, R and C = f(state of charge)';
dat.resi.charge.soc.Comment = 'Index vector - state of charge';
dat.resi.charge.soc.Unit = '%';
dat.resi.charge.soc.vec = [0 100];

dat.resi.charge.R0.Comment = 'R0';
dat.resi.charge.R0.Unit = 'ohm';
dat.resi.charge.R0.vec = [0.008 0.008];

dat.resi.charge.R.Comment = 'R';
dat.resi.charge.R.Unit = 'ohm';
dat.resi.charge.R.vec = [0.01 0.01];

dat.resi.charge.C.Comment = 'C';
dat.resi.charge.C.Unit = 'F';
dat.resi.charge.C.vec = [1300 1300];


%--------------------------------------------------------------------------
% Thermal data (optional, leave unmodified if not needed)
%--------------------------------------------------------------------------
dat.Rth.comment = 'thermal resistance to the cooling fluid';
dat.Rth.unit = 'K/W';
dat.Rth.value = 0.01;

dat.cm.comment = 'thermal capacity';
dat.cm.unit = 'J / K';
dat.cm.value = 125e3;

dat.coolingFluid.comment = 'properties of the cooling fluid';
dat.coolingFluid.c.comment = 'specific thermal capacity';
dat.coolingFluid.c.unit = 'J / (kg K)';
dat.coolingFluid.c.value = 4190;

dat.coolingFluid.flowTemp.comment = 'global temperature of cooling fluid downstream heat exchanger to ambient air';
dat.coolingFluid.flowTemp.unit = 'K';
dat.coolingFluid.flowTemp.value = 273 + 25;

dat.alpha.comment = 'weighting factor between inlet and outlet temperature of cooling fluid for calculating the heat flow from cooling fluid to point mass';
dat.alpha.unit = '-';
dat.alpha.value =0.5;

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = 'i      (Battery Current Input)';
con_info(1).unit = '[A]';
con_info(2).descr = '       (Battery Actual Cooling Flow)';
con_info(2).unit = '[kg/s]';


%%%%%%%%%%%
%revision history
% 
