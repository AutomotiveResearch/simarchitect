%   Filename    :  parameter_main.m
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================
%  ============================================================
%  Drivecycle
%  ============================================================
load WHVC 

dat.comment = 'WHVC, road gradients have to be set according to the rated power of the hybrid system at WHVC.mat';
dat.time.comment = 'run time';
dat.time.unit = 'sec';
dat.time.vec = drivecycle(:,1);

dat.speed.comment = 'desired vehicle speed';
dat.speed.unit = 'm/s';
dat.speed.vec = drivecycle(:,2);

dat.slope.comment ='road gradient';
dat.slope.unit = 'rad';
dat.slope.vec = atan(drivecycle(:,3)./100);

clear drivecycle

%  ============================================================
%  Surrounding (environment) conditions
%  ============================================================
hilsmdl.init.comment = 'global initialization data';
hilsmdl.init.temp.comment = 'temperature';
hilsmdl.init.temp.unit = 'K';
hilsmdl.init.temp.value = 273 + 20;

hilsmdl.constants.ambientTemp.comment = 'global temperature of ambient air';
hilsmdl.constants.ambientTemp.unit = 'K';
hilsmdl.constants.ambientTemp.value = 273 + 20;

hilsmdl.constants.ambientPressure.comment = 'global atmospheric pressure';
hilsmdl.constants.ambientPressure.unit = 'Pa';
hilsmdl.constants.ambientPressure.value = 101300;

hilsmdl.constants.g.comment = 'gravitational constant';
hilsmdl.constants.g.unit = 'm/s^2';
hilsmdl.constants.g.value =  9.80665;

hilsmdl.constants.airdensity.comment = 'air density';
hilsmdl.constants.airdensity.unit = 'kg/m^3';
hilsmdl.constants.airdensity.value = 1.17;