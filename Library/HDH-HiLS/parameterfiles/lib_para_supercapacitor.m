%   Filename:  para_supercapacitor.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'pseudo supercap data - like a Maxwell one, 37F, 330V';
dat.filename = 'para_supercapacitor.m';
dat.version = '2';
dat.lastModified = '28.02.2014';
dat.modifiedFrom = 'Gerard Silberholz';

dat.ns.comment = 'number of cells connected in series';
dat.ns.unit = '-';
dat.ns.value = 122;

dat.np.comment = 'number of cells connected in parallel';
dat.np.unit = '-';
dat.np.value = 3;

dat.C.comment = 'capacity of cell';
dat.C.unit = 'F';
dat.C.value = 1500; 

dat.R.comment = 'internal resistance of cell';
dat.R.unit = 'Ohm';
dat.R.value = 0.47e-3;

dat.initialVoltage.comment = 'initial voltage of cell';
dat.initialVoltage.unit = 'V';
dat.initialVoltage.value = 2.16;

dat.Vmin.comment = 'minimum cell voltage';
dat.Vmin.unit = 'V';
dat.Vmin.value = 0;

dat.Vmax.comment = 'maximum cell voltage';
dat.Vmax.unit = 'V';
dat.Vmax.value = 2.7;

%--------------------------------------------------------------------------
% Thermal data (optional, leave unmodified if not needed)
%--------------------------------------------------------------------------
dat.Rth.comment = 'thermal resistance to the cooling fluid';
dat.Rth.unit = 'K/W';
dat.Rth.value = 0.01;

dat.cm.comment = 'thermal capacity';
dat.cm.unit = 'J / K';
dat.cm.value = 125e3;

dat.coolingFluid.comment = 'properties of the cooling fluid';
dat.coolingFluid.c.comment = 'specific thermal capacity';
dat.coolingFluid.c.unit = 'J / (kg K)';
dat.coolingFluid.c.value = 4190;

dat.coolingFluid.flowTemp.comment = 'global temperature of cooling fluid downstream heat exchanger to ambient air';
dat.coolingFluid.flowTemp.unit = 'K';
dat.coolingFluid.flowTemp.value = 273 + 25;

dat.alpha.comment = 'weighting factor between inlet and outlet temperature of cooling fluid for calculating the heat flow from cooling fluid to point mass';
dat.alpha.unit = '-';
dat.alpha.value =0.5;

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = '           (Super Capacitor Actual Cooling Flow)';
con_info(1).unit = '[kg/s]';
con_info(2).descr = 'i          (Super Capacitor Current Input)';
con_info(2).unit = '[A]';

%%%%%%%%%%%
%revision history
% 
