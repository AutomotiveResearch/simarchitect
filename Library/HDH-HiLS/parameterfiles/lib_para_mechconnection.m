%   Filename:  para_mechconnection.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'Mechanical connection modeling data';
dat.filename = 'para_mechconnection.m';
dat.version = '1';
dat.lastModified = '18.04.2013';
dat.modifiedBy = 'Jonas Fredriksson';

dat.in1.comment = 'input gear 1';
dat.in1.ratio.comment = 'Gear ratio for input 1';
dat.in1.ratio.unit = '-';
dat.in1.ratio.value = 1;
dat.in1.inertia.comment = 'inertia';
dat.in1.inertia.unit = '-';
dat.in1.inertia.value = 0.05;
dat.in1.mechefficiency.comment = 'efficiency map = f(torque, speed)';
dat.in1.mechefficiency.torque.Comment = 'Index vector - torque';
dat.in1.mechefficiency.torque.unit = 'Nm';
dat.in1.mechefficiency.torque.vec = [0 20000];
dat.in1.mechefficiency.speed.Comment = 'Index vector - speed';
dat.in1.mechefficiency.speed.unit = 'rad/s';
dat.in1.mechefficiency.speed.vec = [0 1000];
dat.in1.mechefficiency.efficiency.Comment = 'efficiency map';
dat.in1.mechefficiency.efficiency.unit = '-';
dat.in1.mechefficiency.efficiency.map = [1 1; 1 1];

dat.in2.comment = 'input gear 2';
dat.in2.ratio.comment = 'Gear ratio for input 2';
dat.in2.ratio.unit = '-';
dat.in2.ratio.value = 2.88;
dat.in2.inertia.comment = 'inertia';
dat.in2.inertia.unit = '-';
dat.in2.inertia.value = 0.05;
dat.in2.mechefficiency.comment = 'efficiency map = f(torque, speed)';
dat.in2.mechefficiency.torque.Comment = 'Index vector - torque';
dat.in2.mechefficiency.torque.unit = 'Nm';
dat.in2.mechefficiency.torque.vec = [0 20000];
dat.in2.mechefficiency.speed.Comment = 'Index vector - speed';
dat.in2.mechefficiency.speed.unit = 'rad/s';
dat.in2.mechefficiency.speed.vec = [0 1000];
dat.in2.mechefficiency.efficiency.Comment = 'efficiency map';
dat.in2.mechefficiency.efficiency.unit = '-';
dat.in2.mechefficiency.efficiency.map = [1 1; 1 1];

dat.out.comment = 'output gear';
dat.out.ratio.comment = 'Gear ratio for output';
dat.out.ratio.unit = '-';
dat.out.ratio.value = 1;
dat.out.inertia.comment = 'inertia';
dat.out.inertia.unit = '-';
dat.out.inertia.value = 0.05;
dat.out.mechefficiency.comment = 'efficiency map = f(torque, speed)';
dat.out.mechefficiency.torque.Comment = 'Index vector - torque';
dat.out.mechefficiency.torque.unit = 'Nm';
dat.out.mechefficiency.torque.vec = [0 20000];
dat.out.mechefficiency.speed.Comment = 'Index vector - speed';
dat.out.mechefficiency.speed.unit = 'rad/s';
dat.out.mechefficiency.speed.vec = [0 1000];
dat.out.mechefficiency.efficiency.Comment = 'efficiency map';
dat.out.mechefficiency.efficiency.unit = '-';
dat.out.mechefficiency.efficiency.map = [1 1; 1 1];

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = 'M_in,1          (Mech Connection Torque Input 1)';
con_info(1).unit = '[Nm]';
con_info(2).descr = 'J_in,1           (Mech Connection Inertia Input 1)';
con_info(2).unit = '[kg*m^2]';
con_info(3).descr = 'M_in,2          (Mech Connection Torque Input 2)';
con_info(3).unit = '[Nm]';
con_info(4).descr = 'J_in,2           (Mech Connection Inertia Input 2)';
con_info(4).unit = '[kg*m^2]';
con_info(5).descr = 'Omega_in   (Mech Connection Feedback Input)';
con_info(5).unit = '[rad/s]';

%%%%%%%%%%%
%revision history
% 
