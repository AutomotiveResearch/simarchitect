%   Filename:  para_transmission.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'Transmission model data';
dat.filename = 'para_transmission.m';
dat.version = '2';
dat.lastModified = '01.10.2013';
dat.modifiedBy = 'Jonas Fredriksson';


dat.timetrqinterrupt.comment = 'Time the transmission is torque free';
dat.timetrqinterrupt.unit = 's';
dat.timetrqinterrupt.value = 0.25;

dat.maxtorque.comment = 'The maximum torque the transmission can transfer (Torque interrupt model)';
dat.maxtorque.unit = 'Nm';
dat.maxtorque.value = 700;

dat.tanh.comment = 'Coefficient for smoothing the function in the clutch model using a tanh function';
dat.tanh.unit = '-';
dat.tanh.value = 3;

dat.speedtolerance.comment = 'Speed tolerance for slipping clutch';
dat.speedtolerance.unit = 'rad/s';
dat.speedtolerance.value = 3;

dat.nofgear.comment = 'The number of gears in the transmission (forward)';
dat.nofgear.unit = '-';
dat.nofgear.value = 6;

dat.gear.comment = 'Gear parameters';
dat.gear.number.comment = 'Index vector - Gear number';
dat.gear.number.unit = '-';
dat.gear.number.vec = [0 1 2 3 4 5 6];
dat.gear.ratio.comment = 'Gear ratio';
dat.gear.ratio.unit = '-';
dat.gear.ratio.vec = [0 6.7 4 2.3 1.4 1 0.75];
dat.gear.inertia.comment = 'Gear inertia';
dat.gear.inertia.unit = 'kgm^2';
dat.gear.inertia.vec = [0 0.37 0.27 0.17 0.13 0.11 0.10];

dat.gear.mechefficiency.comment = 'efficiency map = f(gear, torque, speed)';
dat.gear.mechefficiency.torque.Comment = 'Index vector - torque';
dat.gear.mechefficiency.torque.unit = 'Nm';
dat.gear.mechefficiency.torque.vec = [0 20000];
dat.gear.mechefficiency.speed.Comment = 'Index vector - speed';
dat.gear.mechefficiency.speed.unit = 'rad/s';
dat.gear.mechefficiency.speed.vec = [0 1000];
dat.gear.mechefficiency.efficiency.Comment = 'efficiency map';
dat.gear.mechefficiency.efficiency.unit = '-';
dat.gear.mechefficiency.efficiency.map(1,:,:)=[0 0; 0 0];
dat.gear.mechefficiency.efficiency.map(2,:,:)=[0.95 0.95; 0.95 0.95];
dat.gear.mechefficiency.efficiency.map(3,:,:)=[0.95 0.95; 0.95 0.95];
dat.gear.mechefficiency.efficiency.map(4,:,:)=[0.95 0.95; 0.95 0.95];
dat.gear.mechefficiency.efficiency.map(5,:,:)=[0.95 0.95; 0.95 0.95];
dat.gear.mechefficiency.efficiency.map(6,:,:)=[0.95 0.95; 0.95 0.95];
dat.gear.mechefficiency.efficiency.map(7,:,:)=[0.95 0.95; 0.95 0.95];

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = '                       (Requested Gear Number)';
con_info(1).unit = '[-]';
con_info(2).descr = 'M_in              (Transmission Torque Input)';
con_info(2).unit = '[Nm]';
con_info(3).descr = 'J_in               (Transmission Inertia Input)';
con_info(3).unit = '[kg*m^2]';
con_info(4).descr = 'Omega_out  (Transmission Rotational Speed Input)';
con_info(4).unit = '[rad/s]';

%%%%%%%%%%%
%revision history
% 
