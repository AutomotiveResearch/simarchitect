%   Filename:  para_spurgear.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'Spur gear modeling data';
dat.filename = 'para_spurgear.m';
dat.version = '1';
dat.lastModified = '28.02.2014';
dat.modifiedBy = 'Christoph Six';


dat.ratio.comment = 'Gear ratio';
dat.ratio.unit = '-';
dat.ratio.value = 1;

dat.inertia.comment = 'inertia';
dat.inertia.unit = '-';
dat.inertia.value = 0.1;

dat.mechefficiency.comment = 'efficiency map = f(torque, speed)';
dat.mechefficiency.torque.Comment = 'Index vector - torque';
dat.mechefficiency.torque.unit = 'Nm';
dat.mechefficiency.torque.vec = [0 20000];
dat.mechefficiency.speed.Comment = 'Index vector - speed';
dat.mechefficiency.speed.unit = 'rad/s';
dat.mechefficiency.speed.vec = [0 1000];
dat.mechefficiency.efficiency.Comment = 'efficiency map';
dat.mechefficiency.efficiency.unit = '-';
dat.mechefficiency.efficiency.map = [0.95 0.95; 0.95 0.95];

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = 'M_in             (Spur Gear Torque Input)';
con_info(1).unit = '[Nm]';
con_info(2).descr = 'J_in              (Spur Gear Inertia Input)';
con_info(2).unit = '[kg*m^2]';
con_info(3).descr = 'Omega_in   (Spur Gear Rotational Speed Input)';
con_info(3).unit = '[rad/s]';

%%%%%%%%%%%
%revision history
% 
