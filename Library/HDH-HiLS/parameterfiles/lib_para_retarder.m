%   Filename:  para_retarder.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'Retarder data';
dat.filename = 'para_retarder.m';
dat.version = '1';
dat.lastModified = '10.06.2013';
dat.modifiedBy = 'Jonas Fredriksson';

dat.inertia.comment = 'Retarder inertia';
dat.inertia.unit = 'kgm^2';
dat.inertia.value = 0.001;

dat.braketorque.comment = 'retarder maximum brake torque map';
dat.braketorque.speed.comment = 'Index vector - speed';
dat.braketorque.speed.unit = 'rad/s';
dat.braketorque.speed.vec = [0 1000];
dat.braketorque.torque.comment = 'maximum brake torque vector';
dat.braketorque.torque.unit = 'Nm';
dat.braketorque.torque.vec = [0 -100];

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = '                     (Retarder ON / OFF)';
con_info(1).unit = '[Boolean]';
con_info(2).descr = 'M_in             (Retarder Torque Input)';
con_info(2).unit = '[Nm]';
con_info(3).descr = 'J_in              (Retarder Inertia Input)';
con_info(3).unit = '[kg*m^2]';
con_info(4).descr = 'Omega_in   (Retarder Rotational Speed Input)';
con_info(4).unit = '[rad/s]';

%%%%%%%%%%%
%revision history
% 
