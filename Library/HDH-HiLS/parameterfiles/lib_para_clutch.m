%   Filename:  para_clutch.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'Clutch model data';
dat.filename = 'para_clutch.m';
dat.version = '2';
dat.lastModified = '01.10.2013';
dat.modifiedBy = 'Jonas Fredriksson';

dat.in.inertia.comment = 'clutch plate inertia for the input side clutch plate';
dat.in.inertia.unit = 'kgm^2';
dat.in.inertia.value = 0.001;

dat.out.inertia.comment = 'clutch plate inertia for the output side clutch plate';
dat.out.inertia.unit = 'kgm^2';
dat.out.inertia.value = 0.001;

dat.maxtorque.comment = 'Maximum torque transmitted through the clutch ';
dat.maxtorque.unit = 'Nm';
dat.maxtorque.value = 400;

dat.tanh.comment = 'clutch actuation coefficient for smoothing the function in the clutch model using a tanh function';
dat.tanh.unit = '-';
dat.tanh.value = 2;

dat.speedtolerance.comment = 'Speed tolerance for slipping clutch';
dat.speedtolerance.unit = 'rad/s';
dat.speedtolerance.value = 3;

dat.actuator.timeconstant.comment = 'clutch actuator timeconstant';
dat.actuator.timeconstant.unit = 's';
dat.actuator.timeconstant.value = 0.01;

dat.clutchthreshold.comment = 'clutch is slipping (cannot lock) if clutch pedal actuator is < threshold';
dat.clutchthreshold.unit = '-';
dat.clutchthreshold.value = 0.8;

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = '                     (Requested Clutch Pedal Position)';
con_info(1).unit = '[ratio (0 - 1)]';
con_info(2).descr = 'M_in            (Clutch Torque Input)';
con_info(2).unit = '[Nm]';
con_info(3).descr = 'Omega)in   (Clutch Inertia Input)';
con_info(3).unit = '[kg*m^2]';
con_info(4).descr = 'Omega_1   (Clutch Rotational Speed Input';
con_info(4).unit = '[rad/s]';

%%%%%%%%%%%
%revision history
% 
