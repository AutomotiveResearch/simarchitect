%   Filename:  para_dcdc.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'DCDC converter data';
dat.filename = 'para_dcdc.m';
dat.version = '1';
dat.lastModified = '24.02.2014';
dat.modifiedBy = 'Jonas Fredriksson';

dat.elecefficiency.comment = 'efficiency map = f(current, voltage)';
dat.elecefficiency.voltage.comment = 'Index vector - voltage';
dat.elecefficiency.voltage.unit = 'V';
dat.elecefficiency.voltage.vec = [-1000 0 1000];
dat.elecefficiency.current.comment = 'Index vector - current';
dat.elecefficiency.current.unit = 'A';
dat.elecefficiency.current.vec = [-1000 0 1000];
dat.elecefficiency.efficiency.comment = 'efficiency map';
dat.elecefficiency.efficiency.unit = '-';
dat.elecefficiency.efficiency.map = [1 1 1; 1 1 1; 1 1 1;];

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = 'u_req    (Requested Output Voltage)';
con_info(1).unit = '[V]';
con_info(2).descr = 'u_in       (DCDC Converter Input Voltage)';
con_info(2).unit = '[V]';
con_info(3).descr = 'i_in        (DCDC Converter Input Current)';
con_info(3).unit = '[A]';

%%%%%%%%%%%
%revision history
% 
