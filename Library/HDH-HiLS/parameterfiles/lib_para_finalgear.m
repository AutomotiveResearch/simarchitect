%   Filename:  para_finalgear.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'Final gear model data';
dat.filename = 'para_finalgear.m';
dat.version = '1.0';
dat.lastModified = '22.01.2014';
dat.modifiedBy = 'Jonas Fredriksson';


dat.ratio.comment = 'final gear ratio';
dat.ratio.unit = '-';
dat.ratio.value = 4.9;

dat.inertia.comment = 'final gear inertia';
dat.inertia.unit = '-';
dat.inertia.value = 0.5;

dat.mechefficiency.comment = 'efficiency map = f(torque, speed)';
dat.mechefficiency.torque.Comment = 'Index vector - torque';
dat.mechefficiency.torque.unit = 'Nm';
dat.mechefficiency.torque.vec = [0 20000];
dat.mechefficiency.speed.Comment = 'Index vector - speed';
dat.mechefficiency.speed.unit = 'rad/s';
dat.mechefficiency.speed.vec = [0 1000];
dat.mechefficiency.efficiency.Comment = 'efficiency map';
dat.mechefficiency.efficiency.unit = '-';
dat.mechefficiency.efficiency.map = [0.95 0.95; 0.95 0.95];

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = 'M_in             (Final Gear Torque Input)';
con_info(1).unit = '[Nm]';
con_info(2).descr = 'J_in              (Final Gear Inertia Input)';
con_info(2).unit = '[kg*m^2]';
con_info(3).descr = 'Omega_in   (Final Gear Rotational Speed Input)';
con_info(3).unit = '[rad/s]';


%%%%%%%%%%%
%revision history
% 
