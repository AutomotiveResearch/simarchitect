%   Filename:  para_engine.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = ' ICE model data';
dat.filename = 'para_engine.m';
dat.version = '1';
dat.lastModified = '29.04.2013';
dat.modifiedBy = 'Jonas Fredriksson';

dat.inertia.comment = 'engine inertia';
dat.inertia.unit = 'kgm^2';
dat.inertia.value = 1.1;

dat.boost.comment = 'engine torque build-up model';
dat.boost.insttorque.timeconstant.T1.Comment = 'T1 time constant for instant torque build-up';
dat.boost.insttorque.timeconstant.T1.Unit = 's';
dat.boost.insttorque.timeconstant.T1.value = 0.1;
dat.boost.insttorque.Comment = 'instant torque available';
dat.boost.insttorque.Unit = 'Nm';
dat.boost.insttorque.vec = [ 200  200  200  200 200 200 200];
dat.boost.speed.Comment = 'Index vector - engine speed';
dat.boost.speed.Unit = 'rad/s';
dat.boost.speed.vec = [0.0  570.0  800.0 1400.0 3000.0 3200.0 3400.0]*2*pi/60;
dat.boost.timeconstant.T2.Comment = 'T2 time constant for boost pressure depending torque build-up ';
dat.boost.timeconstant.T2.Unit = 's';
dat.boost.timeconstant.T2.vec = [ 0.1 0.5 1 1 1 0.5 0.1];

dat.torquereqtype.comment = 'engine torque request, indicated torque = 0, crankshaft torque = 1 ';
dat.torquereqtype.unit = 'boolean';
dat.torquereqtype.value = 0;

dat.friction.comment = 'engine friction = f( speed)';
dat.friction.speed.Comment = 'Index vector -  speed';
dat.friction.speed.Unit = 'rad/s';
dat.friction.speed.vec = [0.0  570.0  800.0 1400.0 3000.0 3200.0 3400.0]*2*pi/60;
dat.friction.friction.Comment = 'engine friction';
dat.friction.friction.Unit = 'Nm';
dat.friction.friction.vec = [ 0  -72  -75  -82 -145 -154 -154];

dat.exhaustbrake.comment = 'exhaust brake = f( speed)';
dat.exhaustbrake.speed.Comment = 'Index vector -  speed';
dat.exhaustbrake.speed.Unit = 'rad/s';
dat.exhaustbrake.speed.vec = [0.0  570.0  800.0 1400.0 3000.0 3200.0 3400.0]*2*pi/60;
dat.exhaustbrake.brake.Comment = 'engine friction with fully engaged exhaust brake(s)';
dat.exhaustbrake.brake.Unit = 'Nm';
dat.exhaustbrake.brake.vec = [0 -144 -150 -164 -290 -308 -308];

dat.maxtorque.comment = 'maximum torque = f( speed)';
dat.maxtorque.speed.Comment = 'Index vector -  speed';
dat.maxtorque.speed.Unit = 'rad/s';
dat.maxtorque.speed.vec = [570.0  800.0 1400.0 3000.0 3200.0 3400.0]*2*pi/60;
dat.maxtorque.torque.Comment = 'maximum torque';
dat.maxtorque.torque.Unit = 'Nm';
dat.maxtorque.torque.vec = [380 540 580 630 540 155];

dat.fuelmap.comment = 'fuel consumption map = f( speed,  torque)';
dat.fuelmap.speedComment = ' speed';
dat.fuelmap.speedUnit = 'radps';
dat.fuelmap.speed = [0  600  900  1400  2000  2600  3200]*2*pi/60;
dat.fuelmap.torqueComment = 'maximum torque';
dat.fuelmap.torqueUnit = 'Nm';
dat.fuelmap.torque = [-160.0 0.0 50.0 100.0 200.0 300.0 400.0 450.0 500.0];
dat.fuelmap.mapComment = 'maximum torque';
dat.fuelmap.mapUnit = 'kg/s';
dat.fuelmap.map = 0.83*[
0.0 0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0,
0.0 0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0,
0.0 2.0  2.5  3.0  5.0  7.0 10.0 11.0 13.0,
0.0 2.0  3.0  5.0  7.0 11.0 15.0 18.0 20.0,
0.0 4.0  6.0  8.0 12.0 17.0 23.0 26.0 29.0,
0.0 6.0  8.0 10.0 15.0 22.0 29.0 33.0 37.0,
0.0 9.0 12.0 15.0 22.0 30.0 40.0 45.0 50.0]/3600;

dat.controller.comment = ' pi controller for speed control';
dat.controller.p.Comment = 'proportional constant';
dat.controller.p.Unit = '-';
dat.controller.p.value = 8;
dat.controller.i.Comment = 'integral constant';
dat.controller.i.Unit = '-';
dat.controller.i.value = 6;

dat.startertorque.comment = 'Starter motor torque';
dat.startertorque.Unit = 'Nm';
dat.startertorque.value = 300;

%--------------------------------------------------------------------------
% Thermal data (optional, leave unmodified if not needed)
%--------------------------------------------------------------------------
dat.cap.comment = 'engine displacement';
dat.cap.unit = 'cm^3';
dat.cap.value = 6000;

dat.oil.comment = ['oil temperature as f (heat loss normalized to  capacity)'];
dat.oil.heatloss.Comment = ' Index vector - heat loss normalized to  capacity';
dat.oil.heatloss.Unit = 'kJ/cm^3';
dat.oil.heatloss.vec = [0.0000 0.4762 0.9524 1.4286 1.9048 2.3810 2.8571 3.3333 3.8095 4.2857 4.7619 5.2381 5.7143 6.1905 6.6667]*dat.cap.value;
dat.oil.temp.Comment = 'oil temperature';
dat.oil.temp.Unit = 'K';
dat.oil.temp.vec = [293.0 301.5 309.5 316.8 323.5 329.8 335.4 340.6 345.3 349.5 353.3 356.6 359.5 362.0 364.2];
dat.oil.fixtemp.Comment = 'fixed oil temperature';
dat.oil.fixtemp.Unit = 'K';
dat.oil.fixtemp.value = 95+273;

dat.cf.comment = ['cooling fluid temperature as f (heat loss normalized to displacement)'];
dat.cf.heatloss.Comment = 'Index vector - heat loss normalized to  displacement';
dat.cf.heatloss.Unit = 'kJ/cm^3';
dat.cf.heatloss.vec = [0.0000 0.4762 0.9524 1.4286 1.9048 2.3810 2.8571 3.3333 3.8095 4.2857 4.7619 5.2381 5.7143 6.1905 6.6667]*dat.cap.value;
dat.cf.temp.Comment = 'cooling fluid temperature';
dat.cf.temp.Unit = 'K';
dat.cf.temp.vec = [293.0 304.5 315.0 324.5 333.1 340.6 347.2 352.7 357.3 360.9 363.5 365.1 365.7 365.3 364.0];
dat.cf.fixtemp.Comment = 'fixed cooling fluid temperature';
dat.cf.fixtemp.Unit = 'K';
dat.cf.fixtemp.value = 85+273;


%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = 'Omega_ref         (Requested Engine Speed)';
con_info(1).unit = ' [rad/s]';
con_info(2).descr = '                           (Switch Speed/Torque Control)';
con_info(2).unit = ' [Boolean]';
con_info(3).descr = 'M_ice,des           (Requested Engine Torque)';
con_info(3).unit = ' [Nm]';
con_info(4).descr = '                           (Exhaust Brake ON / OFF)';
con_info(4).unit = ' [Boolean]';
con_info(5).descr = '                           (Engine ON / OFF)';
con_info(5).unit = ' [Boolean]';
con_info(6).descr = '                           (Starter Motor ON / OFF)';
con_info(6).unit = ' [Boolean]';
con_info(7).descr = '                           (Fuel Cut Off)';
con_info(7).unit = ' [Boolean]';
con_info(8).descr = 'Omega_ice         (Engine rotational Speed input)';
con_info(8).unit = ' [rad/s]';

%%%%%%%%%%%
%revision history
% 
