%   Filename:  para_cvt.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'continuously variable transmission data';
dat.filename = 'para_cvt.m';
dat.version = '1';
dat.lastModified = '22.05.2013';
dat.modifiedBy = 'Jonas Fredriksson';

dat.inertia.comment = 'CVT inertia';
dat.inertia.unit = 'kgm^2';
dat.inertia.value = 0.01;

dat.timeconstant.comment = 'actuator time constant';
dat.timeconstant.unit = '-';
dat.timeconstant.value = 0.1;

dat.mechefficiency.comment = 'CVT eff = f(speed, torque, gear ratio) ';
dat.mechefficiency.speed.comment = 'Index vector - speed';
dat.mechefficiency.speed.unit = 'rad/s';
dat.mechefficiency.speed.vec = [0 1000 5000];
dat.mechefficiency.torque.comment = 'Index vector - torque';
dat.mechefficiency.torque.unit = 'rad/s';
dat.mechefficiency.torque.vec = [-200 0 200];
dat.mechefficiency.ratio.comment = 'Index vector - gear ratio';
dat.mechefficiency.ratio.unit = '-';
dat.mechefficiency.ratio.vec = [0.5 5 10];
dat.mechefficiency.efficiency.comment = 'efficiency map';
dat.mechefficiency.efficiency.unit = '-';
dat.mechefficiency.efficiency.map(:,:,1) = [1/0.8 1 0.8; 1/0.8 1 0.8; 1/0.8 1 0.8];
dat.mechefficiency.efficiency.map(:,:,2) = [1/0.8 1 0.8; 1/0.8 1 0.8; 1/0.8 1 0.8];
dat.mechefficiency.efficiency.map(:,:,3) = [1/0.8 1 0.8; 1/0.8 1 0.8; 1/0.8 1 0.8];

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = 'r_des              (CVT Requested Gear Ratio)';
con_info(1).unit = '[ratio (0 - 1)]';
con_info(2).descr = 'M_in               (CVT Torque Input)';
con_info(2).unit = '[Nm]';
con_info(3).descr = 'J_in                (CVT Inertia Input)';
con_info(3).unit = '[kg*m^2]';
con_info(4).descr = 'Omega_out   (CVT Rotational Speed Input)';
con_info(4).unit = '[rad/s]';

%%%%%%%%%%%
%revision history
% 
