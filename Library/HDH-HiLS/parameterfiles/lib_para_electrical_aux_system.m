%   Filename:  para_electrical_aux_system.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================



%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = '           (Requested Power)';
con_info(1).unit = '[W]';
con_info(2).descr = 'u         (Voltage Input)';
con_info(2).unit = '[V]';

%%%%%%%%%%%
%revision history
% 
