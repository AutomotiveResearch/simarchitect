%   Filename:  para_torqueconverter.m
%
%  ============================================================
%   SIMarchitect 
%   Model: HDH-HiL
%	author: CB
%  ============================================================

dat.comment = 'Torque converter model data';
dat.filename = 'para_torqueconverter.m';
dat.version = '1';
dat.lastModified = '07.03.2014';
dat.modifiedBy = 'Gerard Silberholz';

dat.inertia.in.comment = 'torque converter input side inertia';
dat.inertia.in.unit = 'kgm^2';
dat.inertia.in.value = 0.1;

dat.inertia.out.comment = 'torque converter output side inertia';
dat.inertia.out.unit = 'kgm^2';
dat.inertia.out.value = 0.15;

dat.clutch.actuator.timeconstant.comment = 'lock-up clutch actuator timeconstant';
dat.clutch.actuator.timeconstant.unit = 's';
dat.clutch.actuator.timeconstant.value = 0.1;

dat.clutch.speedtolerance.comment = 'Speed tolerance for slipping lock-up clutch';
dat.clutch.speedtolerance.unit = 'rad/s';
dat.clutch.speedtolerance.value = 3;

dat.clutch.maxtorque.comment = 'Max torque transmitted by lock-up clutch';
dat.clutch.maxtorque.unit = 'Nm';
dat.clutch.maxtorque.value = 1000;

dat.clutch.threshold.comment = 'lock-up clutch threshold value';
dat.clutch.threshold.unit = '-';
dat.clutch.threshold.value = 0.2;

dat.clutch.tanh.comment = 'lock-up clutch actuation coefficient for smoothing the sign function in the clutch model using a tanh function';
dat.clutch.tanh.unit = '-';
dat.clutch.tanh.value = 2;

dat.characteristics.comment = 'Torque converter characteristics according to "VDI 2153"';
dat.characteristics.speedratio.comment = 'Index vector - speed ratio (output speed/input speed)';
dat.characteristics.speedratio.unit = '-';
dat.characteristics.speedratio.vec = [0 0.3 0.6 0.8 1 1.2];

dat.characteristics.refspeed.comment = 'Reference input speed for input torque characteristics';
dat.characteristics.refspeed.unit = 'rad/s';
dat.characteristics.refspeed.value = (1000) *2*pi/60;

dat.characteristics.inputtorque.comment = 'Input torque at reference input speed = f(speed ratio)';
dat.characteristics.inputtorque.unit = 'Nm';
dat.characteristics.inputtorque.vec = [200 200 170 120 0 -100];

dat.characteristics.torqueratio.comment = 'Torque ratio (output torque/input torque) = f(speed ratio)';
dat.characteristics.torqueratio.unit = '-';
dat.characteristics.torqueratio.vec = [2.1 1.7 1.3 1.1 1 1.1];

dat.characteristics.loss.comment = 'Torque loss at locked lock-up clutch  = f(speed)';
dat.characteristics.loss.speed.comment = 'Index vector - speed ';
dat.characteristics.loss.speed.unit = 'rad/s';
dat.characteristics.loss.speed.vec = [0 500 1000 1500 2000 3000]*2*pi/60;

dat.characteristics.loss.torque.comment = 'Loss torque';
dat.characteristics.loss.torque.unit = 'Nm';
dat.characteristics.loss.torque.vec = [0 -2 -4 -6 -8 -11];

%____Connection information__________________________
%The con_info structure describes the signals and the order of the signals that are needed for on the con_i port.
%The 'desr' field gives a description of the signal and the 'unit' field contains the required unit.
%This information is used for the GUI of the SIMarchitect busselector.
con_info(1).descr = '                           (Torque Converter Lock Up)';
con_info(1).unit = '[Boolean]';
con_info(2).descr = 'M_in                   (Torque Converter Torque Input)';
con_info(2).unit = '[Nm]';
con_info(3).descr = 'Omegaa_in       (Torque Converter Inertia Input)';
con_info(3).unit = '[kg*m^2]';
con_info(4).descr = 'Omega_t           (Torque Converter Rotational Input)';
con_info(4).unit = '[rad/s]';

%%%%%%%%%%%
%revision history
% 
