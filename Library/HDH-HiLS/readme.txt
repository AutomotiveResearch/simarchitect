%{ 
HDH-HiLS Library  is based on original work produced by UNECE
Original work Copyright (c) 2018 [United Nations Economic Commission for Europe � UNECE]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

More information on the original work of UNECE can be found at: https://wiki.unece.org/display/trans/HDH+General+Files 
%}

Please consult DISCLAIMER_HDH-HiLS.pdf with regards to the role of the HAN.

==============================================================================================
========================= SIMarchitect HDH-HiLS Library Installation ========================= 
============
**Important**

    
How to install the HDH-HiLS Library
 
1. Navigate to path \...\Library Database	NOTE: This is a folder which must be one level higher
						than the SIMarchitect root folder.

2. Place HDH-HiLS folder inside the Library Database folder

3. Open Matlab 2015b or higher

4. Navigate to \...\Library Database\HDH-HiLS\

5. In the Matlab command window, type 'HDH_HiLS_install'

6. When prompted, the License and Disclaimer must be accepted by typing 'y' and pressing 'enter'

7. Installation is complete. 			NOTE: If the SIMarchitect HDH-HiLS library is not 
						visible in the Simulink Library Browser, press
						'F5' to refresh the browser. 

WARNING: 'installed.txt' may not be deleted. When deleted, SIMarchitect HDH-HiLS Library must be re-installed. 

================================================================
  Build with MATLAB Version: 8.6.0.267246 (R2015b)  
================================================================   