%{ 
HDH-HiLS Library  is based on original work produced by UNECE
Original work Copyright (c) 2018 [United Nations Economic Commission for Europe � UNECE]  
Modified work Copyright (c) 2018 [HAN University of applied Sciences]
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

More information on the original work of UNECE can be found at: https://wiki.unece.org/display/trans/HDH+General+Files 
%}

function [] = HDH_HiLS_install()

disp('The use of the HDH-HiLS Library is subject to the License and Disclaimer. Accept and Continue?')
key = input('(Y/N)','s');
if strcmp(key,'y') || strcmp(key,'Y')
    % create install file
    disp('License and Disclaimer Accepted')
    if ~exist(strcat(pwd,'\installed.txt'),'file') % Not yet installed (First install)
        disp('Installing ...')
        fid=fopen(strcat(pwd,'\installed.txt'),'wt+'); % Create file
        fprintf(fid,strcat('Installed on:  ',date));       % Write date to file
        run('slblocks.m')
        disp('Installation Complete')
    else
        disp('Library is already installed')    % Library has been installed already
    end
else
    error('License and Disclaimer must be accepted. Installation aborted.') % Abort Install in not Accepted
end

end

